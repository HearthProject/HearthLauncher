#!/bin/sh

rm -rf release
mkdir -p release
cd ./release
cp ../build/HearthLauncher .
mkdir -p plugins/imageformats
cp /usr/lib/x86_64-linux-gnu/qt5/plugins/imageformats/libqsvg.so plugins/imageformats
linuxdeployqt HearthLauncher -no-translations -appimage
rm default.* *.AppImage .DirIcon AppRun