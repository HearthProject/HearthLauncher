from html import escape
from ftplib import FTP
from os import environ, remove, path, makedirs
from shutil import make_archive, move, rmtree
from json import loads, dumps
from sys import platform
from glob import glob
from datetime import datetime
from requests import post
from markdown import markdown

VERSION_NUM = open("src/VERSION").read().strip()
CHANGES = markdown(escape(open("src/CHANGES").read().strip()))
OS_NAME = platform.lower()
JOB_ID = environ["CI_JOB_ID"]
JOB_TYPE = "release"
DEST_FILE_PATH = "/hl3/builds/{}/HearthLauncher-{}-{}.zip".format(OS_NAME,OS_NAME,VERSION_NUM)
JSON_FILE_PATH = "/hl3/builds/{}/versions.json".format(OS_NAME)

TARGET_NAME = "HearthLauncher-" + VERSION_NUM
TARGET_FILE_PATH = "release"
print(glob("*"))
if OS_NAME == "win32":
    for o in glob("release/*.o"):
        remove(o)
    for o in glob("release/*.cpp"):
        remove(o)
    for o in glob("release/*.h"):
        remove(o)

print("Zipping build")

if path.exists("OUT_REL"):
    rmtree("OUT_REL")

makedirs("OUT_REL")

move(TARGET_FILE_PATH, "OUT_REL/" + TARGET_NAME)
TARGET_ARCHIVE = make_archive(TARGET_NAME, "zip", "OUT_REL")

print("Uploading to FTP server")

ftp = FTP(environ["FTP_SERVER"], environ["FTP_USERNAME"], environ["FTP_PASSWORD"])
ftp.storbinary("STOR " + DEST_FILE_PATH, open(TARGET_ARCHIVE, "rb"))

print("Grabbing previous JSON")

with open("versions.json", "wb+") as f:
    def callback(data):
        f.write(data)

    ftp.retrbinary("RETR " + JSON_FILE_PATH, callback)

VERSIONS = loads(open("versions.json").read())
if float(VERSIONS["latest"]) < float(VERSION_NUM):
    VERSIONS["latest"] = VERSION_NUM

VERSIONS["versions"][VERSION_NUM] = {
        "changelog": CHANGES,
        "file": "https://fdn.redstone.tech/theoneclient" + DEST_FILE_PATH,
        "time": datetime.now().isoformat()
}

open("versions.json", "w").write(dumps(VERSIONS))

print("Uploading updated versions")

ftp.storbinary("STOR " + JSON_FILE_PATH, open("versions.json", "rb"))

print("Cleaning up")

rmtree("OUT_REL")
remove(TARGET_ARCHIVE)
remove("versions.json")

print("Updating website")
post("https://hearthproject.uk/update", json = {"KEY": environ["WEBHOOK_KEY"]})

print("Done!")
