rm -rf release
mkdir release
cd release

mkdir -p HearthLauncher.app/Contents/Frameworks
mkdir -p HearthLauncher.app/Contents/MacOS

mv ../build/HearthLauncher HearthLauncher.app/Contents/MacOS/

cp -R /usr/local/opt/qt/lib/QtCore.framework HearthLauncher.app/Contents/Frameworks
cp -R /usr/local/opt/qt/lib/QtGui.framework HearthLauncher.app/Contents/Frameworks
cp -R /usr/local/opt/qt/lib/QtNetwork.framework HearthLauncher.app/Contents/Frameworks
cp -R /usr/local/opt/qt/lib/QtWidgets.framework HearthLauncher.app/Contents/Frameworks

chmod 644 HearthLauncher.app/Contents/Frameworks/Qt*.framework/Versions/5/Qt*

/usr/local/opt/qt/bin/macdeployqt HearthLauncher.app

install_name_tool -change /usr/local/Cellar/qt/5.9.3/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore HearthLauncher.app/Contents/Frameworks/QtGui.framework/Versions/5/QtGui
install_name_tool -change /usr/local/Cellar/qt/5.9.3/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore HearthLauncher.app/Contents/Frameworks/QtWidgets.framework/Versions/5/QtWidgets
install_name_tool -change /usr/local/Cellar/qt/5.9.3/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore HearthLauncher.app/Contents/Frameworks/QtNetwork.framework/Versions/5/QtNetwork

install_name_tool -change /usr/local/Cellar/qt/5.9.3/lib/QtGui.framework/Versions/5/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/5/QtGui HearthLauncher.app/Contents/Frameworks/QtWidgets.framework/Versions/5/QtWidgets

cp ../MacOS/Info.plist HearthLauncher.app/Contents/
cp ../MacOS/icon.icns HearthLauncher.app/Contents/Resources/
sed -i '' s/\{\{version\}\}/$(cat ../src/VERSION)/g HearthLauncher.app/Contents/Info.plist
