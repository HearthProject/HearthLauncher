﻿# The OneFormat for Minecraft Modpacks

OneFormat is designed to be easily converted to and from different modpack formats.  

## Overview:

OneFormat packs are tarballs, with the extension `.1f`

### Contents structure:

* Root level (`pack.1f`)
 * Pack Manifest (`manifest.json`)
 * Files (`pack_files`)
   * Sub-folders and files
   
## Manifest Format:

Manifest files use the JSON format.

### Schema:

* info
 * title
 * desc
 * authors
 * version
 * id
 * icon [
 	* file
 	* hash
 	]
* minecraft
 * version
* forge
 * version
* mods [
 * name
 * source
 * project
 * file
 * sha1 ]
* files [
 * source
 * dest
 * sha1 ]

### Fields:

**INFO** (Dict)  
Information about the pack  

**INFO.TITLE** (String)  
The pack title  

**INFO.DESC** (String)  
Description of the modpack  

**INFO.AUTHORS** (List > String)  
List of author names  

**INFO.VERSION** (Integer)  
Version of the modpack

**INFO.ID** (String)  
The source of the pack - a unique idetifier  

**INFO.ICON.FILE** (String)  
The file of the pack icon image

**INFO.ICON.HASH** (String)  
The hash of the pack icon image- a unique idetifier  

**MINECRAFT** (Dict)  
Minecraft information  

**MINECRAFT.VERSION** (String)  
Minecraft version  

**FORGE** (Dict)  
Forge information  

**FORGE.VERSION** (String)  
Forge version  

**MODS** (List > Mod)  
List of mods  

**MOD.NAME** (String)  
Name of the mod

**MOD.SOURCE** (String)  
Source of the mod  

**MOD.PROJECT** (Integer)  
Project ID for the mod  

**MOD.FILE** (String | Int)  
File, string for local file, int for curse project  

**MOD.SHA1** (String)  
Checksum of the file. Leave blank to skip  

### Example Manifest:

```json
{
	"info": {
    	"title": "Modpack Title",
        "desc": "Full description for your modpack",
        "authors": [
        	"joonatoona",
            "primetoxinz"
        ],
        "version": 1,
        "id": "hearth-123",
        "icon": {
        	"file": "icon.png",
        	"hash": "<hash>"
        }
    },
    "minecraft": {
    	"version": "1.12.2"
    },
    "forge": {
    	"version": "14.23.0.2515"
    },
    "mods": [
    	{
        	"name": "Fake Mod",
            "source": "curse",
            "project": 123,
            "file": 123,
            "sha1": "<hash>"
        },
        {
        	"name": "More Fake Mod",
            "source": "file",
            "file": "mods/fakemod.jar",
            "sha1": "<hash>"
        }
    ],
    "files": [
    	{
        	"source": "configs/fakemod/config.cfg",
            "dest": "config/fakemod.cfg",
            "sha1": "<hash>"
        },
        {
        	"source": "configs/options.txt",
            "dest": "options.txt",
            "sha1": "<hash>"
        }
    ]
}
```
