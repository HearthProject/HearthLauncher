[ ] I have verified that this issue is not a duplicate

* Your OS: (Your OS name and version)
* HearthLauncher Version: (The version of HearthLauncher you're using)

### Summary

(Summarize the bug)

### Steps to reproduce

(Complete steps on what you did to cause the bug)

### Expected behavior

(What you expect to happen)

### Actual behavior

(What actually happens)

### Relevant logs and/or screenshots

(Put any logs or screenshots. Make sure to put your logs in a code block (```))
