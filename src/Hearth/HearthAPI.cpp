/*
 * Copyright (C) 2017-2018 joonatoona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "HearthAPI.h"

#include "Hearth/HearthAuthSession.h"
#include "Util/Instrumentation.h"
#include "Util/NetworkUtil.h"

#include <QDebug>
#include <QJsonArray>

#ifdef HL_DEBUG
const QString HearthAPI::BASE_URL = "https://testing.hearthproject.uk";
#else
const QString HearthAPI::BASE_URL = "https://api.hearthproject.uk";
#endif

HearthAPI *hearthAPI;

HearthAPI::HearthAPI() {}

void HearthAPI::init() {
    ScopedTask _("HearthAPI::init");
    QJsonObject serverInfo = NetworkUtil::get(QUrl(BASE_URL)).json.object();
    config.inviteRequired = serverInfo["inviteRequired"].toBool();
    config.instanceName = serverInfo["instanceName"].toString();
    QJsonArray toVex = serverInfo["roleNames"].toArray();
    for (QJsonValue val : toVex) {
        config.roleNames.append(val.toString());
    }
    toVex = serverInfo["maxFileSize"].toArray();
    for (QJsonValue val : toVex) {
        config.maxFileSize.append(val.toInt());
    }
    toVex = serverInfo["maxTotalSize"].toArray();
    for (QJsonValue val : toVex) {
        config.maxTotalSize.append(val.toInt());
    }

    config.index.load(serverInfo["sources"].toArray());
    qInfo() << "Loading OneMeta Sources " << config.index.sources.keys();
}

void HearthAPI::login(const AuthSession &auth, const QString &inviteKey) {
    QString sessionHash = Utils::generateUUID();
    QJsonObject mojangReq;
    mojangReq["accessToken"] = auth.accesstoken();
    mojangReq["selectedProfile"] = auth.user().id;
    mojangReq["serverId"] = sessionHash;
    QJsonObject req;
    req["loginHash"] = sessionHash;
    req["username"] = auth.user().name;
    req["inviteKey"] = inviteKey;
    NetworkUtil::post(QUrl("https://sessionserver.mojang.com/session/minecraft/join"), mojangReq);
    QJsonObject userObj = NetworkUtil::post(QUrl(BASE_URL + "/auth/login"), req).json.object();
    HearthAuthSession::instance()->loadFromJson(userObj);
    //    HearthAuthSession::instance()->writeToFile(config.data_dir + "/hauth.dat");
}

QVector<OneMeta::Source> HearthAPI::getSources(OneMeta::SourceType source) {
    return config.index.sources.values(source).toVector();
}
