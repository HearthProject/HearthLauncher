#include "HearthAuthSession.h"

#include "Hearth/HearthAPI.h"

#include <QJsonDocument>

HearthAuthSession *hearthAuthSession;

HearthAuthSession::HearthAuthSession() {}

void HearthAuthSession::loadFromJson(const QJsonObject &resp) {
    uuid = resp["uuid"].toString();
    username = resp["username"].toString();
    apikey = resp["apikey"].toString();
    role = static_cast<OneMeta::Role>(resp["role"].toInt());
    roleName = HearthAPI::instance()->config.roleNames.value(role, "Hearth Failed...");
    banned = resp["banned"].toBool();
    usedStorage = resp["usedStorage"].toInt();
}

void HearthAuthSession::loadFromFile(QString auth_f) {
    QFile authFile(auth_f);
    authFile.open(QIODevice::ReadOnly);
    QJsonObject authObject = QJsonDocument::fromBinaryData(authFile.readAll()).object();
    authFile.close();
    uuid = authObject["i"].toString();
    username = authObject["u"].toString();
    apikey = authObject["a"].toString();
    role = static_cast<OneMeta::Role>(authObject["r"].toInt());
    roleName = HearthAPI::instance()->config.roleNames.value(role, "Hearth Failed...");
    banned = authObject["b"].toBool();
    usedStorage = authObject["s"].toInt();
}

void HearthAuthSession::writeToFile(QString file) {
    QJsonObject out;
    out["i"] = uuid;
    out["u"] = username;
    out["a"] = apikey;
    out["r"] = role;
    out["b"] = banned;
    out["s"] = usedStorage;
    QFile authFile(file);
    authFile.open(QIODevice::WriteOnly);
    QJsonDocument outDoc(out);
    authFile.write(outDoc.toBinaryData());
}
