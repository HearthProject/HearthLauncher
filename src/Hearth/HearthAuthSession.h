#pragma once

#include "OneMeta/OneMeta.h"
#include <QJsonObject>

class HearthAuthSession;
extern HearthAuthSession *hearthAuthSession;

class HearthAuthSession {
public:
    HearthAuthSession();

    static HearthAuthSession *instance() { return hearthAuthSession; }

    void loadFromJson(const QJsonObject &resp);
    void loadFromFile(QString auth_f);
    void writeToFile(QString auth_f);

    QString uuid;
    QString username;
    QString apikey;
    OneMeta::Role role;
    QString roleName;
    bool banned;
    int usedStorage;
};
