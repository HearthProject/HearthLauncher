<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>qrc:/about.html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HearthLauncher v{ver} built by foo on bar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please login to your Mojang Account for launching Minecraft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuthWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BasePack</name>
    <message>
        <source>Downloading Pack Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pack Notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Finished Installing %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pack Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pack handler missing. Failed to install pack.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Finished Updating %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error: No Update Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browse</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckTreeDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minecraft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Settings Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Icon Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images (*.png *.xpm *.jpg *.gif)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CurseImporter</name>
    <message>
        <source>Select a Curse Import file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ZIP (*.zip)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorDialog</name>
    <message>
        <source>Oh Noes!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OpenMineMods has crashed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email Address (OPTIONAL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional Notes (OPTIONAL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send the crash report to the developers?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CheckBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Files/Folders to include</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CheckBox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtendedTabWidget</name>
    <message>
        <source>Page %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileDialog</name>
    <message>
        <source>Pick a file to install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh Instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Featured Modpack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Latest News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(Latest post title on blog.hearthproject.uk)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading Featured Packs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Featured Packs Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OneFormat Importer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Curse Import</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstanceDialog</name>
    <message>
        <source>Add Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Submit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstanceInfo</name>
    <message>
        <source>Select an Export location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OneFormat (*.1f)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstanceWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>instanceWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Instance Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Launch Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure you want to delete %1?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstanceWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Installed Mods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse Mods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mod Browsing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JavaDialog</name>
    <message>
        <source>Select Java Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please select a Java Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JavaUtils</name>
    <message>
        <source>Java Verification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please use Java Detection or set the path to a valid binary.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This Java Binary did not work. 
 Please use Java Detection or set the path to a valid binary.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Kill Minecraft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Debug Dump</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Log upload failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Link copied to clipboard!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MetaFileModel</name>
    <message>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name of the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The release stage of the file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinecraftInstance</name>
    <message>
        <source>Update Confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Updates Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Debug Dump</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HearthLauncher Dump (*.hldump)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Would you like to update %1 to %2?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading Minecraft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Installing Minecraft Server </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading Version Manifest </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading Server Jar </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minecraft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forge</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModList</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Checks for update to this mod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModListWidget</name>
    <message>
        <source>Select a mod file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModTableModel</name>
    <message>
        <source>Latest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Is the mod enabled?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name of the mod.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mod Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Behind %1 version(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OFImporter</name>
    <message>
        <source>Select a OneFormat Import file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1f (*.1f)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneFormat::Pack</name>
    <message>
        <source>Downloading Mods %1/%2
 Installing %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing Overrides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copying Overrides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copying Overrides
 Copying </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PackWidget</name>
    <message>
        <source>Installing </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <source>ProgressDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open in Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View Extra Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View More</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send basic analytics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Minecraft Snapshot Versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Minecraft Alpha/Beta Versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check for Pack Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tray Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Java Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Detect Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Java Arguments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrapper Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum RAM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum RAM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Find Java Executable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupDialog</name>
    <message>
        <source>Hearth Launcher Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/hearth.png&quot; width=&quot;50&quot; /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Welcome to the Hearth Launcher!&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; vertical-align:sub;&quot;&gt;HearthLauncher {VER}, built by {BUILDER}.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Automatically check for Hearth Launcher updates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CheckBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send basic analytics:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Java Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Detect Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verify Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <source>Hearth Launcher Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An update is availible!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Would you like to update now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
