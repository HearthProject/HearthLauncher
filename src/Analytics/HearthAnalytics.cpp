/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "HearthAnalytics.h"
#include "Hearth/HearthAPI.h"
#include "Util/Utils.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>

HearthAnalytics::HearthAnalytics(Config &conf) : config(conf) {
    OS_NAME = QSysInfo::prettyProductName();
    OS_ARCH = QSysInfo::currentCpuArchitecture();
    LOCALE = QLocale::system().name();
    INSTALLED_MEM = Utils::getInstalledMemory();
}

void HearthAnalytics::sendEvent(HearthEventType evt, QVariant extra) {
    if (!config.sendAnalytics) {
        emit post();
        return;
    }
    QJsonObject packet;
    packet["uuid"] = config.analyticsId;
    packet["version"] = VERSION_STRING;
    packet["builder"] = BUILDER_NAME;
    packet["os_name"] = OS_NAME;
    packet["arch"] = OS_ARCH;
    packet["mem"] = INSTALLED_MEM;
    packet["locale"] = LOCALE;
    packet["event"] = evt;
    packet["extra"] = extra.toString();

    QJsonDocument doc(packet);

    emit post(HearthAPI::BASE_URL + "/analytics/event", "application/json", doc.toJson());
}
