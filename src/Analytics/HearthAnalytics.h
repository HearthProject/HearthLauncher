/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Util/Config.h"

#include <QObject>
#include <QVariant>

enum HearthEventType { INSTALLED, LAUNCHED, PACK_INSTALLED, INSTANCE_CREATED, MOD_INSTALLED };

class HearthAnalytics : public QObject {
    Q_OBJECT
public:
    explicit HearthAnalytics(Config &conf);

    static HearthAnalytics &instance() {
        static HearthAnalytics ha(Config::instance());
        return ha;
    }

    void sendEvent(HearthEventType evt, QVariant extra = QVariant());

signals:
    void post(QString url = QString(), QString type = QString(), QByteArray content = QByteArray());

public:
    Config &config;
    QString OS_NAME;
    QString OS_ARCH;
    QString LOCALE;
    int INSTALLED_MEM;
};
