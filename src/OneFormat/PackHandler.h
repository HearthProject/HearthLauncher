#pragma once

#include "OneFormat.h"
#include <QString>

class PackHandler {
public:
    virtual OneFormat::Pack *readPack() = 0;
};
