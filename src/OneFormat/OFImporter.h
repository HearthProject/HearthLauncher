#pragma once

#include "OneFormat/PackImporter.h"

class Config;
class HearthAnalytics;

class OFImporter : public PackImporter {
    Q_OBJECT
public:
    OFImporter(QWidget *parent, Config &config, HearthAnalytics &tracker);
    bool import(const QString &name) override;

    Config &config;
    HearthAnalytics &tracker;
};
