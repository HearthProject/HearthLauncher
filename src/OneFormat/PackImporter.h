#pragma once

#include "Util/Utils.h"
#include <QFileDialog>
#include <QObject>
#include <utility>

class PackImporter : public QObject {
    Q_OBJECT
public:
    PackImporter(QString caption = QString(), QString startDir = QString(), const QString &filters = QString(),
                 QWidget *parent = nullptr)
        : caption(std::move(caption)), startDir(std::move(startDir)), filters(std::move(filters)), parent(parent) {}

public slots:
    void selectFile() {
        QString file = QFileDialog::getOpenFileName(parent, caption, startDir, filters);
        if (Utils::fileExists(file)) {
            setFile(file);
        } else {
            setFile("");
        }
        emit(signalFile(file));
    }

    virtual bool import(const QString &name = "") = 0;

    void setFile(const QString &file) { this->file = file; }

signals:
    void signalFile(QString file);
    void installed();

protected:
    QString file;
    QString caption, startDir, filters;
    QWidget *parent;
};
