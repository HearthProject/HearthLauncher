/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OneFormat.h"
#include "Analytics/HearthAnalytics.h"
#include "Minecraft/MinecraftInstance.h"
#include <Minecraft/MetaMod.h>
#include <QObject>

#ifdef Q_OS_LINUX
#else
#include <JlCompress.h>
#endif

using namespace OneFormat;

Pack::Pack(Config &conf, HearthAnalytics &ha) : config(conf), tracker(ha) {}

Pack::Pack(Config &conf, HearthAnalytics &ha, MinecraftInstance *instance)
    : config(conf), tracker(ha), m_instance(instance) {

    minecraft = m_instance->minecraft();
    forge = m_instance->forge();
    info->title = m_instance->name();
    info->version = "1";
    // TODO icon encoding
}

Pack::Pack(QString dir, Config &conf, HearthAnalytics &ha, OneMeta::FilePtr file)
    : config(conf), tracker(ha), file(file) {
    load(dir % "/manifest.json");
}

void Pack::addMod(OneMeta::FilePtr file) {
    QString path = Utils::combinePath(m_instance->getSubDir("mods"), file->filename);
    PackMod *packMod = new PackMod;
    packMod->file = file->id;
    packMod->project = file->project;
    packMod->source = file->sourceType;
    packMod->hash = Utils::getChecksum(path);
    modHashes.insert(packMod->hash, file);
    mods.insert(packMod->project, packMod);
    qDebug() << "Adding" << file->displayname;
    save(Utils::combinePath(m_instance->directory(), "manifest.json"));
}

void Pack::removeMod(OneMeta::FilePtr file) {
    mods.remove(file->project);
    modHashes.remove(file->hash);
    QString path = Utils::combinePath(m_instance->getSubDir("mods"), file->filename);
    QDir().remove(path);
    qDebug() << "Removing" << file->displayname;
    save(Utils::combinePath(m_instance->directory(), "manifest.json"));
}

void Pack::exportPack(const QString &zip) {
    Utils::zipFiles({Utils::combinePath(m_instance->directory(), "manifest.json")}, zip);
}

void Pack::update() {
    if (!m_instance) {
        qWarning() << "No instance!";
        return;
    }
    Pack *old = m_instance->pack();

    if (old->forge != forge)
        m_instance->setForge(forge);
    if (old->minecraft != minecraft)
        m_instance->setMinecraft(minecraft);

    QString mc_dir = m_instance->getSubDir();
    int max = mods.size() - 1;
    signalMax(max);
    int i = 0;

    // Remove mods that are no longer available
    for (PackMod *old_mod : old->mods.values()) {
        if (!mods.contains(old_mod->project)) {
            OneMeta::FilePtr old_file = OneMeta::database.findFile(old_mod->source, old_mod->file);
            removeMod(old_file);
        }
    }

    for (PackMod *cmod : mods.values()) {
        OneMeta::ProjectPtr project = OneMeta::database.findProject(cmod->source, cmod->project);
        OneMeta::FilePtr file = OneMeta::database.findFile(cmod->source, cmod->file);
        signalMessage(QString(tr("Downloading Mods %1/%2\n Installing %3"))
                          .arg(QString::number(i), QString::number(max), project->title));

        PackMod *old_mod = old->mods.value(cmod->project, 0);

        if (old_mod) {
            if (cmod->file != old_mod->file) {

                // Get old file for path and remove
                OneMeta::FilePtr old_file = OneMeta::database.findFile(old_mod->source, old_mod->file);
                removeMod(old_file);

                qDebug() << "Updating" << old_file->displayname << "To" << file->displayname;
                // install new file
                MetaMod mod(m_instance, file);
                mod.install();
            }
        }

        signalProgress(i++);
    }

    i = 0;
    for (PackFile ofile : old->files.values()) {
        QFileInfo info(ofile.dest);
        QDir().remove(mc_dir + info.path());
        emit(signalMessage(tr("Removing Overrides")));
    }

    i = 0;
    signalMax(files.size() - 1);
    signalMessage(tr("Copying Overrides"));
    for (PackFile cfile : files.values()) {
        QFileInfo info(cfile.dest);
        QDir().mkpath(mc_dir + info.path());
        QFile::copy(cfile.source, mc_dir + cfile.dest);
        emit(signalMessage(tr("Copying Overrides\n Copying ") + info.fileName()));
        emit(signalProgress(i++));
    }

    save(Utils::combinePath(m_instance->directory(), "manifest.json"));
}

void Pack::install() {
    MinecraftInstance instance(info->title, config, tracker);
    instance.setupInstance(minecraft, forge);
    instance.setMinecraft(minecraft);
    instance.setForge(forge);
    instance.setIcon(info->id + ".png");
    QString mc_dir = instance.getSubDir();
    int max = mods.size() - 1;
    signalMax(max);
    int i = 0;
    for (PackMod *cmod : mods.values()) {
        OneMeta::ProjectPtr project = OneMeta::database.findProject(cmod->source, cmod->project);
        OneMeta::FilePtr file = OneMeta::database.findFile(cmod->source, cmod->file);
        if (!project || !file) {
            qDebug() << cmod->source << cmod->project << "not found";
            continue;
        }

        signalMessage(QString(tr("Downloading Mods %1/%2\n Installing %3"))
                          .arg(QString::number(i), QString::number(max), project->title));
        MetaMod mod(&instance, file);
        QString result = mod.install();
        if (!result.isEmpty()) {
            cmod->hash = Utils::getChecksum(result);
            if (file)
                modHashes.insert(cmod->hash, file);
            mods.insert(cmod->project, cmod);
        }
        signalProgress(i++);
    }
    i = 0;
    signalMax(files.size() - 1);
    signalMessage(tr("Copying Overrides"));
    for (PackFile cfile : files.values()) {
        QFileInfo info(cfile.dest);
        QDir().mkpath(Utils::combinePath(mc_dir, info.path()));
        auto override = Utils::combinePath(mc_dir, cfile.dest);
        QFile::copy(cfile.source, override);
        qDebug() << "Overriding:" << info.fileName() << " - " << override;
        signalMessage(tr("Copying Overrides\n Copying ") + info.fileName());
        signalProgress(i++);
    }

    save(Utils::combinePath(instance.directory(), "manifest.json"));
}

void Pack::load(QString manifest) {
    QFile manifestFile(manifest);
    if (!manifestFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Could not open" << manifest;
        return;
    }
    QJsonObject manifestObject = QJsonDocument::fromJson(manifestFile.readAll()).object();
    manifestFile.close();

    QJsonObject m_info = manifestObject["info"].toObject();
    info->title = m_info["title"].toString();
    info->desc = m_info["desc"].toString();

    QJsonArray authors = m_info["authors"].toArray();
    for (int i = 0; i < authors.size(); i++)
        info->authors << authors[i].toString();
    info->version = m_info["version"].toString();
    info->id = m_info["id"].toString();
    info->source = static_cast<OneMeta::SourceType>(m_info["source"].toInt());
    info->release = m_info["release"].toInt();
    minecraft = manifestObject["minecraft"].toObject()["version"].toString();
    forge = manifestObject["forge"].toObject()["version"].toString();

    for (QJsonValue value : manifestObject["mods"].toArray()) {
        auto c_mod = value.toObject();
        auto *n_mod = new PackMod;
        n_mod->source = static_cast<OneMeta::SourceType>(c_mod["source"].toInt());
        n_mod->file = c_mod["file"].toInt();
        n_mod->project = c_mod["project"].toInt();
        n_mod->hash = c_mod["sha1"].toString();
        mods.insert(n_mod->project, n_mod);
        auto file = OneMeta::getDatabase().findFile(n_mod->source, n_mod->file);
        if (file)
            modHashes.insert(n_mod->hash, file);
    }

    for (QJsonValue value : manifestObject["files"].toArray()) {
        auto c_file = value.toObject();
        PackFile n_file;
        n_file.dest = c_file["dest"].toString();
        n_file.source = c_file["source"].toString();
        n_file.hash = c_file["sha1"].toString();
        files.insert(n_file.hash, n_file);
    }
}

void Pack::save(QString manifest) {
    QJsonObject manifestObject;

    if (info) {
        QJsonObject m_info;
        m_info["title"] = info->title;
        m_info["desc"] = info->desc;
        manifestObject["authors"] = QJsonArray::fromStringList(info->authors);
        m_info["version"] = info->version;
        m_info["id"] = info->id;
        m_info["source"] = info->source;
        m_info["release"] = info->release;
        manifestObject["info"] = m_info;
    }

    QJsonObject m_mc;
    m_mc["version"] = minecraft;
    manifestObject["minecraft"] = m_mc;

    QJsonObject m_forge;
    m_forge["version"] = forge;
    manifestObject["forge"] = m_forge;

    QJsonArray m_mods;
    QJsonObject c_mod;
    for (PackMod *n_mod : mods.values()) {
        c_mod["source"] = n_mod->source;
        c_mod["file"] = n_mod->file;
        c_mod["project"] = n_mod->project;
        c_mod["sha1"] = n_mod->hash;
        m_mods.append(c_mod);
    }
    manifestObject["mods"] = m_mods;

    QJsonArray m_files;
    QJsonObject c_file;
    for (PackFile n_file : files.values()) {
        c_file["dest"] = n_file.dest;
        c_file["source"] = n_file.source;
        c_file["sha1"] = n_file.hash;
        m_files.append(c_file);
    }
    manifestObject["files"] = m_files;

    QFile manifestFile(manifest);
    if (!manifestFile.open(QIODevice::WriteOnly)) {
        qWarning() << "Could not open" << manifest;
        return;
    }
    QJsonDocument doc(manifestObject);
    manifestFile.write(doc.toJson());
    manifestFile.close();
}

QVector<QString> Pack::filterFiles(QString filter) {
    QVector<QString> files;
    for (auto f : this->files.values()) {
        if (f.dest.startsWith(filter)) {
            QFileInfo info(f.dest);
            files << info.fileName();
        }
    }
    return files;
}
