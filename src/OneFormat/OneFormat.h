/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "OneMeta/OneMeta.h"

#include <QMap>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QVector>
#include <QtWidgets>

class MinecraftInstance;
class Config;
class HearthAnalytics;

namespace OneFormat {

struct PackInfo {
    OneMeta::SourceType source = static_cast<OneMeta::SourceType>(0);
    QString icon = QString();
    QString title = QString();
    QString desc = QString();
    QString displayVersion = QString();
    QString id = QString();
    QString version = QString();
    QStringList authors = QStringList();
    int release = 0;
};

struct PackMod {
    OneMeta::SourceType source = static_cast<OneMeta::SourceType>(0);
    int project = 0;
    int file = 0;
    QString hash;
};

struct PackFile {
    QString source;
    QString dest;
    QString hash;
};

class Pack : public QObject {
    Q_OBJECT
public:
    Pack(Config &conf, HearthAnalytics &hash);
    Pack(Config &conf, HearthAnalytics &hash, MinecraftInstance *instance);
    Pack(QString manifest, Config &conf, HearthAnalytics &ha, OneMeta::FilePtr file);
    void load(QString manifest);
    void save(QString manifest);
    void install();
    void update();
    void exportPack(const QString &zip);

    PackInfo *info = new PackInfo;
    QString minecraft = QString();
    QString forge = QString();
    OneMeta::FilePtr file = nullptr;
    QMap<int, PackMod *> mods;
    QMap<QString, OneMeta::FilePtr> modHashes;
    QMap<QString, PackFile> files;

    QVector<QString> filterFiles(QString filter);

    void addMod(OneMeta::FilePtr file);
    void removeMod(OneMeta::FilePtr file);

signals:
    void signalMax(int max);
    void signalProgress(int progress);
    void signalMessage(QString message);
    void installed();

private:
    Config &config;
    HearthAnalytics &tracker;
    MinecraftInstance *m_instance = nullptr;
};
typedef Pack *PackPtr;
} // namespace OneFormat
