#pragma once

#include <utility>

#include "OneFormat/OneFormat.h"
#include "OneFormat/PackHandler.h"

class OFHandler : public PackHandler {
public:
    OFHandler(QString directory);
    QString dir;
    OneFormat::PackPtr readPack() override;
};
