#include "OFHandler.h"
#include "Analytics/HearthAnalytics.h"
#include "OneMeta/OneMeta.h"

OFHandler::OFHandler(QString directory) : dir(std::move(directory)) {}

OneFormat::PackPtr OFHandler::readPack() {
    auto pack = new OneFormat::Pack(Config::instance(), HearthAnalytics::instance());
    pack->load(Utils::combinePath(dir, "manifest.json"));
    return pack;
}
