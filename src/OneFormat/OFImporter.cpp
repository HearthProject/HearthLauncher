#include "OFImporter.h"
#include "OFHandler.h"
#include "Minecraft/BasePack.h"
#include <OneFormat/PackImporter.h>
#include <QFileInfo>

OFImporter::OFImporter(QWidget *parent, Config &config, HearthAnalytics &tracker)
    : PackImporter(tr("Select a OneFormat Import file"),
                   QStandardPaths::writableLocation(QStandardPaths::DownloadLocation), tr("1f (*.1f)"), parent),
      config(config), tracker(tracker) {}

bool OFImporter::import(const QString &name) {
    if (file.isEmpty() || file.isNull())
        return false;
    QFileInfo info(file);

    QString dir(config.cache_dir + "/packs/" + info.baseName());
    Utils::unzipFolder(file, dir);
    OFHandler *importer = new OFHandler(dir);
    OneFormat::PackPtr packptr = importer->readPack();
    if (!name.isEmpty())
        packptr->info->title = name;
    auto *pack = new BasePack(config, tracker, packptr, parent);
    if (pack) {
        QProgressDialog dia(parent);
        dia.setWindowTitle(packptr->info->title);
        connect(pack, SIGNAL(signalMessage(QString)), &dia, SLOT(setLabelText(QString)));
        connect(pack, SIGNAL(signalProgress(int)), &dia, SLOT(setValue(int)));
        connect(pack, SIGNAL(signalMax(int)), &dia, SLOT(setMaximum(int)));
        connect(pack, SIGNAL(installed()), this, SIGNAL(installed()));
        pack->install();
        return true;
    }
    return false;
}
