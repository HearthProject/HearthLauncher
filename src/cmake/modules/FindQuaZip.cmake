# QUAZIP_FOUND               - QuaZip library was found
# QUAZIP_INCLUDE_DIR         - Path to QuaZip include dir
# QUAZIP_INCLUDE_DIRS        - Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES           - List of QuaZip libraries
# QUAZIP_ZLIB_INCLUDE_DIR    - The include dir of zlib headers


if (QUAZIP_INCLUDE_DIRS AND QUAZIP_LIBRARIES)
    # in cache already
    set(QUAZIP_FOUND TRUE)
else()
    if (WIN32)
        if (QUAZIP_STATIC)
            find_library(QUAZIP_LIBRARY_RELEASE
                NAMES quazip_static
                HINTS ${QUAZIP_LIBRARY_DIR})
            find_library(QUAZIP_LIBRARY_DEBUG
                NAMES quazip_staticd
                HINTS ${QUAZIP_LIBRARY_DIR})
            set(QUAZIP_LIBRARIES
                optimized ${QUAZIP_LIBRARY_RELEASE}
                debug ${QUAZIP_LIBRARY_DEBUG})
        else()
            find_library(QUAZIP_LIBRARIES
                WIN32_DEBUG_POSTFIX d
                NAMES quazip5 quazip
                HINTS ${QUAZIP_LIBRARY_DIR})
        endif()
        find_path(QUAZIP_INCLUDE_DIR
            NAMES quazip.h
            HINTS ${QUAZIP_LIBRARY_DIR}/../include/quazip5)
        find_path(QUAZIP_ZLIB_INCLUDE_DIR
            NAMES zlib.h
            HINTS ${ZLIB_INCLUDE_DIR})

    else()
        find_package(PkgConfig QUIET)
        pkg_check_modules(PC_QUAZIP QUIET quazip)
        if (QUAZIP_STATIC)
            find_library(QUAZIP_LIBRARIES
                NAMES quazip5.a quazip.a
                HINTS /usr/lib /usr/lib64 /usr/local/lib)
        else()
            find_library(QUAZIP_LIBRARIES
                NAMES quazip5 quazip
                HINTS /usr/lib /usr/lib64 /usr/local/lib)
        endif()
        find_path(QUAZIP_INCLUDE_DIR quazip.h
            HINTS /usr/include /usr/local/include
            PATH_SUFFIXES quazip5 quazip)
        find_path(QUAZIP_ZLIB_INCLUDE_DIR
            zlib.h
            HINTS ${ZLIB_INCLUDE_DIR} /usr/include /usr/local/include)
    endif()

    include(FindPackageHandleStandardArgs)
    set(QUAZIP_INCLUDE_DIRS ${QUAZIP_INCLUDE_DIR} ${QUAZIP_ZLIB_INCLUDE_DIR})
    find_package_handle_standard_args(QUAZIP DEFAULT_MSG QUAZIP_LIBRARIES
        QUAZIP_INCLUDE_DIR QUAZIP_ZLIB_INCLUDE_DIR QUAZIP_INCLUDE_DIRS)
        
    if (QuaZip_FIND_REQUIRED AND NOT QUAZIP_FOUND)
        message(SEND_ERROR "Failed to find required package QuaZip.")
    endif()
endif()
