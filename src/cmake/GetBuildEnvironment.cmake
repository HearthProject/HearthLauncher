if (git_executable)
    execute_process(COMMAND ${git_executable} rev-parse --short HEAD
        WORKING_DIRECTORY ${SOURCE_DIR}
        TIMEOUT 5
        RESULT_VARIABLE git_result
        OUTPUT_VARIABLE git_output)
    if (${git_result} EQUAL 0)
        string(REPLACE "\n" "" git_output ${git_output})
        set(GIT_HASH ${git_output})
    endif()
    execute_process(COMMAND ${git_executable} rev-parse --abbrev-ref HEAD
        WORKING_DIRECTORY ${SOURCE_DIR}
        TIMEOUT 5
        RESULT_VARIABLE git_result
        OUTPUT_VARIABLE git_output)
    if (${git_result} EQUAL 0)
        string(REPLACE "\n" "" git_output ${git_output})
        set(GIT_BRANCH ${git_output})
    endif()
endif()

file(READ ${SOURCE_DIR}/VERSION VERSION_NUM)
string(STRIP ${VERSION_NUM} VERSION_NUM)

if ("$ENV{HL_JOB_ID}" STREQUAL "")
    set(HL_JOB_ID -1)
else()
    set(HL_JOB_ID $ENV{HL_JOB_ID})
endif()

if ("$ENV{HL_BUILDER}" STREQUAL "")
    set(HL_BUILDER "unknown")
else()
    set(HL_BUILDER $ENV{HL_BUILDER})
endif()

set(OUTPUT
    "#define GIT_HASH \"${GIT_HASH}\"\n"
    "#define GIT_BRANCH \"${GIT_BRANCH}\"\n"
    "#define M_JOB_ID ${HL_JOB_ID}\n"
    "#define BUILDER \"${HL_BUILDER}\"\n"
    "#define VERSION_NUM ${VERSION_NUM}\n"
    )
file(WRITE BuildEnvironment.h.txt ${OUTPUT})

execute_process(COMMAND ${CMAKE_COMMAND} -E copy_if_different BuildEnvironment.h.txt BuildEnvironment.h)
