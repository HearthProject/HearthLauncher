/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QMap>
#include <QMultiMap>
#include <QString>
#include <QVector>

struct BasicForgeVersion {
    QString releaseTime;
    QString sha256;
    QString version;
    QMap<QString, QString> requirements;
};

class ForgeVersions;
extern ForgeVersions *forgeVersions;

class ForgeVersions {
public:
    ForgeVersions();

    static ForgeVersions *instance() { return forgeVersions; }

    QMultiMap<QString, BasicForgeVersion> versions;
    void loadFromFile(QString file);
    QStringList getAvailableVersions(const QString &minecraft);
};

struct ForgeLibrary {
    QString name;
    QString url;
    bool packxz;
};

class ForgeVersion {
public:
    ForgeVersion();

    void loadFromFile(QString file);
    QString name;
    QString mainClass;
    QString releaseTime;
    QString version;
    QMap<QString, QString> requirements;
    QVector<QString> tweakers;
    QVector<ForgeLibrary> libraries;
};

class Artifact {
public:
    Artifact(QString d);
    QString descriptor;
    QString path;
    QString ext;
    QString classifier;
    QString file;
    QString name;
    QString version;
    QString domain;
    QString file_path;
};
