/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MinecraftInstance.h"

#include "AuthSession.h"
#include "GUI/Main/AuthWidget.h"
#include "Minecraft/MinecraftVersions.h"
#include "Util/DownloadUtil.h"
#include "Util/Installer.h"

#include "BasePack.h"
#include "FileMod.h"
#include "MetaMod.h"
#include "GUI/Dialogs/FileDialog.h"

MinecraftInstance::MinecraftInstance(QString instanceName, Config &conf, HearthAnalytics &ha, QObject *parent)
    : Messenger(parent), config(conf), tracker(ha), process(new QProcess()),
      m_baseDir(Utils::combinePath(config.instance_dir, instanceName)),
      m_mcDir(Utils::combinePath(m_baseDir, "minecraft")) {

    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(outputChanged()));
    connect(process, SIGNAL(readyReadStandardError()), this, SLOT(outputChanged()));

    QObject::connect(process, &QProcess::errorOccurred, this,
                     [&](QProcess::ProcessError) { emit(log(process->errorString())); });

    connect(process, SIGNAL(finished(int)), this, SLOT(close(int)));

    auto manifest = Utils::combinePath(m_baseDir, "manifest.json");
    if (Utils::fileExists(manifest)) {
        m_pack->load(manifest);
    }
}

bool MinecraftInstance::update(bool simulate, QWidget *parent) {
    int id = m_pack->info->id.toInt();
    OneMeta::ProjectPtr project = OneMeta::getDatabase().findProject(OneMeta::CURSE, id);
    BasePack *packUpdate =
        FileDialog::getFilePack(config, this->tracker, project, false, m_pack->info->release, "", parent);

    if (simulate)
        return packUpdate != nullptr;

    if (packUpdate &&
        QMessageBox::question(
            parent, tr("Update Confirmation"),
            QString(tr("Would you like to update %1 to %2?")).arg(project->title, packUpdate->m_file->displayname),
            QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
        packUpdate->update();
        return true;
    } else {
        QMessageBox::information(parent, tr("Update Notification"), tr("No Updates Available"));
        return false;
    }
}

QSettings *MinecraftInstance::settings() const {
    return new QSettings(Utils::combinePath(m_baseDir, "instance.ini"), QSettings::IniFormat);
}

OneFormat::PackPtr MinecraftInstance::pack() const { return m_pack; }

void MinecraftInstance::setupInstance(QString minecraft, QString forge) {
    setMinecraft(minecraft);
    setForge(forge);
    QSettings *s = settings();
    s->setValue("instance/dir_name", QFileInfo(m_baseDir).fileName());
    s->setValue("instance/name", QFileInfo(m_baseDir).baseName());
    s->setValue("instance/time_created", QDateTime::currentMSecsSinceEpoch());
    s->setValue("instance/id", Utils::generateUUID());
    s->setValue("instance/playtime", 0);
    s->setValue("instance/icon", "default");
    s->setValue("modpack/id", -1);
    QDir().mkpath(m_mcDir);
    QDir().mkpath(m_mcDir + "/mods");
    m_pack = new OneFormat::Pack(config, tracker, this);
    m_pack->save(Utils::combinePath(directory(), "manifest.json"));
}

void MinecraftInstance::setMinecraft(QString minecraft) {
    settings()->setValue("minecraft/version", minecraft);
    emit(propertiesChanged(this));
}
void MinecraftInstance::setForge(QString forge) {
    settings()->setValue("minecraft/forge", forge);
    emit(propertiesChanged(this));
}

void MinecraftInstance::setIcon(QString icon) {
    settings()->setValue("instance/icon", icon);
    emit(signalIcon(QIcon(config.cache_dir + "/icons/" + icon)));
    emit(propertiesChanged(this));
}

void MinecraftInstance::setName(QString name) {
    settings()->setValue("instance/name", name);
    emit(signalName(name));
    emit(propertiesChanged(this));
}

QString MinecraftInstance::minecraft() const { return settings()->value("minecraft/version", "").toString(); }

QString MinecraftInstance::forge() const {
    return settings()->value("minecraft/forge", "").toString().replace("None", QString());
}

QIcon MinecraftInstance::icon() const {
    QString icon = config.cache_dir + "/icons/" + settings()->value("instance/icon", "default.svg").toString();
    if (!Utils::fileExists(icon)) {
        icon = ":/icons/hearth.png";
    }
    return QIcon(icon);
}

QString MinecraftInstance::name() const { return settings()->value("instance/name", "Unnamed Instance").toString(); }

QString MinecraftInstance::id() const { return settings()->value("instance/dir_name", name()).toString(); }

QString MinecraftInstance::directory() const {
    QString dir = Utils::combinePath(config.instance_dir, id());
    return dir.replace("%20", " ");
}

QString MinecraftInstance::getSubDir(QString sub) const {
    return Utils::combinePath(directory(), QString("/minecraft/%1/").arg(sub));
}

int MinecraftInstance::lastLauched() const {
    return settings()->value("instance/last_launched", settings()->value("instance/time_created", 0)).toInt();
}

void MinecraftInstance::launch(AuthSession &authSession) {
    if (process->isOpen() || process->state() == QProcess::Running)
        return;
    emit(preLaunch());

    log(QString(tr("Instance") + ":%1").arg(name()));
    log(QString(tr("Minecraft") + ":%1").arg(minecraft()));
    if(!forge().isEmpty())
        log(QString(tr("Forge") + ":%1").arg(forge()));


    QString version_number = minecraft();
    QString mcver = Utils::combinePath(Utils::combinePath(config.data_dir, "versions"), version_number + ".json");
    DownloadUtil dl;
    if (!Utils::fileExists(mcver)) {
        BasicMinecraftVersion ver = MinecraftVersions::instance()->versions[version_number];
        dl.downloadFile(ver.url, mcver);
    }

    MinecraftVersion m_ver;
    m_ver.loadFromFile(mcver);

    QStringList libraries;
    QString mainClass = m_ver.main_class;

    QString forge_version = forge();

    // Installing Forge
    qDebug() << "Installing Forge" << forge();

    ForgeInstaller forgeInstaller(forge_version, config.data_dir + "/libraries/", config.data_dir + "/libraries/",
                                  config);
    forgeInstaller.connectProgress(this);

    if (forgeInstaller.install()) {
        mainClass = forgeInstaller.mainClass;
        libraries << forgeInstaller.libraries;
    }

    // Installing Minecraft Libraries
    LibraryInstaller libraryInstaller(m_ver, config.data_dir + "/libraries/", config);
    libraryInstaller.connectProgress(this);

    if (libraryInstaller.install()) {
        libraries << libraryInstaller.libraries;
    }

    // Installing Assets
    QString mcassets = config.cache_dir + "/assets/indexes/" + m_ver.asset_index.id + ".json";
    if (!Utils::fileExists(mcassets)) {
        dl.downloadFile(m_ver.asset_index.url, mcassets);
    }

    MinecraftAssets m_assets;
    m_assets.loadFromFile(mcassets);

    AssetInstaller assetInstaller(m_assets, config);
    assetInstaller.connectProgress(this);
    assetInstaller.install();

    signalMessage(tr("Downloading Minecraft"));
    signalProgress(0);
    signalMax(1);

    QString jar_dir = Utils::combinePath(Utils::combinePath(config.data_dir, "versions"), m_ver.id);;
    QString jar_loc = Utils::combinePath(jar_dir, m_ver.id + ".jar");
    if (!Utils::fileExists(jar_loc)) {
        QDir().mkpath(jar_dir);
        dl.downloadFile(m_ver.client.url, jar_loc);
    }

    QStringList arguments;

    QSettings settings(config.config_file, QSettings::IniFormat);
    QString java = settings.value("java/path").toString();
    QString wrapper = settings.value("java/wrapper").toString();
    QString run = java;
    if (!wrapper.isEmpty()) {
        run = wrapper;
        arguments << "java";
    }

    QString natives_dir = config.data_dir % "/natives/" % m_ver.id;
    // Natives, needs to be the first argument
    arguments << "-Djava.library.path=" % natives_dir;
    // For debugging purposes
#ifdef HL_DEBUG
//    arguments << "-XshowSettings:properties";
#endif
    QString min_ram = settings.value("java/min_ram").toString() % "m";
    QString max_ram = settings.value("java/max_ram").toString() % "m";
    arguments << "-Xms" % min_ram;
    arguments << "-Xmx" % max_ram;
    QString custom_args(settings.value("java/args").toString());
    if (!custom_args.isEmpty()) {
        arguments.append(custom_args.split(" "));
    }
    arguments << "-cp" << libraries.join(config.seperator) + config.seperator + jar_loc;
    arguments << mainClass;

    QString assetsDir = config.cache_dir % "/assets";
    QString m_args = m_ver.args;
    m_args.replace("${auth_player_name}", authSession.user().name);
    m_args.replace("${version_name}", m_ver.id);
    m_args.replace("${game_directory}", m_mcDir.replace(" ", "%20"));
    // assets
    m_args.replace("${assets_root}", assetsDir.replace(" ", "%20"));
    m_args.replace("${game_assets}", assetsDir.replace(" ", "%20"));
    m_args.replace("${assets_index_name}", m_ver.asset_index.id);
    m_args.replace("${auth_uuid}", authSession.user().id);
    if (!config.offline)
        m_args.replace("${auth_access_token}", authSession.accesstoken());
    m_args.replace("${user_type}", (authSession.user().legacy ? "legacy" : "mojang"));
    m_args.replace("${version_type}", "Hearth%20Launcher");
    m_args.replace("${user_properties}", "[]");
    QStringList user_args(m_args.split(" "));
    for (int i = 0; i < user_args.size(); i++) {
        user_args[i] = user_args[i].replace("%20", " ");
    }
    arguments.append(user_args);
    if (!forge_version.isEmpty()) {
        arguments << "--tweakClass" << forgeInstaller.forge.tweakers[0];
    }

    log(QString("%1 %2 %3 %4 %5").arg(run, min_ram, max_ram, custom_args, user_args.join(" ")));

    this->settings()->setValue("instance/last_launched", Utils::secondsNow());

    process->setWorkingDirectory(getSubDir());
    process->start(run, arguments);
}

void MinecraftInstance::close(int exitCode) {
    qInfo() << "Instance exited with code " << exitCode;
    QSettings settings(m_baseDir + QDir::separator() + "instance.ini", QSettings::IniFormat);

    int launched = settings.value("instance/last_launched", Utils::secondsNow()).toInt();
    int time = Utils::secondsNow() - launched;
    int previous = settings.value("instance/playtime", 0).toInt();
    settings.setValue("instance/playtime", previous + time);
    emit changeTimePlayed(time);
}

void MinecraftInstance::outputChanged() {
    QString stout = process->readAllStandardOutput();
    QString sterr = process->readAllStandardError();
    if (!stout.isEmpty())
        log(stout.mid(0, stout.length() - 1));
    if (!sterr.isEmpty())
        log(sterr.mid(0, sterr.length() - 1));
}

void MinecraftInstance::remove() {
    if (!QDir(m_baseDir).removeRecursively())
        qCritical() << "Failed to remove directory" << m_baseDir;
    emit instanceRemoved();
}

void MinecraftInstance::kill() {
    if (process->isOpen()) {
        process->kill();
    }
}

void MinecraftInstance::debugDump(QString log) {
    QJsonObject dat;
    dat["OS_NAME"] = tracker.OS_NAME;
    dat["LOG"] = log;

    QJsonObject inst;
    inst["VERSION"] = minecraft();
    inst["FORGE"] = minecraft();
    inst["FOLDER"] = m_baseDir;

    QJsonObject set;
    set["DATA_DIR"] = config.data_dir;
    set["CACHE_DIR"] = config.cache_dir;
    set["CONFIG_DIR"] = config.config_dir;

    QJsonArray files;
    QStringList foundFiles;
    Utils::findFiles(m_baseDir, &foundFiles);
    for (QString f : foundFiles) {
        files.append(f + " : " + Utils::getChecksum(f));
    }

    dat["CONFIG"] = set;
    dat["INSTANCE"] = inst;
    dat["FILES"] = files;

    QString filename =
        QFileDialog::getSaveFileName(nullptr, tr("Save Debug Dump"), "", tr("HearthLauncher Dump (*.hldump)"));
    QFile outf(filename);
    if (!outf.open(QIODevice::WriteOnly)) {
        qWarning() << filename << " Could not be opened";
        return;
    }
    outf.write(QJsonDocument(dat).toJson());
    outf.close();
}

QVector<BaseModPtr> MinecraftInstance::mods() {
    QVector<BaseModPtr> mods;
    QStringList files;
    Utils::findFiles(getSubDir("mods"), &files);
    for (auto f : files) {
        QFileInfo info(f);
        auto hash = Utils::getChecksum(f);
        if (this->pack()->modHashes.contains(hash)) {
            auto mod = new MetaMod(this, this->pack()->modHashes.value(hash));
            mods.append(mod);
        } else {
            auto mod = new FileMod(this, info.fileName());
            mods.append(mod);
        }
    }

    return mods;
}

void MinecraftInstance::installServer(const QString &dir) {
    if (!Utils::fileExists(dir))
        QDir().mkpath(dir);
    DownloadUtil dl;
    dl.connectProgress(this);

    QString version_number = minecraft();
    QString forge_version = forge();
    QString mcver = config.cache_dir + "/manifests/" + version_number + ".json";
    signalTitle(tr("Installing Minecraft Server ") + version_number);

    if (!Utils::fileExists(mcver)) {
        BasicMinecraftVersion ver = MinecraftVersions::instance()->versions[version_number];
        signalMessage(tr("Downloading Version Manifest ") + version_number);
        dl.downloadFile(ver.url, mcver);
    }

    MinecraftVersion m_ver;
    m_ver.loadFromFile(mcver);

    QString jar_loc = Utils::combinePath(dir, "minecraft_server." + version_number + ".jar");
    if (!Utils::fileExists(jar_loc)) {
        QDir dir = QFileInfo(jar_loc).absoluteDir();
        if (!dir.exists())
            dir.mkpath(dir.absolutePath());
        signalMessage(tr("Downloading Server Jar ") + version_number);
        dl.downloadFile(m_ver.server.url, jar_loc);
    }

    // Installing Forge
    ForgeInstaller forgeInstaller(forge_version, dir, Utils::combinePath(dir, "libraries"), config, true);
    forgeInstaller.connectProgress(this);
    forgeInstaller.install();

    // Installing Minecraft Libraries
    LibraryInstaller libraryInstaller(m_ver, Utils::combinePath(dir, "libraries"), config);
    libraryInstaller.connectProgress(this);
    libraryInstaller.install();

    QFile eula(Utils::combinePath(dir, "eula.txt"));
    if (!eula.open(QFile::WriteOnly)) {
        qWarning() << "Could not open eula.txt";
    }
    eula.write("eula=true");

    Utils::zipFolder(dir, Utils::combinePath(dir, name() + ".zip"));
}
