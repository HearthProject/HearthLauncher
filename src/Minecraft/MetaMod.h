
#pragma once

#include <utility>

#include "BaseMod.h"
#include "MinecraftInstance.h"
class MetaMod : public BaseMod {
    Q_OBJECT
public:
    MetaMod(MinecraftInstance *instance, OneMeta::FilePtr file);

    QString install() override;
    QString update(QWidget *parent) override;

    OneMeta::FilePtr m_file = nullptr;

    QString name() override;

    QString file(bool enabled = true) override;

    int versionsBehind() override;

    void remove() override;

    bool canUpdate() override { return true; }

    void setResolveDependencies(bool value) { resolveDependencies = value; }
    bool resolveDependencies;
signals:
    void installed();
};
