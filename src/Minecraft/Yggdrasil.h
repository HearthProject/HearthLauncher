/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Minecraft/AuthSession.h"
#include <QJsonObject>
#include <QString>
#include <QtWidgets>
#include <QVector>

namespace Auth_Constants {
const QUrl AUTH_ENDPOINT("https://authserver.mojang.com/authenticate");
const QUrl REAUTH_ENDPOINT("https://authserver.mojang.com/refresh");
const QUrl VALIDATE_ENDPOINT("https://authserver.mojang.com/validate");
const QUrl INVALIDATE_ENDPOINT("https://authserver.mojang.com/invalidate");
} // namespace Auth_Constants

namespace Yggdrasil {

struct Service {
    QColor m_color;
    QString m_name;
    QString m_url;
    Service() {}
    explicit Service(QString name, QString url, QColor color): m_color(color), m_url(url), m_name(name) {}
};

QJsonDocument login(const QString &username, const QString &password);

QJsonDocument refresh(const QString &accessToken, const QString &clientToken, const User &profile);
QJsonDocument validate(const QString &accessToken, const QString &clientToken);

void invalidate(const QString &accessToken, const QString &clientToken);

QVector<Service> checkStatus();

}// namespace Yggdrasil
