
#include "BasePack.h"
#include "Curse/CurseHandler.h"
#include <Analytics/HearthAnalytics.h>
#include <Util/DownloadUtil.h>
#include <QProgressDialog>

BasePack::BasePack(Config &config, HearthAnalytics &tracker, OneMeta::FilePtr file, QWidget *parent)
    : config(config), m_file(std::move(file)), parent(parent), tracker(tracker) {}

BasePack::BasePack(Config &config, HearthAnalytics &tracker, OneFormat::PackPtr pack, QWidget *parent)
    : config(config), pack(pack), parent(parent), tracker(tracker) {}

void BasePack::download() {
    emit(signalMessage(tr("Downloading Pack Data")));
    DownloadUtil dl;
    dl.connectProgress(this);
    emit(signalMessage(tr("Downloading ") + m_file->filename));
    tracker.sendEvent(HearthEventType::PACK_INSTALLED, m_file->project);
    PackHandler *handler = nullptr;

    OneMeta::SourceType type = m_file->sourceType;
    switch (type) {
    case OneMeta::CURSE:
        QString base(config.cache_dir + "/packs/" + QString::number(m_file->id));
        QString zip(base + ".zip");
        dl.downloadFile(m_file->url, zip);
        Utils::unzipFolder(zip, base);
        handler = new CurseHandler(base, m_file->project, m_file->id);
        break;
    }

    if (handler) {
        pack = handler->readPack();
        pack->file = m_file;
        OneFormat::PackInfo *info = pack->info;
        info->id = QString::number(m_file->project);
        info->title = OneMeta::getDatabase().findProject(type, m_file->project)->title;
    }
}

void BasePack::install() {
    if (pack) {
        connect(pack, SIGNAL(signalMax(int)), this, SLOT(setMaximum(int)));
        connect(pack, SIGNAL(signalProgress(int)), this, SLOT(setValue(int)));
        connect(pack, SIGNAL(signalMessage(QString)), this, SLOT(setMessage(QString)));
        pack->install();
        emit(installed());
        QMessageBox::information(parent, tr("Pack Notification"), QString(tr("Finished Installing %1")).arg(pack->info->title));
    } else {
        QMessageBox::warning(parent, tr("Pack Error"), tr("Pack handler missing. Failed to install pack."));
    }
}

void BasePack::update() {
    if (pack) {
        connect(pack, SIGNAL(signalMax(int)), this, SLOT(setMaximum(int)));
        connect(pack, SIGNAL(signalProgress(int)), this, SLOT(setValue(int)));
        connect(pack, SIGNAL(signalMessage(QString)), this, SLOT(setMessage(QString)));
        pack->update();
        emit(installed());
        QMessageBox::information(parent, tr("Pack Notification"), QString(tr("Finished Updating %1")).arg(pack->info->title));

    } else {
        QMessageBox::warning(parent, tr("Pack Error"), tr("Error: No Update Found"));
    }
}
