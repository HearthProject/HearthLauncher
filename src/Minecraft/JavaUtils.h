/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QVector>

class Config;
class JavaInstall {
public:
    JavaInstall() {}
    JavaInstall(QString path, QString arch = "64", QString version = "") {
        this->path = path;
        this->arch = arch;
        this->version = version;
    }
    QString version;
    QString path;
    QString arch;
};

class JavaUtils : public QObject {
Q_OBJECT
public:
    static void setJavaExecutable(Config &config, const QString& path);
    static QVector<JavaInstall> getInstallLocations();
    static JavaInstall getDefaultInstall();
    static bool verifyJava(QString path, bool dialog, QWidget *parent = nullptr);
    static QString selectJava(Config &config, QWidget *parent = nullptr);
};
