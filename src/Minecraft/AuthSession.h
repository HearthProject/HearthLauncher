/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QJsonObject>
#include <QMap>
#include <QPixmap>
#include <QString>

class Config;

struct User {
    QString id;
    QString name;
    bool legacy;
};

class AuthSession : public QObject {
    Q_OBJECT
public:
    explicit AuthSession(Config &config, QObject *parent = nullptr);

    void write(QString file);
    void load(QString authFile);
    void read(QJsonObject json);
    void refresh();
    void invalidate();
    bool validateToken();

    bool valid() const;
    QString accesstoken() const;
    User user() const;
    QString file() const;

    void login();
    void logout();

public slots:
    bool auth();

signals:
    void signalAuthenticate(QString state, QString skin, QString role);

private:
    bool is_valid = false;
    QString access_token;
    QString client_token;
    User profile;

    Config &config;
};
