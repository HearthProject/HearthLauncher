//
// Created by tyler on 12/11/17.
//

#include "MetaMod.h"
#include "MinecraftInstance.h"
#include "GUI/Dialogs/FileDialog.h"
#include <Util/DownloadUtil.h>
MetaMod::MetaMod(MinecraftInstance *instance, OneMeta::FilePtr file) : BaseMod(instance), m_file(std::move(file)) {}

QString MetaMod::install() {
    auto file = m_file;
    if (!file) {
        qWarning() << "Missing mod file";
        return QString();
    }

    DownloadUtil dl;

    QString path = Utils::combinePath(m_instance->getSubDir("mods"), file->filename);

    if (!Utils::fileExists(path))
        dl.downloadFile(file->url, path);
    m_instance->pack()->addMod(file);
    emit installed();

    if (resolveDependencies) {
        for (OneMeta::Dependency dep : file->dependencies) {
            OneMeta::FilePtr depFile = OneMeta::database.findProject(file->sourceType, dep.project)->latest(m_instance->minecraft());
            if (depFile) {
                MetaMod mod(m_instance, depFile);
                mod.setResolveDependencies(resolveDependencies);
                mod.install();
            }
        }
    }

    return path;
}

QString MetaMod::update(QWidget *parent) {
    qInfo() << "Checking for mod updates";

    if (m_file) {
        auto p = OneMeta::getDatabase().findProject(m_file->sourceType, m_file->project);
        auto update = FileDialog::getFileMod(m_instance, m_instance->tracker, p, false, m_file->date,
                                             m_instance->minecraft(), parent);
        if (update && !update->install().isEmpty()) {
            remove();
        }
    }
    return QString();
}

int MetaMod::versionsBehind() {
    if (m_file) {
        auto p = OneMeta::getDatabase().findProject(m_file->sourceType, m_file->project);
        auto files = p->filelist(m_instance->minecraft(), m_file->date);
        return files.size();
    }
    return false;
}

QString MetaMod::name() { return m_file->displayname; }

QString MetaMod::file(bool enabled) {
    auto file = Utils::combinePath(m_instance->getSubDir("mods"), m_file->filename);
    if (!enabled)
        file += ".disabled";
    return file;
}

void MetaMod::remove() {
    m_instance->pack()->removeMod(m_file);
    QDir().remove(file());
}
