#pragma once

#include <utility>

#include "MinecraftInstance.h"
#include "Analytics/HearthAnalytics.h"

class BaseMod : public QObject {
    Q_OBJECT
public:
    explicit BaseMod(MinecraftInstance *instance);

    bool enabled();
    bool setEnabled(bool enabled);

    virtual QString file(bool enabled = true) = 0;
    virtual QString name() = 0;

    virtual QString update(QWidget *parent) = 0;
    virtual QString install() = 0;

    virtual int versionsBehind() = 0;

    virtual void remove() = 0;

    virtual bool canUpdate() = 0;

signals:
    void installed();

protected:
    MinecraftInstance *m_instance;
};

typedef BaseMod *BaseModPtr;
