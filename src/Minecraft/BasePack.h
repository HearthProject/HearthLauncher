#pragma once

#include "MinecraftInstance.h"
#include "OneFormat/OneFormat.h"
#include "OneMeta/OneMeta.h"
#include <utility>

class Config;
class HearthAnalytics;

class BasePack : public QObject {
    Q_OBJECT
public:
    BasePack(Config &config, HearthAnalytics &tracker, OneMeta::FilePtr file, QWidget *parent);
    BasePack(Config &config, HearthAnalytics &tracker, OneFormat::PackPtr pack, QWidget *parent);

    OneMeta::FilePtr m_file;
    OneFormat::PackPtr pack = nullptr;
    Config &config;
    HearthAnalytics &tracker;
    QWidget *parent;

    void download();
    void install();
    void update();

private slots:
    void setMaximum(int max) { emit(signalMax(max)); }
    void setValue(int value) { emit(signalProgress(value)); }
    void setMessage(QString message) { emit(signalMessage(message)); }

signals:
    void installed();
    void signalProgress(int p);
    void signalMessage(QString message);
    void signalMax(int max);
};

typedef BasePack *BasePackPtr;
