/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "GUI/Model/CFileSystemModel.h"
#include "OneFormat/OneFormat.h"
#include "OneMeta/OneMeta.h"
#include "Util/Messenger.h"

#include <QObject>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QtWidgets>

class AuthSession;
class Config;
class HearthAnalytics;
class BaseMod;

class MinecraftInstance : public Messenger {
    Q_OBJECT
public:
    MinecraftInstance(QString baseDir, Config &conf, HearthAnalytics &ha, QObject *parent = nullptr);

    void setupInstance(QString minecraft, QString forge);
    int lastLauched() const;

    QString minecraft() const;
    QString forge() const;
    QString name() const;
    QString id() const;
    QString directory() const;
    QIcon icon() const;
    QString getSubDir(QString sub = "") const;
    OneFormat::PackPtr pack() const;
    QVector<BaseMod *> mods();

    bool operator<(const MinecraftInstance &i) { return lastLauched() < i.lastLauched(); }

public slots:
    void setName(QString name);
    void setIcon(QString icon);
    void setMinecraft(QString minecraft);
    void setForge(QString forge);
    void launch(AuthSession &authSession);
    void installServer(const QString &dir);
    void outputChanged();
    void remove();
    void kill();
    void debugDump(QString log);
    bool update(bool simulate, QWidget *parent);

private slots:
    void close(int exitCode);

signals:
    void preLaunch();
    void changeTimePlayed(int time);
    void instanceRemoved();
    void signalIcon(QIcon icon);
    void signalName(QString name);
    void propertiesChanged(MinecraftInstance *inst);
    void log(QString message);

private:
    Config &config;
    HearthAnalytics &tracker;
    QString m_baseDir = QString();
    QString m_mcDir = QString();
    QProcess *process;
    OneFormat::PackPtr m_pack = new OneFormat::Pack{config, tracker, this};

    QSettings *settings() const;
    friend class MetaMod;
};
