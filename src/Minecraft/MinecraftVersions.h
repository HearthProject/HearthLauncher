/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QDateTime>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QVector>

class Config;

enum ReleaseType { RELEASE, SNAPSHOT, BETA, ALPHA };

class BasicMinecraftVersion {
public:
    QString id;
    ReleaseType type;
    QString release_time;
    QString url;
    bool operator<(BasicMinecraftVersion &other) const {
        return (QDateTime::fromString(release_time, Qt::ISODate) <
                QDateTime::fromString(other.release_time, Qt::ISODate));
    }
    bool operator>(BasicMinecraftVersion &other) const {
        return (QDateTime::fromString(release_time, Qt::ISODate) >
                QDateTime::fromString(other.release_time, Qt::ISODate));
    }
};

class MinecraftVersions;
extern MinecraftVersions *minecraftVersions;

class MinecraftVersions {
public:
    MinecraftVersions(Config &conf);

    static MinecraftVersions *instance() { return minecraftVersions; }

    void loadFromFile(QString file);
    QStringList getVersions();

    Config &config;
    QMap<QString, BasicMinecraftVersion> versions;
    QString latest_release;
    QString latest_snapshot;
};

struct Rule {
    bool allow;
    QStringList allowed;
};

struct MinecraftDownload {
    int size;
    QString sha1;
    QString path;
    QString url;
};

struct MinecraftFile {
    QString name;
    MinecraftDownload download;
    bool native;
    QVector<Rule> rules;
    QStringList excludes;
};

struct AssetIndex {
    QString id;
    QString sha1;
    QString url;
    int totalsize;
    int size;
};

class MinecraftVersion {
public:
    MinecraftVersion();

    void loadFromFile(QString file);

    QVector<MinecraftFile> libraries;

    int total_size;
    AssetIndex asset_index;
    MinecraftDownload client;
    MinecraftDownload server;
    QString id;
    QString main_class;
    QString args;
    QString release_time;
    ReleaseType type;
};

struct MinecraftAsset {
    QString hash;
    int size;
};

class MinecraftAssets {
public:
    void loadFromFile(QString file);
    QMap<QString, MinecraftAsset> objects;
};
