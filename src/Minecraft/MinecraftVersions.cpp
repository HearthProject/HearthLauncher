/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MinecraftVersions.h"

#include "Util/Config.h"
#include "Util/Instrumentation.h"

#include "Util/Utils.h"
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

MinecraftVersions *minecraftVersions;

MinecraftVersions::MinecraftVersions(Config &conf) : config(conf) {}

QStringList MinecraftVersions::getVersions() {

    QSettings settings(config.config_file, QSettings::IniFormat);
    bool snapshots = settings.value("snapshots", false).toBool();
    bool prerelease = settings.value("prerelease", false).toBool();

    QList<BasicMinecraftVersion> values;

    for (QString key : versions.keys()) {
        auto value = versions.value(key);
        if (!snapshots && (value.type == SNAPSHOT))
            continue;
        if (!prerelease && (value.type == BETA || value.type == ALPHA))
            continue;
        values.append(value);
    }

    std::sort(values.begin(), values.end(), [](auto a, auto b) { return (a > b); });
    QStringList mcVersions;
    for (auto v : values)
        mcVersions.append(v.id);
    return mcVersions;
}

void MinecraftVersions::loadFromFile(QString file) {
    ScopedTask _("Load Mincraft Version Info");
    QFile versionsFile(file);
    versionsFile.open(QIODevice::ReadOnly);
    QJsonObject versionsObject = QJsonDocument::fromJson(versionsFile.readAll()).object();
    versionsFile.close();

    QJsonObject latest = versionsObject["latest"].toObject();
    latest_release = latest["release"].toString();
    latest_snapshot = latest["snapshot"].toString();

    QJsonArray c_versions = versionsObject["versions"].toArray();
    QJsonObject current;
    QMap<QString, ReleaseType> types;
    types.insert("release", RELEASE);
    types.insert("snapshot", SNAPSHOT);
    types.insert("old_alpha", ALPHA);
    types.insert("old_beta", BETA);
    for (int i = 0; i < c_versions.size(); ++i) {
        BasicMinecraftVersion ver;
        current = c_versions[i].toObject();
        ver.id = current["id"].toString();
        ver.type = types[current["type"].toString()];
        ver.release_time = current["releaseTime"].toString();
        ver.url = current["url"].toString();
        versions.insert(ver.id, ver);
    }
}

MinecraftVersion::MinecraftVersion() {}

void MinecraftVersion::loadFromFile(QString file) {
    QFile versionFile(file);
    versionFile.open(QIODevice::ReadOnly);
    QJsonObject versionObject = QJsonDocument::fromJson(versionFile.readAll()).object();
    versionFile.close();

    QJsonObject downloads = versionObject["downloads"].toObject();
    QJsonObject j_client = downloads["client"].toObject();
    QJsonObject j_server = downloads["server"].toObject();

    client.sha1 = j_client["sha1"].toString();
    client.size = j_client["size"].toInt();
    total_size += client.size;
    client.url = j_client["url"].toString();

    server.sha1 = j_server["sha1"].toString();
    server.size = j_server["size"].toInt();
    server.url = j_server["url"].toString();

    id = versionObject["id"].toString();
    QJsonObject indexObject = versionObject["assetIndex"].toObject();
    asset_index = AssetIndex();
    asset_index.id = indexObject["id"].toString();
    asset_index.sha1 = indexObject["sha1"].toString();
    asset_index.url = indexObject["url"].toString();
    asset_index.totalsize = indexObject["totalsize"].toInt();
    asset_index.size = indexObject["size"].toInt();
    main_class = versionObject["mainClass"].toString();
    args = versionObject["minecraftArguments"].toString();
    release_time = versionObject["releaseTime"].toString();

    if (versionObject["type"] == "release")
        type = ReleaseType::RELEASE;
    else
        type = ReleaseType::SNAPSHOT;

    QJsonArray libs = versionObject["libraries"].toArray();
    QJsonObject clib;
    for (int i = 0; i < libs.size(); ++i) {
        clib = libs[i].toObject();

        MinecraftFile file;
        file.native = false;
        file.name = clib["name"].toString();

        MinecraftDownload dl;

        if (clib.contains("natives")) {
            if (!clib["natives"].toObject().contains(NATIVE_OS_NAME))
                continue;
            QString key = clib["natives"].toObject()[NATIVE_OS_NAME].toString();
            downloads = clib["downloads"].toObject()["classifiers"].toObject()[key].toObject();
            QJsonArray rulesObject(clib["rules"].toArray());
            file.native = true;
            for (int i = 0; i < rulesObject.size(); i++) {
                bool allow = rulesObject[i].toObject()["action"] == "allow" ? true : false;
                file.native = allow;
                if (rulesObject[i].toObject()["os"].toObject()["name"].toString() == OS_NAME)
                    file.native = allow;
            }
            QJsonArray exclude(clib["extract"].toObject()["exclude"].toArray());
            for (int i = 0; i < exclude.size(); i++) {
                file.excludes.append(exclude[i].toString());
            }

        } else {
            downloads = clib["downloads"].toObject()["artifact"].toObject();
        }

        dl.size = downloads["size"].toInt();
        total_size += dl.size;
        dl.sha1 = downloads["sha1"].toString();
        dl.url = downloads["url"].toString();
        dl.path = downloads["path"].toString();

        file.download = dl;

        libraries.append(file);
    }
}

void MinecraftAssets::loadFromFile(QString file) {
    QFile assetsFile(file);
    assetsFile.open(QIODevice::ReadOnly);
    QJsonObject assetsManifest = QJsonDocument::fromJson(assetsFile.readAll()).object();
    assetsFile.close();
    QJsonObject assetsObjects(assetsManifest["objects"].toObject());
    QStringListIterator iter(assetsObjects.keys());
    QJsonObject current;
    while (iter.hasNext()) {
        QString key(iter.next());
        current = assetsObjects[key].toObject();
        MinecraftAsset asset;
        asset.hash = current["hash"].toString();
        asset.size = current["size"].toInt();
        objects.insert(key, asset);
    }
}
