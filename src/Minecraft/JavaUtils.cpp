/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "JavaUtils.h"

#include "Util/Utils.h"

#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <QRegularExpression>
#include <QStringBuilder>
#include "GUI/Dialogs/JavaDialog.h"

void JavaUtils::setJavaExecutable(Config &config, const QString& path) {
    QSettings settings(config.config_file, QSettings::IniFormat);
    settings.setValue("java/path", path);
}

QString JavaUtils::selectJava(Config &config, QWidget *parent) {
    auto path = JavaDialog::getSelectPathed(parent);
    setJavaExecutable(config, path);
    return path;
}

bool JavaUtils::verifyJava(QString path, bool dialog, QWidget *parent) {
    if (path.isEmpty()) {
        QMessageBox::information(parent, tr("Java Verification"),
                                 tr("Please use Java Detection or set the path to a valid binary."));
        return false;
    }
    QProcess *process = new QProcess();
    process->start(path, QStringList("-version"));
    process->waitForFinished();
    QString output(process->readAllStandardError());
    QRegularExpression re("\"(.*?)\"", QRegularExpression::MultilineOption);
    auto match = re.match(output);
    if (match.hasMatch()) {
        if (dialog)
            QMessageBox::information(parent, tr("Java Verification"), tr("Path: ") % path % tr("\nVersion: ") % match.captured(1));
        return true;
    } else {
        if (dialog)
            QMessageBox::information(
                parent, tr("Java Verification"),
                tr("This Java Binary did not work. \n Please use Java Detection or set the path to a valid binary."));
        return false;
    }
}

JavaInstall JavaUtils::getDefaultInstall() {
#ifdef Q_OS_WIN
    return JavaInstall("javaw");
#else
    return JavaInstall("java");
#endif
}

QVector<JavaInstall> JavaUtils::getInstallLocations() {
    QVector<JavaInstall> installs;
    installs.append(JavaUtils::getDefaultInstall());
#ifdef Q_OS_LINUX
    qWarning() << "Linux Java detection incomplete - defaulting to \"java\"";
    auto scanJavaDir = [&](const QString &dirPath) {
        QDir dir(dirPath);
        if (!dir.exists())
            return;
        auto entries = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        for (auto &entry : entries) {

            QString prefix;
            if (entry.isAbsolute()) {
                prefix = entry.absoluteFilePath();
            } else {
                prefix = entry.filePath();
            }
            installs.append(JavaInstall(prefix + "/jre/bin/java"));
            installs.append(JavaInstall(prefix + "/bin/java"));
        }
    };
    // oracle RPMs
    scanJavaDir("/usr/java");
    // general locations used by distro packaging
    scanJavaDir("/usr/lib/jvm");
    scanJavaDir("/usr/lib32/jvm");
    // javas stored in MultiMC's folder
    scanJavaDir("java");

#endif
#ifdef Q_OS_WIN
    installs.append(JavaInstall("C:\\Program Files\\Java\\jre8\\bin\\javaw.exe"));
    installs.append(JavaInstall("C:\\Program Files\\Java\\jre7\\bin\\javaw.exe"));
    installs.append(JavaInstall("C:\\Program Files\\Java\\jre6\\bin\\javaw.exe"));
    installs.append(JavaInstall("C:\\Program Files\\Java\\jre8\\bin\\java.exe"));
    installs.append(JavaInstall("C:\\Program Files\\Java\\jre7\\bin\\java.exe"));
    installs.append(JavaInstall("C:\\Program Files\\Java\\jre6\\bin\\java.exe"));

    installs.append(JavaInstall("C:\\Program Files (x86)\\Java\\jre8\\bin\\javaw.exe", "32"));
    installs.append(JavaInstall("C:\\Program Files (x86) Files\\Java\\jre7\\bin\\javaw.exe", "32"));
    installs.append(JavaInstall("C:\\Program Files (x86) Files\\Java\\jre6\\bin\\javaw.exe", "32"));
    installs.append(JavaInstall("C:\\Program Files (x86)\\Java\\jre8\\bin\\java.exe", "32"));
    installs.append(JavaInstall("C:\\Program Files (x86)\\Java\\jre7\\bin\\java.exe", "32"));
    installs.append(JavaInstall("C:\\Program Files (x86)\\Java\\jre6\\bin\\java.exe", "32"));
#endif

#ifdef Q_OS_MAC
    installs.append(JavaInstall(
        "/Applications/Xcode.app/Contents/Applications/Application Loader.app/Contents/MacOS/itms/java/bin/java"));
    installs.append(JavaInstall("/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java"));
    installs.append(JavaInstall("/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java"));

    QDir libraryJVMDir("/Library/Java/JavaVirtualMachines/");
    QStringList libraryJVMJavas = libraryJVMDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QString &java, libraryJVMJavas) {
        installs.append(JavaInstall(libraryJVMDir.absolutePath() + "/" + java + "/Contents/Home/bin/java"));
        installs.append(JavaInstall(libraryJVMDir.absolutePath() + "/" + java + "/Contents/Home/jre/bin/java"));
    }
    QDir systemLibraryJVMDir("/System/Library/Java/JavaVirtualMachines/");
    QStringList systemLibraryJVMJavas = systemLibraryJVMDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    for (const QString &java: systemLibraryJVMJavas) {
        installs.append(JavaInstall(systemLibraryJVMDir.absolutePath() + "/" + java + "/Contents/Home/bin/java"));
        installs.append(JavaInstall(systemLibraryJVMDir.absolutePath() + "/" + java + "/Contents/Commands/java"));
    }
#endif
    return installs;
}
