
#include "BaseMod.h"
#include "MinecraftInstance.h"

BaseMod::BaseMod(MinecraftInstance *instance) : m_instance(instance) {}

bool BaseMod::setEnabled(bool enabled) {
    QString oldFile, newFile;
    if (enabled) {
        oldFile = file(false);
        newFile = file();
    } else {
        oldFile = file();
        newFile = file(false);
    }
    return QFile().rename(oldFile, newFile);
}

bool BaseMod::enabled() { return Utils::fileExists(file()) && !(Utils::fileExists(file(false))); }
