#pragma once

#include "BaseMod.h"
class FileMod : public BaseMod {
    Q_OBJECT
public:
    FileMod(MinecraftInstance *instance, const QString &file);
    QString file(bool enabled = true) override;

    QString update(QWidget *parent) override;

    QString install() override;

    int versionsBehind() override;

    void remove() override;

    QString name() override;

    bool canUpdate() override { return false; }

private:
    QString m_file;
};
