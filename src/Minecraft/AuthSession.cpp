/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AuthSession.h"

#include "Minecraft/Yggdrasil.h"

#include "Util/DownloadUtil.h"
#include "Util/Utils.h"

AuthSession::AuthSession(Config &config, QObject *parent) : QObject(parent), config(config) {}

void AuthSession::load(QString auth_f) {
    if (!Utils::fileExists(auth_f)) {
        logout();
        return;
    }

    QFile authFile(auth_f);
    authFile.open(QIODevice::ReadOnly);
    QJsonObject authObject = QJsonDocument::fromBinaryData(authFile.readAll()).object();
    authFile.close();
    is_valid = authObject["valid"].toBool();
    access_token = authObject["token"].toString();
    client_token = authObject["client"].toString();
    profile.id = authObject["id"].toString();
    profile.name = authObject["name"].toString();
    profile.legacy = authObject["legay"].toBool();
    if (is_valid) {
        login();
    } else {
        logout();
    }
}

void AuthSession::read(QJsonObject json) {
    if (json.contains("error")) {
        is_valid = false;
        return;
    }
    is_valid = true;
    access_token = json["accessToken"].toString();
    client_token = json["clientToken"].toString();
    QJsonObject sel_prof = json["selectedProfile"].toObject();
    profile.id = sel_prof["id"].toString();
    profile.name = sel_prof["name"].toString();
    profile.legacy = sel_prof["legacy"].toBool();
}

void AuthSession::write(QString file) {
    QJsonObject out;
    out["valid"] = is_valid;
    out["token"] = access_token;
    out["client"] = client_token;
    out["id"] = profile.id;
    out["name"] = profile.name;
    out["legacy"] = profile.legacy;
    QFile authFile(file);
    authFile.open(QIODevice::WriteOnly);
    QJsonDocument outDoc(out);
    authFile.write(outDoc.toBinaryData());
}

void AuthSession::invalidate() {
    qDebug() << "Auth:" << "Invalidating Token";
    Yggdrasil::invalidate(access_token, client_token);
    is_valid = false;
    access_token = "";
    client_token = "";
    profile.id = "";
    profile.name = "";
    profile.legacy = false;
}

void AuthSession::refresh() {
    qDebug() << "Auth:" << "Refreshing Token";

    QJsonDocument reply = Yggdrasil::refresh(access_token, client_token, profile);
    QJsonObject obj = reply.object();
    client_token = obj["clientToken"].toString();
    access_token = obj["accessToken"].toString();
    write(Utils::combinePath(config.data_dir, "auth.dat"));
}

bool AuthSession::validateToken() {
    QJsonDocument reply = Yggdrasil::validate(access_token, client_token);
    QJsonObject obj = reply.object();
    return obj.isEmpty();
}

bool AuthSession::valid() const { return is_valid; }

QString AuthSession::accesstoken() const { return access_token; }

User AuthSession::user() const { return profile; }

QString AuthSession::file() const { return config.data_dir + "/auth.dat"; }

bool AuthSession::auth() {
    if (is_valid) {
        logout();
    } else {
        login();
    }
    return valid();
}

void AuthSession::logout() {
    qDebug() << "Auth:" << "Logging out";
    invalidate();
    QDir().remove(file());
    emit(signalAuthenticate("Login", ":/icons/steve.png", ""));
}

void AuthSession::login() {
    qDebug() << "Auth:" << "Logging in";
    QString skin = Utils::getPlayerSkin(profile.id);
    emit(signalAuthenticate("Logout", skin, ""));
}
