/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ForgeVersions.h"

#include "Util/Instrumentation.h"

#include "Util/Utils.h"
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

ForgeVersions *forgeVersions;

ForgeVersions::ForgeVersions() {}

QStringList ForgeVersions::getAvailableVersions(const QString &minecraft) {

    QList<BasicForgeVersion> forge;
    if (!minecraft.isEmpty())
        forge = this->versions.values(minecraft);
    else
        forge = this->versions.values();
    std::reverse(forge.begin(), forge.end());
    QListIterator<BasicForgeVersion> iter(forge);
    QStringList versions;
    while (iter.hasNext()) {
        versions.append(iter.next().version);
    }
    return versions;
}

void ForgeVersions::loadFromFile(QString file) {
    ScopedTask _("Load Forge Version Info");
    if (!Utils::fileExists(file))
        return;
    QFile versionsFile(file);
    versionsFile.open(QIODevice::ReadOnly);
    QJsonObject versionsObject = QJsonDocument::fromJson(versionsFile.readAll()).object();
    versionsFile.close();

    QJsonArray versionsArray(versionsObject["versions"].toArray());
    QJsonObject v;
    for (int i = 0; i < versionsArray.size(); i++) {
        v = versionsArray[i].toObject();
        BasicForgeVersion forge;
        forge.releaseTime = v["releaseTime"].toString();
        forge.sha256 = v["sha256"].toString();
        forge.version = v["version"].toString();
        QJsonArray reqs = v["requires"].toArray();
        for (QJsonValueRef obj : reqs) {
            QJsonObject req = obj.toObject();
            QString key = req["uid"].toString();
            QString value = req["equals"].toString();
            forge.requirements.insert(key, value);
            versions.insert(value, forge);
        }
    }
}

ForgeVersion::ForgeVersion() {}

void ForgeVersion::loadFromFile(QString file) {
    if (!Utils::fileExists(file))
        return;
    QFile versionFile(file);
    versionFile.open(QIODevice::ReadOnly);
    QJsonObject versionObject = QJsonDocument::fromJson(versionFile.readAll()).object();
    versionFile.close();

    QJsonArray tweakersArray = versionObject["+tweakers"].toArray();
    for (int i = 0; i < tweakersArray.size(); i++) {
        tweakers.append(tweakersArray[i].toString());
    }
    QJsonArray librariesArray = versionObject["libraries"].toArray();
    QJsonObject current;
    for (int i = 0; i < librariesArray.size(); i++) {
        current = librariesArray[i].toObject();
        ForgeLibrary lib;
        lib.name = current["name"].toString();
        if (current.contains("MMC-hint")) {
            auto hint = current["MMC-hint"].toString();
            if (hint == "forge-pack-xz") {
                lib.packxz = true;
            }
        } else {
            lib.packxz = false;
        }
        if (current.contains("url")) {
            lib.url = current["url"].toString();
        } else {
            lib.url = "https://libraries.minecraft.net/";
        }
        libraries.append(lib);
    }
    QJsonObject req = versionObject["requires"].toObject();
    for (int j = 0; j < req.size(); j++) {
        QString key = req.keys()[j];
        QString value = req[key].toString();
        requirements.insert(key, value);
    }
    releaseTime = versionObject["releaseTime"].toString();
    mainClass = versionObject["mainClass"].toString();
    version = versionObject["version"].toString();
    name = versionObject["name"].toString();
}

Artifact::Artifact(QString d) {
    descriptor = d;
    QStringList s = d.split(":");
    domain = s[0];
    name = s[1];
    ext = "jar";
    int last = s.size() - 1;
    int idx = s[last].indexOf("@");
    if (idx != -1) {
        ext = s[last].mid(idx + 1);
        s[last] = s[last].mid(0, idx);
    }
    version = s[2];
    if (s.size() > 3) {
        classifier = s[3];
    }
    file = name + '-' + version;
    if (!classifier.isEmpty())
        file += '-' + classifier;
    file += '.' + ext;
    path = domain.replace('.', '/') + '/' + name + '/' + version;
    file_path = path + '/' + file;
}
