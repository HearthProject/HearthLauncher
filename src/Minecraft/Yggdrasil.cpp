/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Yggdrasil.h"

#include "Util/Utils.h"
#include "Util/NetworkUtil.h"

QJsonDocument Yggdrasil::login(const QString &username, const QString &password) {
    QJsonObject auth_request;
    QJsonObject agent;
    agent["name"] = "Minecraft";
    agent["version"] = 1;
    auth_request["agent"] = agent;
    auth_request["username"] = username;
    auth_request["password"] = password;
    return NetworkUtil::post(Auth_Constants::AUTH_ENDPOINT, auth_request).content();
}

void Yggdrasil::invalidate(const QString &accessToken, const QString &clientToken) {
    QJsonObject invalid_request;
    invalid_request["accessToken"] = accessToken;
    if (!clientToken.isEmpty())
        invalid_request["clientToken"] = clientToken;
    NetworkUtil::post(Auth_Constants::INVALIDATE_ENDPOINT, invalid_request);
}

QJsonDocument Yggdrasil::refresh(const QString &accessToken, const QString &clientToken, const User &profile) {
    QJsonObject refresh_request;
    QJsonObject selectedProfile;
    selectedProfile["name"] = profile.name;
    selectedProfile["id"] = profile.id;
    refresh_request["selectedProfile"] = selectedProfile;
    refresh_request["accessToken"] = accessToken;
    refresh_request["clientToken"] = clientToken;

    return NetworkUtil::post(Auth_Constants::REAUTH_ENDPOINT, refresh_request).content();
}


QJsonDocument Yggdrasil::validate(const QString &accessToken, const QString &clientToken) {
    QJsonObject validate_request;
    validate_request["accessToken"] = accessToken;
    validate_request["clientToken"] = clientToken;
    return NetworkUtil::post(Auth_Constants::VALIDATE_ENDPOINT, validate_request).content();
}


QVector<Yggdrasil::Service> Yggdrasil::checkStatus() {
    QVector<Yggdrasil::Service> status;
    QJsonArray content = NetworkUtil::post("https://status.mojang.com/check").content().array();

    QMap<QString,QString> names;
    names.insert("minecraft.net", "Web");
    names.insert("session.minecraft.net", "Session");
    names.insert("account.mojang.com", "Account");
    names.insert("auth.mojang.com", "Auth");
    names.insert("skins.minecraft.net", "Skins");

    for(QJsonValue value: content) {
        auto o = value.toObject();
        auto url = o.keys().first();
        auto name = names.value(url,QString());
        if(!name.isEmpty()) {
            status << Service(name, url, QColor(o[url].toString()));
        }
    }
    return status;
}
