//
// Created by tyler on 1/22/18.
//

#include "FileMod.h"

FileMod::FileMod(MinecraftInstance *instance, const QString &file) : BaseMod(instance), m_file(file) {}

QString FileMod::file(bool enabled) {
    auto file = Utils::combinePath(m_instance->getSubDir("mods"), m_file);
    if (!enabled)
        file += ".disabled";
    return file;
}

QString FileMod::name() { return m_file; }

QString FileMod::update(QWidget *parent) { return nullptr; }

QString FileMod::install() { return nullptr; }

int FileMod::versionsBehind() { return 0; }

void FileMod::remove() { QDir().remove(file()); }
