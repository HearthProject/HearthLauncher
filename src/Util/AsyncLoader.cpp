/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AsyncLoader.h"

#include "Hearth/HearthAPI.h"
#include "Util/Instrumentation.h"
#include "Util/NetworkUtil.h"
#include "Util/Reply.h"

#ifdef Q_OS_LINUX
#include <quazip5/JlCompress.h>

#else
#include <JlCompress.h>
#include <quazip.h>
#endif

#include <QJsonArray>

#include <thread>

AsyncLoad::AsyncLoad(QThread *thread, Config &conf, QNetworkAccessManager &network) : config(conf), NAM(network) {
    moveToThread(thread);
}

Reply AsyncLoad::createRequest(const QString &url, const QString &dest) {
    qDebug() << "Downloading:" << url << "to" << dest;
    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    QNetworkReply *reply = NAM.get(req);
    Reply r(reply, [this](std::exception_ptr e) { emit error(e); });
    r.then([dest](QNetworkReply *reply) {
         QFile file(dest);
         file.open(QIODevice::WriteOnly);
         file.write(reply->readAll());
     })
        .error([reply, this](QNetworkReply::NetworkError) {
            qWarning() << "Error Loading: " << reply->url().toDisplayString() << ": " << reply->errorString();
            emit error(std::make_exception_ptr(std::runtime_error(reply->errorString().toStdString())));
        })
        .finally([reply] { reply->deleteLater(); });
    return r;
}

void AsyncLoad::loadSource(OneMeta::Source source) {
    // Load the json in another thread as it takes a while.
    std::thread t([this, source] {
        ScopedEvent _("Parsing type");
        auto dir = Utils::combinePath(config.onemeta_dir, Utils::combinePath(source.name, source.name + ".json"));
        QFile metaFile(dir);
        metaFile.open(QIODevice::ReadOnly);
        emit provider_loaded(std::make_shared<OneMeta::Provider>(QJsonDocument::fromJson(metaFile.readAll()).object(),
                                                                 source.type),
                             source.type);
        metaFile.close();
        if (++loadState == toLoad)
            emit loaded();
    });
    t.detach();
}

void AsyncLoad::loadMinecraftVersionManifest() {
    emit minecraft_version_manifest_loaded(config.cache_dir + "/minecraft.json");
    if (++loadState == toLoad)
        emit loaded();
}

void AsyncLoad::loadForgeVersionManifest() {
    emit forge_version_manifest_loaded(config.cache_dir + "/forge.json");
    if (++loadState == toLoad)
        emit loaded();
}

static bool test(QNetworkReply *reply, QString file, QString hash,
                 QCryptographicHash::Algorithm algo = QCryptographicHash::Md5) {
    qDebug() << "Testing hash:" << file << "-" << hash;
    if (!Utils::fileExists(file))
        return true;
    if (reply->attribute(QNetworkRequest::SourceIsFromCacheAttribute).toBool())
        return false;
    if (!hash.isEmpty())
        return !Utils::compareChecksum(file, hash, algo);
    return true;
}

void AsyncLoad::retrieveProvider(OneMeta::Source source) {
    ScopedEvent se("Source hash");
    qInfo() << "Loading " << source.name;
    QString dir(Utils::combinePath(config.onemeta_dir, source.name));

    QDir().mkpath(dir);

    createRequest(source.hash, Utils::combinePath(dir, source.name + ".json.md5")).then([
        this, dir, source, se = std::move(se)
    ](QNetworkReply * reply) {
        auto sourceFile = Utils::combinePath(dir, source.name + ".json");
        if (test(reply, sourceFile, Utils::readHash(sourceFile + ".md5"))) {
            ScopedEvent se_meta("Source file");
            qInfo() << "Provider" << source.name << "out of date, retrieving new data";
            QDir d(dir);
            for (QString e : d.entryList(QDir::NoDotAndDotDot))
                d.remove(e);
            QString compressed = Utils::combinePath(dir, source.name + source.getCompressionExt());
            createRequest(source.url, compressed)
                .then(
                    [ this, dir, source, sourceFile, compressed, se_meta = std::move(se_meta) ](QNetworkReply * reply) {
                        ScopedTask _("Source extracting");
                        switch (source.compression) {
                        case OneMeta::NONE:
                            break;
                        case OneMeta::ZIP:
                            JlCompress::extractDir(compressed, dir);
                            QFile(compressed).remove();
                            break;
                        case OneMeta::XZ:
                            Utils::decompressXZ(compressed, sourceFile);
                            break;
                        }
                        loadSource(source);
                    })
                .error([this, source](QNetworkReply::NetworkError) {
                    // Connection is probably down, try to load cached version.
                    loadSource(source);
                });
        } else {
            QTimer::singleShot(0, this, [this, source]() { loadSource(source); });
        }
    });
}

void AsyncLoad::reloadDatabase() {
    loadState = 0;
    try {
        toLoad = 2 + HearthAPI::instance()->getSources(OneMeta::CURSE).size();

        ScopedEvent se_mh("sources.json.md5");

        ScopedEvent se_index("mmc-index.json");
        createRequest("https://v1.meta.multimc.org/index.json", Utils::combinePath(config.cache_dir, "mmc-index.json"))
            .then([ this, se_index = std::move(se_index) ](QNetworkReply * reply) {
                QFile index(Utils::combinePath(config.cache_dir, "mmc-index.json"));
                index.open(QIODevice::ReadOnly);
                QJsonObject indexObject = QJsonDocument::fromJson(index.readAll()).object();
                index.close();
                QString forgeHash;
                QJsonArray array = indexObject["packages"].toArray();
                for (int i = 0; i < array.size(); i++) {
                    QJsonObject o = array[i].toObject();
                    if (o["name"] == "Forge") {
                        forgeHash = QString(o["sha256"].toString());
                        break;
                    }
                }
                ScopedEvent se_f("forge.json");
                if (test(reply, config.cache_dir + "/forge.json", forgeHash, QCryptographicHash::Sha256)) {
                    createRequest("https://v1.meta.multimc.org/net.minecraftforge/index.json",
                                  config.cache_dir + "/forge.json")
                        .then([ this, se_f = std::move(se_f) ](QNetworkReply * reply) { loadForgeVersionManifest(); });
                } else
                    loadForgeVersionManifest();
            })
            .error([this](QNetworkReply::NetworkError) {
                // Connection is probably down, try to load cached version.
                loadForgeVersionManifest();
            });

        ScopedEvent se_mch("minecraft.json.md5");
        createRequest("https://fdn.redstone.tech/theoneclient/hl3/version_manifest.json.md5",
                      config.cache_dir + "/minecraft.json.md5")
            .then([ this, se_mch = std::move(se_mch) ](QNetworkReply * reply) {
                if (test(reply, config.cache_dir + "/minecraft.json",
                         Utils::readHash(config.cache_dir + "/minecraft.json.md5"))) {
                    ScopedEvent se_mc("minecraft.json");
                    createRequest("https://fdn.redstone.tech/theoneclient/hl3/version_manifest.json",
                                  config.cache_dir + "/minecraft.json")
                        .then([ this, se_mc = std::move(se_mc) ](QNetworkReply * reply) {
                            loadMinecraftVersionManifest();
                        });
                } else
                    loadMinecraftVersionManifest();
            })
            .error([this](QNetworkReply::NetworkError e) {
                // Connection is probably down, try to load cached version.
                loadMinecraftVersionManifest();
            });

        for (OneMeta::Source provider : HearthAPI::instance()->getSources(OneMeta::CURSE)) {
            retrieveProvider(provider);
        }
    } catch (...) {
        emit error(std::current_exception());
    }
}
