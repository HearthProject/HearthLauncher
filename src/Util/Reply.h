/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Util/Utils.h"

#include <QNetworkReply>

#include <exception>
#include <functional>

// A convience class to attatch continuations to a reply.
class Reply {
public:
    Reply(QNetworkReply *r, std::function<void(std::exception_ptr)> onErr) : reply(r), onError(onErr) {}

    template <class F> Reply &then(F &&f) {
        reply->connect(reply, &QNetworkReply::finished, [ reply = reply, f = std::forward<F>(f), onError = onError ] {
            if (reply->error()) {
                return;
            }
            try {
                f(reply);
            } catch (...) {
                onError(std::current_exception());
            }
        });
        return *this;
    }

    template <class F> Reply &error(F &&f) {
        reply->connect(reply, overload<QNetworkReply::NetworkError>(&QNetworkReply::error),
                       [ f = std::forward<F>(f), onError = onError ](QNetworkReply::NetworkError error) {
                           try {
                               f(error);
                           } catch (...) {
                               onError(std::current_exception());
                           }
                       });
        return *this;
    }

    template <class F> Reply &finally(F &&f) {
        reply->connect(reply, &QNetworkReply::finished, [ f = std::forward<F>(f), onError = onError ] {
            try {
                f();
            } catch (...) {
                onError(std::current_exception());
            }
        });
        return *this;
    }

private:
    QNetworkReply *reply; // Already instructed to deleteLater, no need to cleanup.
    std::function<void(std::exception_ptr)> onError;
};
