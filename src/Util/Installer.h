/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Minecraft/ForgeVersions.h"
#include "Minecraft/MinecraftVersions.h"
#include "Util/Config.h"
#include <QString>
#include <QTemporaryFile>
#include <QtWidgets>

class Installer : public QObject {
    Q_OBJECT
public:
    Installer() {}
    virtual bool install() = 0;

    void connectProgress(const QObject *receiver);

signals:
    void signalMax(int max);
    void signalProgress(int progress);
    void signalMessage(QString message);
};

class AssetInstaller : public Installer {
    Q_OBJECT
public:
    AssetInstaller(MinecraftAssets assets, Config &conf) : config(conf), m_assets(assets) {}
    bool install();

private:
    Config &config;
    MinecraftAssets m_assets;
};

class LibraryInstaller : public Installer {
    Q_OBJECT
public:
    LibraryInstaller(MinecraftVersion mc, QString dir, Config &conf) : config(conf), m_dir(dir), m_mc(mc) {}
    bool install();
    bool unzipNative(QString source, QString targetFolder, QStringList excludes = QStringList());

    QStringList libraries;

private:
    Config &config;
    QString m_dir;
    MinecraftVersion m_mc;
};

class ForgeInstaller : public Installer {
    Q_OBJECT
public:
    ForgeInstaller(QString forge, QString forge_dir, QString lib_dir, Config &conf, bool server = false)
        : config(conf), m_forge(forge), m_forge_dir(forge_dir), m_lib_dir(lib_dir), server(server) {}
    bool install();
    void downloadAndDecompressPackXZ(const QString &url, const QString &path);

    QString mainClass;
    QStringList libraries;
    ForgeVersion forge;

private:
    Config &config;
    QString m_forge, m_forge_dir, m_lib_dir;
    bool server;
};
