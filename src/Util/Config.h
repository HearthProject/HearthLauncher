/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QDir>
#include <QSettings>
#include <QStandardPaths>
#include <QString>
#include <QDateTime>
#include <QApplication>

class Config {
public:
    Config();

    static Config &instance() {
        static Config c;
        return c;
    }
    //main directories
    QString cache_dir;
    QString config_dir;
    QString data_dir;
    //subdirectories
    QString instance_dir;
    QString icon_dir;
    QString onemeta_dir;
    QString theme_dir;

    //important files
    QString config_file;
    QString log_file;

    QString seperator =
#ifdef Q_OS_WIN
        ";";
#else
        ":";
#endif
    bool sendAnalytics = false;
    QString analyticsId = "";

    bool offline = false;

    QSettings *settings();

    void parseCommandline(QApplication &app);
};
