#pragma once
#include <QObject>

class Messenger: public QObject {
    Q_OBJECT
public:
    Messenger(QObject *parent = nullptr): QObject(parent) {}

signals:
    void signalMax(int max);
    void signalProgress(int progress);
    void signalMessage(QString message);
    void signalTitle(QString title);
};
