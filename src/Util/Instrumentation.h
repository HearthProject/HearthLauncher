/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#ifdef HL_HAS_VTUNE
#include <ittnotify.h>
#endif

#include <cassert>
#include <utility>

#ifdef HL_HAS_VTUNE
/// \brief A unique global scope for instrumentation data.
///
/// Domains last for the lifetime of the application and cannot be destroyed.
/// Multiple Domains created with the same name represent the same domain.
class Domain {
    __itt_domain *domain;

public:
    explicit Domain(const char *name) : domain(__itt_domain_createA(name)) {}

    operator __itt_domain *() const { return domain; }
    __itt_domain *operator->() const { return domain; }
};

inline const Domain &getDefaultDomain() {
    static Domain domain("hearthlauncher");
    return domain;
}

/// \brief A global reference to a string constant.
///
/// These are uniqued by the ITT runtime and cannot be deleted. They are not
/// specific to a domain.
///
/// Prefer reusing a single StringHandle over passing a ntbs when the same
/// string will be used often.
class StringHandle {
    __itt_string_handle *handle;

public:
    StringHandle(const char *name) : handle(__itt_string_handle_createA(name)) {}

    operator __itt_string_handle *() const { return handle; }
};

/// \brief A task on a single thread. Nests within other tasks.
///
/// Each thread has its own task stack and tasks nest recursively on that stack.
/// A task cannot transfer threads.
///
/// SBRM is used to ensure task starts and ends are ballanced. The lifetime of
/// a task is either the lifetime of this object, or until end is called.
class ScopedTask {
    __itt_domain *domain;
    __itt_string_handle *string;
    bool done;

    ScopedTask(const ScopedTask &) = delete;
    ScopedTask &operator=(const ScopedTask &) = delete;

public:
    /// \brief Create a task in Domain \p d named \p s.
    ScopedTask(const StringHandle &s, const Domain &d = getDefaultDomain()) : domain(d), string(s) { start(); }

    ScopedTask(ScopedTask &&other) { *this = std::move(other); }

    ScopedTask &operator=(ScopedTask &&other) {
        domain = other.domain;
        other.domain = nullptr;
        string = other.string;
        other.string = nullptr;
        done = other.done;
        other.done = true;
        return *this;
    }

    void start() {
        assert(domain && string && "start called on moved from ScopedTask");
        __itt_task_begin(domain, __itt_null, __itt_null, string);
        done = false;
    }

    /// \brief Prematurely end this task.
    void end() {
        assert((done || domain) && "end called on moved from ScopedTask");
        if (!done)
            __itt_task_end(domain);
        done = true;
    }

    ~ScopedTask() { end(); }
};

class EventType {
    __itt_event event;

public:
    EventType(const char *name) : event(__itt_event_create(name, strlen(name))) {}

    operator __itt_event() const { return event; }
};

/// \brief An event that may start and finish on different threads.
///
/// SBRM is used to ensure event starts and ends are ballanced. The lifetime of
/// a event is either the lifetime of this object, or until end is called.
class ScopedEvent {
    __itt_event event;

    ScopedEvent &operator=(const ScopedEvent &) = delete;

public:
    /// \brief Create a task in Domain \p d named \p s.
    ScopedEvent(const EventType &e) : event(e) { __itt_event_start(event); }

    // HACK(Bigcheese): Allow and implement copy as move because Qt connect isn't move enabled.
    ScopedEvent(ScopedEvent &other) { *this = std::move(other); }
    ScopedEvent(const ScopedEvent &other) { *this = std::move(const_cast<ScopedEvent &>(other)); }

    ScopedEvent(ScopedEvent &&other) { *this = std::move(other); }

    ScopedEvent &operator=(ScopedEvent &&other) {
        event = other.event;
        other.event = -1;
        return *this;
    }

    /// \brief Prematurely end this task.
    void end() {
        if (event != -1)
            __itt_event_end(event);
        event = -1;
    }

    ~ScopedEvent() { end(); }
};
#else
class Domain {
public:
    Domain() {}
    Domain(const char *name) {}
};

class StringHandle {
public:
    StringHandle(const char *name) {}
};

class ScopedTask {
public:
    ScopedTask(const StringHandle &s, const Domain &d = Domain()) {}
    void start() {}
    void end() {}
};

class EventType {
public:
    EventType(const char *name) {}
};

class ScopedEvent {
public:
    ScopedEvent(const EventType &d) {}
    void end() {}
};
#endif
