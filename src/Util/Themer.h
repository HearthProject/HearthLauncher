#pragma once

#include <QObject>
#include "Theme.h"
#include "Config.h"
#include <QVector>
#include <QtCore/QMap>
#include <QFileSystemWatcher>
#include <QStringList>



class Themer : public QObject {
Q_OBJECT
public:
    Themer(Config &config, QObject *parent = nullptr);

    void loadDefaults();
    void loadFiles();
    void loadFile(const QString &file);

    void setTheme(Theme theme);
    Theme selected() const;

    void addTheme(Theme theme);

    QMap<QString,Theme> themes;

signals:
    void changeTheme();

private:
    Config &config;
    Theme m_selected = SYSTEM_THEME;


    QFileSystemWatcher *watcher;

    static Theme SYSTEM_THEME;
    static Theme LIGHT_THEME;
    static Theme DARK_THEME;


};



