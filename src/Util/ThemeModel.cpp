#include "ThemeModel.h"
#include "Theme.h"

ThemeModel::ThemeModel(Themer &themer, QObject *parent)
    : QAbstractListModel(parent), themer(themer) {
}

int ThemeModel::rowCount(const QModelIndex &parent) const
{
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;
    return themer.themes.size();

}

Theme ThemeModel::at(int index) const {
    return themer.themes.values().at(index);
}


QVariant ThemeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    Theme theme = at(index.row());
    switch(role) {
        case Qt::DisplayRole:
            return theme.name();
        default:
            return QVariant();
    }

}

void ThemeModel::setTheme(int row) {
   Theme theme = at(row);
   themer.setTheme(theme);
}

int ThemeModel::selected() {
    return themer.themes.values().indexOf(themer.selected());
}
