/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DownloadUtil.h"
#include "Utils.h"
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QNetworkReply>

void DownloadUtil::downloadFile(const QString &url, const QString &dest) {
    QNetworkAccessManager m_netMan;
    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    QNetworkReply *reply = m_netMan.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), this, SIGNAL(success()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), &loop, SLOT(quit()));
    connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(m_downloadProgress(qint64, qint64)));
    loop.exec();
    QFile file(dest);
    file.open(QIODevice::WriteOnly);
    file.write(reply->readAll());
    file.close();
    delete reply;
}

void DownloadUtil::downloadTempFile(QTemporaryFile *temp, const QString &url) {
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkDiskCache *diskCache = new QNetworkDiskCache(this);
    diskCache->setCacheDirectory(Config::instance().cache_dir);
    manager->setCache(diskCache);
    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    req.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
    QNetworkReply *reply = manager->get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(m_downloadProgress(qint64, qint64)));
    loop.exec();
    temp->open();
    temp->write(reply->readAll());
    delete reply;
}

void DownloadUtil::m_downloadProgress(qint64 bytesRecived, qint64 totalBytes) {
    emit(signalProgress(bytesRecived));
    emit(signalMax(totalBytes));
}

void DownloadUtil::connectProgress(const QObject *reciever) {
    connect(this, SIGNAL(signalMax(int)), reciever, SIGNAL(signalMax(int)));
    connect(this, SIGNAL(signalProgress(int)), reciever, SIGNAL(signalProgress(int)));
}
