#include "Theme.h"
#include "Utils.h"
#include <QMessageBox>
#include <QRegularExpressionMatchIterator>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QTextStream>

Theme::Theme(const QString &file, const QString &name) : m_file(file), m_name(name) {}

QString Theme::load() {
    if (m_file.isEmpty() || !Utils::fileExists(m_file))
        return QString();

    QFile stylesheet(m_file);
    stylesheet.open(QFile::ReadOnly);
    QString s_stylesheet = QString(stylesheet.readAll());
    stylesheet.close();

    if (!s_stylesheet.contains("/* THIS SOFTWARE IS PUBLIC DOMAIN */", Qt::CaseInsensitive)) {
        QMessageBox::warning(nullptr, "Proprietary Stylesheet Detected",
                             QString("The stylesheet \"%1\" does not contain the public domain dedication, and has "
                                     "been blocked from loading.")
                                 .arg(name()));
        return QString();
    }

    QRegularExpression var(R"((\$.+):\s*(.+);)");
    QRegularExpression alpha(R"(\$alpha\((.*),\s*(\d+)\))");
    QRegularExpression darken(R"(\$darken\((.*),\s*(\d+)\))");
    QRegularExpression lighten(R"(\$lighten\((.*),\s*(\d+)\))");

    QRegularExpressionMatch m;
    QString setting_key;
    QRegularExpressionMatchIterator iter = var.globalMatch(s_stylesheet);
    while (iter.hasNext()) {
        m = iter.next();
        setting_key = "css/" + m.captured(1);
        setting_key.replace("$", "");
        s_stylesheet.replace(m.captured(0), "");
        s_stylesheet.replace(m.captured(1), m.captured(2));
    }

    iter = alpha.globalMatch(s_stylesheet);
    while (iter.hasNext()) {
        m = iter.next();
        QString color_code = m.captured(1);
        QString new_code = "rgba(";

        new_code += QString::number(color_code.mid(1, 2).toInt(nullptr, 16)) + ", ";
        new_code += QString::number(color_code.mid(3, 2).toInt(nullptr, 16)) + ", ";
        new_code += QString::number(color_code.mid(5, 2).toInt(nullptr, 16)) + ", ";
        new_code += m.captured(2) + ")";

        s_stylesheet.replace(m.captured(0), new_code);
    }

    iter = darken.globalMatch(s_stylesheet);
    while (iter.hasNext()) {
        m = iter.next();
        QString color_code = m.captured(1);
        QString new_code = "rgb(";

        int red = color_code.mid(1, 2).toInt(nullptr, 16);
        int green = color_code.mid(3, 2).toInt(nullptr, 16);
        int blue = color_code.mid(5, 2).toInt(nullptr, 16);
        int darken = m.captured(2).toInt();

        new_code += QString::number(std::max(0, red - darken)) + ", ";
        new_code += QString::number(std::max(0, green - darken)) + ", ";
        new_code += QString::number(std::max(0, blue - darken)) + ")";

        s_stylesheet.replace(m.captured(0), new_code);
    }

    iter = lighten.globalMatch(s_stylesheet);
    while (iter.hasNext()) {
        m = iter.next();
        QString color_code = m.captured(1);
        QString new_code = "rgb(";

        int red = color_code.mid(1, 2).toInt(nullptr, 16);
        int green = color_code.mid(3, 2).toInt(nullptr, 16);
        int blue = color_code.mid(5, 2).toInt(nullptr, 16);
        int lighten = m.captured(2).toInt();

        new_code += QString::number(std::max(0, red + lighten)) + ", ";
        new_code += QString::number(std::max(0, green + lighten)) + ", ";
        new_code += QString::number(std::max(0, blue + lighten)) + ")";

        s_stylesheet.replace(m.captured(0), new_code);
    }

    return s_stylesheet;
}

QString Theme::file() const { return m_file; }

QString Theme::name() const {
    if (!m_name.isEmpty())
        return m_name;

    QFile stylesheet(m_file);
    stylesheet.open(QFile::ReadOnly);
    QTextStream in(&stylesheet);
    QString firstLine = in.readLine();
    stylesheet.close();
    QRegularExpression name(R"(\/\*\s+THEME NAME:\s+(.+)\s+\*\/)");
    QRegularExpressionMatch m = name.match(firstLine);
    if (!m.hasMatch())
        return QFileInfo(m_file).baseName();
    return m.captured(1);
}

bool Theme::operator==(Theme t) const { return this->file() == t.file(); }
