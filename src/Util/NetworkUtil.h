/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 0xFireball
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QHttpMultiPart>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QtNetwork/QNetworkConfiguration>

namespace NetworkUtil {

struct NetworkReply {
    int statusCode;
    QJsonDocument json;

    NetworkReply(int statusCode, QJsonDocument json) : statusCode(statusCode), json(json) {}

    QJsonDocument content() const {
        return json;
    }
};

QString paste(const QString &body, const QString &lang = "text");
NetworkReply post(const QString &url, const QJsonObject &body = QJsonObject(), const QString &auth = "");
NetworkReply post(const QUrl &url, const QJsonObject &body = QJsonObject(), const QString &auth = "");
NetworkReply get(const QUrl &url, const QString &auth = "");
NetworkReply postMultipart(const QUrl &url, QHttpMultiPart *parts, const QString &auth = "");

} // namespace NetworkUtil
