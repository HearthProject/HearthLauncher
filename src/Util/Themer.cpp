#include "Themer.h"
#include "Utils.h"
#include <QDebug>


Theme Themer::SYSTEM_THEME("", "System Theme");
Theme Themer::DARK_THEME(":style/default.qss");
Theme Themer::LIGHT_THEME(":style/light.qss");

Themer::Themer(Config &config, QObject *parent) : QObject(parent), config(config),
                                                  watcher(new QFileSystemWatcher({config.theme_dir})) {
    QString select_key = config.settings()->value("theme", ":style/default.qss").toString();
    loadDefaults();
    loadFiles();
    setTheme(themes.value(select_key, Themer::SYSTEM_THEME));
    connect(watcher, &QFileSystemWatcher::fileChanged, this, [&](const QString &path){
        if(path == selected().file()) {
            emit(changeTheme());
        }
    });
    connect(watcher, &QFileSystemWatcher::directoryChanged, this, [&]{
        loadFiles();
    });
}

Theme Themer::selected() const {
    return m_selected;
}

void Themer::setTheme(Theme theme) {
    m_selected = theme;
    config.settings()->setValue("theme", m_selected.file());
    changeTheme();
}

void Themer::loadDefaults() {
    addTheme(SYSTEM_THEME);
    addTheme(DARK_THEME);
    addTheme(LIGHT_THEME);
}

void Themer::addTheme(Theme theme) {
    themes.insert(theme.file(), theme);
}

void Themer::loadFiles() {
    QStringList files;
    Utils::findFiles(config.theme_dir, &files, {"*.qss"});
    for (const QString &file: files) {
        loadFile(file);
    }
}

void Themer::loadFile(const QString &file) {
    if (!themes.contains(file)) {
        Theme theme(file);
        qDebug() << "Adding theme:" << theme.name();
        addTheme(theme);
    }
}






