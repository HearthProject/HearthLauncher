/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
#define UTILS_H

#include "BuildEnvironment.h"

#include <QCryptographicHash>
#include <QString>
#include <QStringList>
#include <QTimer>
#include <QDateTime>

#include <functional>
#include <tuple>
#include <utility>

#include "Config.h"


class QJsonDocument;

class QJsonObject;

class QLayout;

class QSplashScreen;

#if defined(Q_OS_WIN)
const static QString OS_NAME = "win32";
const static QString NATIVE_OS_NAME = "windows";
#elif defined(Q_OS_MACOS)
const static QString OS_NAME = "darwin";
const static QString NATIVE_OS_NAME = OS_NAME;
#elif defined(Q_OS_LINUX)
const static QString OS_NAME = "linux";
const static QString NATIVE_OS_NAME = OS_NAME;
#else
const static QString OS_NAME = "unknown";
const static QString NATIVE_OS_NAME = OS_NAME;
#endif

extern const QString VERSION_STRING;
extern const QString USER_AGENT;
const static QString BUILDER_NAME(BUILDER);
const static QString BUILDER_OS_NAME(BUILDER_OS);

extern const int JOB_ID;

template <class... Args> struct Overload {
    template <class T, class R> auto operator()(R (T::*f)(Args...)) { return f; }
};

template <class... Args> Overload<Args...> overload{};

namespace detail {
template <class R, class T, class Tuple, std::size_t W, std::size_t... I>
constexpr decltype(auto) apply_impl(R T::*f, Tuple &&t, std::index_sequence<W, I...>) {
    return (std::get<0>(std::forward<Tuple>(t))->*f)(std::get<I>(std::forward<Tuple>(t))...);
}

template <class F, class Tuple, std::size_t W, std::size_t... I>
constexpr decltype(auto) apply_impl(F &&f, Tuple &&t, std::index_sequence<W, I...>) {
    return std::forward<F>(f)(std::get<I>(std::forward<Tuple>(t))...);
}
} // namespace detail

template <class F, class Tuple> constexpr decltype(auto) apply(F &&f, Tuple &&t) {
    return detail::apply_impl(std::forward<F>(f), std::forward<Tuple>(t),
                              std::make_index_sequence<std::tuple_size<std::remove_reference_t<Tuple>>::value>{});
}

template <class Q, class F, class... Args> void invoke_slot(Q *where, F &&f, Args &&... args) noexcept {
    QTimer::singleShot(0, where,
                       [ f = std::forward<F>(f), args = std::make_tuple(where, std::forward<Args>(args)...) ] {
                           apply(f, args);
                       });
}

template <class T, class Accessor, class Value> struct Filter {
    Filter(Accessor &&a, Value &&v) : a(std::forward<Accessor>(a)), v(std::forward<Value>(v)) {}

    template <class U> bool operator()(U &&u) const { return a(std::forward<U>(u)) == v; }

    Accessor a;
    Value v;
};

template <class T, class R, class Value> auto make_filter(R T::*f, Value &&v) {
    auto m = std::mem_fn(f);
    return Filter<T, decltype(m), Value>(std::move(m), std::forward<Value>(v));
}

namespace Utils {

QString timeDifference(QDateTime first, QDateTime second = QDateTime::currentDateTime());

bool updateTimestamp(const QString &filename);

QString combinePath(const QString &path1, const QString &path2);

QStringList sortVersions(QStringList versions);


QString generateUUID();

void clearLayout(QLayout *layout);

bool fileExists(QString path);

bool unzipFolder(QString source, QString targetFolder);

bool zipFolder(QString folder, QString zip);

bool zipFiles(QStringList files, QString zip);

QString formatNumber(double number, int decimals = 1);

void scaleImage(QString icon_filename, int scaleX = 128, int scaleY = 128);

QString getChecksum(const QString &filename, QCryptographicHash::Algorithm algo = QCryptographicHash::Sha1);

bool compareChecksum(const QString &filename, const QString &hash,
                     QCryptographicHash::Algorithm algo = QCryptographicHash::Md5);

QString readHash(QString file);

void findFiles(const QString &path, QStringList *res, QStringList filters = QStringList());

QString getPlayerSkin(QString uuid);

QJsonDocument post(const QString &url, const QJsonObject &body);

void splashMessage(QSplashScreen *splash, const QString &message);

void decompressXZ(const QString &file, const QString &output);

void downloadAndDecompressPackXZ(const QString &url, const QString &path);

bool copyDirectory(QString source, QString dest, QStringList blacklist = QStringList());

int getInstalledMemory();

void updateSettings(Config &config, QString setting, QVariant defaultValue);

int secondsNow();

QString formatTime(int seconds);
} // namespace Utils

#endif // UTILS_H
