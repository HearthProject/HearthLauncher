/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Util/AsyncPoster.h"

#include "Util/Reply.h"

AsyncPoster::AsyncPoster(QThread *thread) {
    moveToThread(thread);
    NAM.moveToThread(thread);
}

void AsyncPoster::post(QString url, QString type, QByteArray content) {
    QNetworkRequest req(std::move(url));
    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setHeader(QNetworkRequest::ContentTypeHeader, std::move(type));
    QNetworkReply *reply = NAM.post(std::move(req), std::move(content));
    Reply r(reply, [this](std::exception_ptr e) { emit error(e); });
    r.then([this](QNetworkReply *reply) { emit posted(); })
        .error([reply, this](QNetworkReply::NetworkError) {
            qWarning() << "Error Posting to: " << reply->url().toDisplayString() << ": " << reply->errorString();
            emit error(std::make_exception_ptr(std::runtime_error(reply->errorString().toStdString())));
        })
        .finally([reply] { reply->deleteLater(); });
}
