/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QNetworkAccessManager>
#include <QObject>
#include <atomic>
#include <exception>

#include "OneMeta/OneMeta.h"

class Config;
class Reply;

// All signals must be received on the same thread.
class AsyncLoad : public QObject {
    Q_OBJECT
public:
    AsyncLoad(QThread *thread, Config &conf, QNetworkAccessManager &network);

public slots:
    void reloadDatabase();

signals:
    void progress(QString);
    void provider_loaded(std::shared_ptr<OneMeta::Provider> provider, int type);
    void minecraft_version_manifest_loaded(QString);
    void forge_version_manifest_loaded(QString);
    void loaded();
    void error(std::exception_ptr);

private:
    Reply createRequest(const QString &url, const QString &dest);

    void loadMinecraftVersionManifest();
    void loadForgeVersionManifest();

    void loadSource(OneMeta::Source source);
    void retrieveProvider(OneMeta::Source source);

    Config &config;
    QNetworkAccessManager &NAM;
    std::atomic<unsigned> loadState;
    unsigned toLoad;
};
