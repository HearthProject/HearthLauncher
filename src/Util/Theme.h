#pragma once


#include <QString>



class Theme {

public:
    explicit Theme(const QString &file, const QString &name = QString());
    Theme() {}

    QString load();
    QString name() const;
    QString file() const;

    bool operator==(Theme t) const;

private:
    QString m_file, m_name = QString();


};



