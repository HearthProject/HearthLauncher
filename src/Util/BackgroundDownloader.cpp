/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "BackgroundDownloader.h"
#include "DownloadUtil.h"

BackgroundDownloader::BackgroundDownloader(QObject *parent) : QThread(parent) {}

void BackgroundDownloader::setTarget(const QString &url, const QString &path) {
    m_url = url;
    m_path = path;
}

void BackgroundDownloader::run() {
    DownloadUtil dl;
    connect(&dl, SIGNAL(signalMax(int)), this, SIGNAL(signalMax(int)));
    connect(&dl, SIGNAL(signalProgress(int)), this, SIGNAL(signalProgress(int)));
    connect(&dl, SIGNAL(signalMessage(QString)), this, SIGNAL(signalMessage(QString)));

    emit signalMessage(m_path);
    dl.downloadFile(m_url, m_path);
    emit downloadDone(m_path);
}

void BackgroundDownloader::connectProgress(const QObject *reciever) {
    connect(this, SIGNAL(signalMax(int)), reciever, SLOT(setMaximum(int)));
    connect(this, SIGNAL(signalProgress(int)), reciever, SLOT(setValue(int)));
}
