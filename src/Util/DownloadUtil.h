/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QString>
#include <QTemporaryFile>
#include <QtWidgets>
class Config;

class DownloadUtil : public QObject {
    Q_OBJECT
public:
    void downloadFile(const QString &url, const QString &dest);
    void downloadTempFile(QTemporaryFile *temp, const QString &url);

    void connectProgress(const QObject *reciever);
public slots:
    void m_downloadProgress(qint64 bytesRecived, qint64 totalBytes);
signals:
    void signalMax(int max);
    void signalProgress(int progress);
    void signalMessage(QString message);
    void success();
};
