/*
 * Copyright (C) 2017-2018 Bigcheese
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Util/Config.h"
#include <QCommandLineParser>
#include <QDebug>

Config::Config() {}

QSettings *Config::settings() {
    return new QSettings(config_file, QSettings::IniFormat);
    ;
}

void Config::parseCommandline(QApplication &app) {
    QCommandLineParser parser;
    parser.setApplicationDescription("Hearth Launcher");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption targetDirectoryOption(QStringList() << "d" << "directory", "Directory to use for storage",
                                             "directory");
    QCommandLineOption urlHandlerOption(QStringList() << "u" << "url-handler", "Parse a URL handler", "url");
    QCommandLineOption runInstance(QStringList() << "r" << "run-instance", "Start an instance", "instance_id");

    parser.addOption(targetDirectoryOption);
    parser.addOption(urlHandlerOption);
    parser.addOption(runInstance);

    parser.process(app);


    if (parser.isSet(urlHandlerOption)) {
        QString url = parser.value(urlHandlerOption);
        url.replace("hearth://", "");
        QString command = url.split("/")[0];
        QString arg = url.split("/")[1];
        qDebug() << "Running" << command << "with arg" << arg;
        std::exit(EXIT_SUCCESS);
    }

    QString port_dir = QApplication::applicationDirPath() + QDir::separator() + "data";
    QDir port_qdir(port_dir);

    if (parser.isSet(targetDirectoryOption)) {
        QString target_dir = QFileInfo(parser.value(targetDirectoryOption)).absoluteFilePath();
        QDir().mkpath(target_dir);
        data_dir = target_dir + QDir::separator() + "data";
        config_dir = target_dir + QDir::separator() + "config";
        cache_dir = target_dir + QDir::separator() + "cache";
        config_file = config_dir + QDir::separator() + "hearthlauncher.ini";
    } else if(port_qdir.exists()) {
        data_dir = port_dir + QDir::separator() + "data";
        config_dir = port_dir + QDir::separator() + "config";
        cache_dir = port_dir + QDir::separator() + "cache";

    } else {
        data_dir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        config_dir = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
        cache_dir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    }
    log_file = data_dir + QDir::separator() + "logs" + QDir::separator() + "hearthlauncher-" + QDateTime::currentDateTime().toString(Qt::ISODate) + ".log";
    config_file = config_dir + QDir::separator() + "hearthlauncher.ini";

    instance_dir = data_dir + QDir::separator() + "instances";
    icon_dir = cache_dir + QDir::separator() + "icons";
    onemeta_dir = cache_dir + QDir::separator() + "onemeta";
    theme_dir = config_dir + QDir::separator() + "themes";



}
