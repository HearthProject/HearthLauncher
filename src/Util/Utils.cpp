/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Utils.h"

#include "Minecraft/MinecraftVersions.h"
#include "Util/DownloadUtil.h"
#include "Util/Instrumentation.h"

#ifdef Q_OS_LINUX
#include <quazip5/JlCompress.h>
#include <quazip5/quazip.h>
#else
#include <JlCompress.h>
#include <quazip.h>
#endif

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <iostream>
#include <regex>

#ifdef HL_DEBUG
const QString VERSION_STRING = QString(GIT_HASH) + "@" + QString(GIT_BRANCH);
#else
const QString VERSION_STRING = "v" + QString::number(VERSION_NUM);
#endif
const QString USER_AGENT = QString("HearthLauncher/v") + QString(VERSION_STRING);
const int JOB_ID = M_JOB_ID;

QString Utils::timeDifference(QDateTime first, QDateTime second) {

    int hours = first.secsTo(second) / 3600;
    int days = first.daysTo(second);
    if(hours < 24) {
      return QString("%1 hours ago").arg(QString::number(hours));
    } else if(days < 20) {
      return QString("%1 days ago").arg(QString::number(days));
    } else {
        return first.toString(Qt::TextDate);
    }
}

bool Utils::updateTimestamp(const QString &filename) {
    QFile file(filename);
    if (!file.exists()) {
        return false;
    }
    if (!file.open(QIODevice::ReadWrite)) {
        return false;
    }
    const quint64 size = file.size();
    file.seek(size);
    file.write(QByteArray(1, '0'));
    file.resize(size);
    return true;
}

QString Utils::combinePath(const QString &path1, const QString &path2) {
    if (path1.isEmpty())
        return path2;
    if (path2.isEmpty())
        return path1;
    auto final = QDir::cleanPath(path1 + QDir::separator() + path2);
    ;
    return final;
}

QStringList Utils::sortVersions(QStringList versions) {
    QList<BasicMinecraftVersion> values;
    for (QString str : versions) {
        values.append(MinecraftVersions::instance()->versions[str]);
    }
    std::sort(values.begin(), values.end(), [](auto a, auto b) { return (a > b); });
    QStringList mcVersions;
    for (auto v : values)
        mcVersions.append(v.id);
    return mcVersions;
}


QString Utils::generateUUID() { return QUuid::createUuid().toString().replace("{", "").replace("}", ""); }

void Utils::clearLayout(QLayout *layout) {
    QLayoutItem *item;
    while ((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}

bool Utils::fileExists(QString path) {
    QFileInfo check_file(path);
    return check_file.exists() && check_file.isFile();
}

static const char *scale[] = {"", "K", "M", "B"};

QString Utils::formatNumber(double n, int decimals) {
    int digits = static_cast<int>(n == 0 ? 0 : 1 + floor(log10l(fabs(n))));
    int exp = digits <= 4 ? 0 : 3 * ((digits - 1) / 3);
    double m = static_cast<double>(n / powl(10, exp));
    if (m - static_cast<long>(n) == 0)
        decimals = 0;
    QString s = QString::number(m, 'f', decimals).append(scale[exp / 3]);
    return s;
}

void Utils::scaleImage(QString icon_filename, int scaleX, int scaleY) {
    ScopedTask _("scaleImage");
    QImage img(icon_filename);
    if (img.isNull())
        return;
    QPixmap p;
    p = p.fromImage(img.scaled(scaleX, scaleY, Qt::KeepAspectRatio, Qt::TransformationMode::SmoothTransformation));
    QFile f(icon_filename);
    f.open(QIODevice::WriteOnly);
    p.save(&f);
    f.close();
}

bool Utils::unzipFolder(QString source, QString targetFolder) {
    QuaZip zip(source);
    if (!zip.open(QuaZip::mdUnzip)) {
        return false;
    }
    QDir directory(targetFolder);
    if (!zip.goToFirstFile()) {
        return false;
    }
    do {
        QString name = zip.getCurrentFileName();
        QString absFilePath = directory.absoluteFilePath(name);
        JlCompress::extractFile(source, name, absFilePath);
    } while (zip.goToNextFile());
    zip.close();
    if (zip.getZipError() != 0) {
        return false;
    }
    return true;
}

bool Utils::zipFolder(QString folder, QString zip) { return JlCompress::compressDir(zip, folder, true); }

bool Utils::zipFiles(QStringList files, QString zip) { return JlCompress::compressFiles(zip, files); }

bool Utils::compareChecksum(const QString &filename, const QString &hash, QCryptographicHash::Algorithm algo) {
    ScopedTask _("Comparing Checksums");
    QString fileHash = getChecksum(filename, algo);
    return fileHash == hash;
}

QString Utils::readHash(QString file) {
    QFile hashFile(file);
    hashFile.open(QIODevice::ReadOnly);
    QString hash = hashFile.readAll().trimmed();
    hashFile.close();
    return hash;
}

QString Utils::getChecksum(const QString &filename, QCryptographicHash::Algorithm algo) {
    QFile f(filename);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(algo);
        if (hash.addData(&f)) {
            return QString::fromUtf8(hash.result().toHex());
        }
    }
    return QString();
}

void Utils::findFiles(const QString &path, QStringList *res, QStringList filters) {
    QDir cdir(path);
    cdir.setNameFilters(filters);
    for (const QString &match : cdir.entryList(QDir::Files | QDir::NoSymLinks)) {
        res->append(Utils::combinePath(path, match));
    }
    for (const QString &dir : cdir.entryList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot)) {
        findFiles(path + "/" + dir, res, filters);
    }
}

QString Utils::getPlayerSkin(QString uuid) {
    QString skin = Config::instance().cache_dir + "/icons/" + uuid;
    if (!Utils::fileExists(skin)) {
        DownloadUtil dl;
        QString url = "https://crafatar.com/avatars/" + uuid;
        dl.downloadFile(url, skin);
    }
    return skin;
}

QJsonDocument Utils::post(const QString &url, const QJsonObject &body) {
    QNetworkAccessManager m_netMan;
    QJsonDocument doc(body);
    QNetworkRequest req;
    req.setUrl(QUrl(url));
    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QNetworkReply *reply = m_netMan.post(req, doc.toJson());
    QEventLoop loop;
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    return QJsonDocument::fromJson(reply->readAll());
}

void Utils::splashMessage(QSplashScreen *splash, const QString &message) {
    if (splash->isActiveWindow())
        splash->showMessage(message, Qt::AlignHCenter, Qt::white);
}

#include "pack200/unpack200.h"
#include "xz-embedded/xz.h"

#ifdef Q_OS_WIN
#include <io.h>
#define dup(x) _dup(x)
#define fdopen(x, y) _fdopen(x, y)
#else
#endif

const size_t buffer_size = 8196;

void Utils::decompressXZ(const QString &file, const QString &output) {
    QFile *xz = new QFile(file);
    xz->open(QFile::ReadOnly);

    QFile *o = new QFile(output);
    o->open(QFile::WriteOnly);
    bool xz_success = false;
    // first, de-xz
    {
        uint8_t in[buffer_size];
        uint8_t out[buffer_size];
        struct xz_buf b;
        struct xz_dec *s;
        enum xz_ret ret;
        xz_crc32_init();
        xz_crc64_init();
        s = xz_dec_init(XZ_DYNALLOC, 1 << 26);
        if (s == nullptr) {
            xz_dec_end(s);
            return;
        }
        b.in = in;
        b.in_pos = 0;
        b.in_size = 0;
        b.out = out;
        b.out_pos = 0;
        b.out_size = buffer_size;
        while (!xz_success) {
            if (b.in_pos == b.in_size) {
                b.in_size = xz->read((char *)in, sizeof(in));
                b.in_pos = 0;
            }

            ret = xz_dec_run(s, &b);

            if (b.out_pos == sizeof(out)) {
                auto wresult = o->write((char *)out, b.out_pos);
                if (wresult < 0 || size_t(wresult) != b.out_pos) {
                    // msg = "Write error\n";
                    xz_dec_end(s);
                    return;
                }

                b.out_pos = 0;
            }

            if (ret == XZ_OK)
                continue;

            if (ret == XZ_UNSUPPORTED_CHECK) {
                // unsupported check. this is OK, but we should log this
                continue;
            }

            auto wresult = o->write((char *)out, b.out_pos);
            if (wresult < 0 || size_t(wresult) != b.out_pos) {
                // write error
                o->close();
                xz_dec_end(s);
                return;
            }

            switch (ret) {
            case XZ_STREAM_END:
                xz_dec_end(s);
                xz_success = true;
                break;

            case XZ_MEM_ERROR:
                qCritical() << "Memory allocation failed\n";
                xz_dec_end(s);
                return;

            case XZ_MEMLIMIT_ERROR:
                qCritical() << "Memory usage limit reached\n";
                xz_dec_end(s);
                return;

            case XZ_FORMAT_ERROR:
                qCritical() << "Not a .xz file\n";
                xz_dec_end(s);
                return;

            case XZ_OPTIONS_ERROR:
                qCritical() << "Unsupported options in the .xz headers\n";
                xz_dec_end(s);
                return;

            case XZ_DATA_ERROR:
            case XZ_BUF_ERROR:
                qCritical() << "File is corrupt\n";
                xz_dec_end(s);
                return;

            default:
                qCritical() << "Bug!\n";
                xz_dec_end(s);
                return;
            }
        }
    }
    xz->remove();
}

void Utils::downloadAndDecompressPackXZ(const QString &url, const QString &path) {
    DownloadUtil dl;
    QTemporaryFile *m_pack200_xz_file = new QTemporaryFile("./dl_temp.XXXXXX");
    dl.downloadTempFile(m_pack200_xz_file, url);
    m_pack200_xz_file->seek(0);

    QTemporaryFile *pack200_file = new QTemporaryFile("./dl_temp.XXXXXX");
    pack200_file->open();

    bool xz_success = false;
    // first, de-xz
    {
        uint8_t in[buffer_size];
        uint8_t out[buffer_size];
        struct xz_buf b;
        struct xz_dec *s;
        enum xz_ret ret;
        xz_crc32_init();
        xz_crc64_init();
        s = xz_dec_init(XZ_DYNALLOC, 1 << 26);
        if (s == nullptr) {
            xz_dec_end(s);
            return;
        }
        b.in = in;
        b.in_pos = 0;
        b.in_size = 0;
        b.out = out;
        b.out_pos = 0;
        b.out_size = buffer_size;
        while (!xz_success) {
            if (b.in_pos == b.in_size) {
                b.in_size = m_pack200_xz_file->read((char *)in, sizeof(in));
                b.in_pos = 0;
            }

            ret = xz_dec_run(s, &b);

            if (b.out_pos == sizeof(out)) {
                auto wresult = pack200_file->write((char *)out, b.out_pos);
                if (wresult < 0 || size_t(wresult) != b.out_pos) {
                    // msg = "Write error\n";
                    xz_dec_end(s);
                    return;
                }

                b.out_pos = 0;
            }

            if (ret == XZ_OK)
                continue;

            if (ret == XZ_UNSUPPORTED_CHECK) {
                // unsupported check. this is OK, but we should log this
                continue;
            }

            auto wresult = pack200_file->write((char *)out, b.out_pos);
            if (wresult < 0 || size_t(wresult) != b.out_pos) {
                // write error
                pack200_file->close();
                xz_dec_end(s);
                return;
            }

            switch (ret) {
            case XZ_STREAM_END:
                xz_dec_end(s);
                xz_success = true;
                break;

            case XZ_MEM_ERROR:
                qCritical() << "Memory allocation failed\n";
                xz_dec_end(s);
                return;

            case XZ_MEMLIMIT_ERROR:
                qCritical() << "Memory usage limit reached\n";
                xz_dec_end(s);
                return;

            case XZ_FORMAT_ERROR:
                qCritical() << "Not a .xz file\n";
                xz_dec_end(s);
                return;

            case XZ_OPTIONS_ERROR:
                qCritical() << "Unsupported options in the .xz headers\n";
                xz_dec_end(s);
                return;

            case XZ_DATA_ERROR:
            case XZ_BUF_ERROR:
                qCritical() << "File is corrupt\n";
                xz_dec_end(s);
                return;

            default:
                qCritical() << "Bug!\n";
                xz_dec_end(s);
                return;
            }
        }
    }
    m_pack200_xz_file->remove();

    pack200_file->seek(0);
    int handle_in = pack200_file->handle();

    // FIXME: dispose of file handles, pointers and the like. Ideally wrap in objects.
    if (handle_in == -1) {
        qCritical() << "Error reopening " << pack200_file->fileName();
        return;
    }
    int handle_in_dup = dup(handle_in);
    if (handle_in_dup == -1) {
        qCritical() << "Error reopening " << pack200_file->fileName();
        return;
    }

    FILE *file_in = fdopen(handle_in_dup, "rb");
    if (!file_in) {
        qCritical() << "Error reopening " << pack200_file->fileName();
        return;
    }

    QFile qfile_out(path);
    if (!qfile_out.open(QIODevice::WriteOnly)) {
        qCritical() << "Error opening " << qfile_out.fileName();
        return;
    }

    int handle_out = qfile_out.handle();
    if (handle_out == -1) {
        qCritical() << "Error opening " << qfile_out.fileName();
        return;
    }
    int handle_out_dup = dup(handle_out);
    if (handle_out_dup == -1) {
        qCritical() << "Error reopening " << qfile_out.fileName();
        return;
    }
    FILE *file_out = fdopen(handle_out_dup, "wb");
    if (!file_out) {
        qCritical() << "Error opening " << qfile_out.fileName();
        return;
    }
    try {
        // NOTE: this takes ownership of both FILE pointers. That's why we duplicate them above.
        unpack_200(file_in, file_out);
    } catch (std::runtime_error &err) {
        qCritical() << "Error unpacking " << pack200_file->fileName() << " : " << err.what();
        QFile f(path);
        if (f.exists()) {
            f.remove();
            return;
        }
    }
    pack200_file->remove();
}

bool Utils::copyDirectory(QString src, QString dst, QStringList blacklist) {
    QDir dir(src);
    if (!dir.exists())
        return false;

    for (QString f : dir.entryList(QDir::Files)) {
        if (blacklist.contains(f))
            continue;
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }

    for (QString d : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        if (blacklist.contains(d))
            continue;

        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        Utils::copyDirectory(src + QDir::separator() + d, dst_path);
    }
    return true;
}

int Utils::getInstalledMemory() {
#ifdef Q_OS_UNIX
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGESIZE);
    return (pages * page_size) / 1048576;
#else
    MEMORYSTATUSEX status;
    status.dwLength = sizeof(status);
    GlobalMemoryStatusEx(&status);
    return status.ullTotalPhys / 1048576;
#endif
}

void Utils::updateSettings(Config &config, QString setting, QVariant defaultValue) {
    QSettings settings(config.config_file, QSettings::IniFormat);
    settings.setValue(setting, defaultValue);
}

int Utils::secondsNow() { return QDateTime::currentMSecsSinceEpoch() / 1000; }

QString Utils::formatTime(int duration) {
    QString res;
    int seconds = (int)(duration % 60);
    duration /= 60;
    int minutes = (int)(duration % 60);
    duration /= 60;
    int hours = (int)(duration % 24);
    int days = (int)(duration / 24);
    if ((hours == 0) && (days == 0))
        return res.sprintf("%02d:%02d", minutes, seconds);
    if (days == 0)
        return res.sprintf("%02d:%02d:%02d", hours, minutes, seconds);
    return res.sprintf("%dd%02d:%02d:%02d", days, hours, minutes, seconds);
}
