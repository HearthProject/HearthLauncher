/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "HearthUpdater.h"

#include "BuildEnvironment.h"

#include "GUI/Dialogs/UpdateDialog.h"
#include "Util/DownloadUtil.h"
#include "Util/NetworkUtil.h"

using namespace Updater;

HearthVersion Updater::getLatest() {
    HearthVersion latestVersion;
    QJsonObject versionData = NetworkUtil::get(UPDATE_URI).json.object();
    QString latestKey = versionData["latest"].toString();
    latestVersion.ver = latestKey.toDouble();
    QJsonObject l_obj = versionData["versions"].toObject()[latestKey].toObject();
    latestVersion.url = l_obj["file"].toString();
    latestVersion.changelog = l_obj["changelog"].toString();
    return latestVersion;
}

void Updater::checkUpdates() {
    Updater::HearthVersion latestVersion = Updater::getLatest();
    qInfo() << "Current Version:" << QString::number(VERSION_NUM) << " Latest version:" << latestVersion.ver;
    // if (!(latestVersion.ver > VERSION_NUM)) return;
    UpdateDialog dia(latestVersion);
    if (!dia.exec())
        return;
    QString tempDir = QDir::temp().absolutePath();
    DownloadUtil dl;
    QString newName = tempDir + "/hl3_update.zip";
    dl.downloadFile(latestVersion.url, newName);
    QDir().mkpath(tempDir + "/hl3_update_temp");
    Utils::unzipFolder(newName, tempDir + "/hl3_update_temp");
    QDir().remove(newName);
    QStringList args;
    QProcess proc;

    args << QApplication::applicationDirPath();
    args << Utils::combinePath(tempDir, "hl3_update_temp");
    proc.startDetached("HearthUpdater", args);
    QCoreApplication::exit(0);
}
