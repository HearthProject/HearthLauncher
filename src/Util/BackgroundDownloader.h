/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QtWidgets>

class BackgroundDownloader : public QThread {
    Q_OBJECT
public:
    BackgroundDownloader(QObject *parent = NULL);
    void setTarget(const QString &url, const QString &path);

    void run() override;

    void connectProgress(const QObject *reciever);

signals:
    void downloadDone(QString file);
    void signalMax(int max);
    void signalProgress(int progress);
    void signalMessage(QString message);
    void success();

private:
    QString m_url;
    QString m_path;
};
