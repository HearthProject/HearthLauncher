#pragma once

#include "Themer.h"
#include "Theme.h"
#include <QAbstractListModel>

class ThemeModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ThemeModel(Themer &themer, QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    Theme at(int index) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    int selected();

public slots:
    void setTheme(int row);
private:
    Themer &themer;

};
