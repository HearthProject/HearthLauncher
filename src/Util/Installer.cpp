#include "Installer.h"
#include "Minecraft/MinecraftInstance.h"
#include "Util/BackgroundDownloader.h"
#include "Util/DownloadUtil.h"
#include <QDebug>

#ifdef Q_OS_LINUX
#include <quazip5/JlCompress.h>

#else
#include <JlCompress.h>
#include <quazip.h>
#include <quazipfile.h>
#endif

bool AssetInstaller::install() {
    qInfo() << "Downloading Assets";
    signalMessage("Downloading Assets");

    signalMax(m_assets.objects.keys().size());
    BackgroundDownloader bgdl;
    connect(&bgdl, SIGNAL(signalMessage(QString)), this, SIGNAL(signalMessage(QString)));
    for (int i = 0; i < m_assets.objects.keys().size(); ++i) {
        QString key(m_assets.objects.keys()[i]);
        MinecraftAsset object = m_assets.objects.value(key);
        QString asset = object.hash.mid(0, 2) + "/" + object.hash;
        QString file = config.cache_dir + "/assets/objects/" + asset;
        if (!Utils::fileExists(file)) {
            QDir dir = QFileInfo(file).absoluteDir();
            if (!dir.exists())
                dir.mkpath(dir.absolutePath());
            bgdl.setTarget("http://resources.download.minecraft.net/" + asset, file);
            bgdl.run();
            qInfo() << "Getting Asset: "
                    << "http://resources.download.minecraft.net/" + asset << " -> " << file;
        } else {
            // TODO hash check the file
        }
        signalProgress(i);
    }
    signalProgress(0);
    return true;
}

bool LibraryInstaller::unzipNative(QString source, QString targetFolder, QStringList excludes) {
    qInfo() << "Extracting Native" << source;
    QuaZip zip(source);
    if (!zip.open(QuaZip::mdUnzip)) {
        return false;
    }
    QDir directory(targetFolder);
    if (!zip.goToFirstFile()) {
        return false;
    }
    do {
        QString name = zip.getCurrentFileName();
        QString absFilePath = directory.absoluteFilePath(name);
        bool exclude = false;
        for (QString e : excludes) {
            if (name.contains(e)) {
                exclude = true;
                break;
            }
        }
        if (!exclude)
            JlCompress::extractFile(source, name, absFilePath);
    } while (zip.goToNextFile());
    zip.close();
    if (zip.getZipError() != 0) {
        return false;
    }
    return true;
}

bool LibraryInstaller::install() {
    DownloadUtil dl;
    signalMessage("Installing Mojang Libraries");
    signalMax(m_mc.libraries.size());
    MinecraftFile f;
    for (int i = 0; i < m_mc.libraries.size(); ++i) {
        f = m_mc.libraries[i];
        QString path = Utils::combinePath(m_dir, f.download.path);
        if (!Utils::fileExists(path)) {
            QDir().mkpath(Utils::combinePath(m_dir, QFileInfo(f.download.path).path()));
            signalMessage("Installing Libraries\nDownloading " + f.download.path);
            dl.downloadFile(f.download.url, path);
        }
        // Apparently the : needs to be ; on windows or something
        libraries << QDir::toNativeSeparators(path);
        if (f.native) {
            QString extract_path = Utils::combinePath(Utils::combinePath(config.data_dir, "natives"), m_mc.id);
            QDir().mkpath(extract_path);
            unzipNative(path, extract_path, f.excludes);
        }
        signalProgress(i);
    }
    signalProgress(0);
    return true;
}

bool ForgeInstaller::install() {

    // Install Forge
    if (!m_forge.isEmpty()) {
        QString file = Utils::combinePath(Utils::combinePath(config.data_dir, "versions"), m_forge + ".json");
        DownloadUtil dl;
        if (!Utils::fileExists(file)) {
            QString url = "https://v1.meta.multimc.org/net.minecraftforge/" + m_forge + ".json";
            dl.downloadFile(url, file);
        }
        forge.loadFromFile(file);

        signalMax(forge.libraries.size());
        signalMessage("Installing Forge");

        auto test = [](QString file, QString hash) {
            return !Utils::fileExists(file) || hash.isEmpty() || !Utils::compareChecksum(file, hash);
        };

        QDir().mkpath(m_lib_dir);
        if (server) {
            ForgeLibrary forge_universal = forge.libraries.takeFirst();
            Artifact f(forge_universal.name);
            QString forge_jar = Utils::combinePath(m_lib_dir, f.file_path);
            QFile::rename(forge_jar, Utils::combinePath(m_forge_dir, f.file));
        }
        for (int i = 0; i < forge.libraries.size(); i++) {
            ForgeLibrary lib = forge.libraries[i];
            Artifact a(lib.name);
            QString file = Utils::combinePath(m_lib_dir, a.file_path);

            if (!Utils::fileExists(file + ".sha1")) {
                QDir().mkpath(Utils::combinePath(m_lib_dir, a.path));
                dl.downloadFile(lib.url + a.file_path + ".sha1", file + ".sha1");
            }
            QString b = Utils::combinePath(m_lib_dir, a.path);
            QDir().mkpath(b);
            qDebug() << "Downloading" << a.file << "From" << (lib.url + a.file_path);
            signalMessage("Installing Forge\nDownloading " + a.file);
            if (lib.packxz) {
                Utils::downloadAndDecompressPackXZ(lib.url + a.file_path + ".pack.xz", file);
            } else if (test(file, Utils::readHash(file + ".sha1"))) {
                QDir(Utils::combinePath(m_lib_dir, a.path)).removeRecursively();
                QDir().mkpath(Utils::combinePath(m_lib_dir, a.path));

                dl.downloadFile(lib.url + a.file_path, file);
            }

            libraries << QDir::toNativeSeparators(file);
            signalProgress(i);
        }

        if (server) {
            // Move forge universal jar to server dir
        }

        mainClass = forge.mainClass;
        signalProgress(0);
        return true;
    }
    return false;
}

void Installer::connectProgress(const QObject *receiver) {
    connect(this, SIGNAL(signalMax(int)), receiver, SIGNAL(signalMax(int)));
    connect(this, SIGNAL(signalProgress(int)), receiver, SIGNAL(signalProgress(int)));
    connect(this, SIGNAL(signalMessage(QString)), receiver, SIGNAL(signalMessage(QString)));
}
