/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 0xFireball
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "NetworkUtil.h"

#include "Util/Utils.h"

#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkReply>

QString NetworkUtil::paste(const QString &body, const QString &lang) {
    QJsonObject req;
    req["language"] = lang;
    req["private"] = true;
    req["data"] = body;

    QJsonObject resp =
        NetworkUtil::post(QUrl("https://paste.gnome.org/api/json/create"), req).json.object()["result"].toObject();
    if (resp.contains("error")) {
        qInfo() << "Failed to upload paste:" << resp["error"].toString();
        return NULL;
    }
    return QString("https://paste.gnome.org/%1/%2").arg(resp["id"].toString(), resp["hash"].toString());
}

NetworkUtil::NetworkReply NetworkUtil::post(const QString &url, const QJsonObject &body, const QString &auth) {
    return post(QUrl(url), body,auth);
}
NetworkUtil::NetworkReply NetworkUtil::post(const QUrl &url, const QJsonObject &body, const QString &auth) {
    QNetworkAccessManager netMan;
    QJsonDocument doc(body);
    QNetworkRequest req(url);

    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    if(!auth.isEmpty()) {
        req.setRawHeader(QByteArray("Authorization"), auth.toUtf8());
    }
    QNetworkReply *reply = netMan.post(req, doc.toJson());
    QEventLoop loop;
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    return NetworkReply(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(),
                        QJsonDocument::fromJson(reply->readAll()));
}

NetworkUtil::NetworkReply NetworkUtil::get(const QUrl &url, const QString &auth) {
    QNetworkAccessManager netMan;
    QNetworkRequest req(url);

    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setRawHeader(QByteArray("Authorization"), auth.toUtf8());

    QNetworkReply *reply = netMan.get(req);
    QEventLoop loop;
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    return NetworkReply(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(),
                        QJsonDocument::fromJson(reply->readAll()));
}

NetworkUtil::NetworkReply NetworkUtil::postMultipart(const QUrl &url, QHttpMultiPart *body, const QString &auth) {
    QNetworkAccessManager netMan;
    QNetworkRequest req(url);

    req.setHeader(QNetworkRequest::UserAgentHeader, USER_AGENT);
    req.setRawHeader(QByteArray("Authorization"), auth.toUtf8());

    QNetworkReply *reply = netMan.post(req, body);
    QEventLoop loop;
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    return NetworkReply(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(),
                        QJsonDocument::fromJson(reply->readAll()));
}
