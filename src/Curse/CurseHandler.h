#pragma once

#include <utility>

#include "OneFormat/OneFormat.h"
#include "OneFormat/PackHandler.h"

class CurseHandler : public PackHandler {
public:
    CurseHandler(QString directory, int pid, int fid);
    QString dir;
    int pid, fid;
    OneFormat::PackPtr readPack() override;
};
