#pragma once

#include "OneFormat/PackImporter.h"

class Config;
class HearthAnalytics;

class CurseImporter : public PackImporter {
    Q_OBJECT
public:
    CurseImporter(QWidget *parent, Config &config, HearthAnalytics &tracker);
    bool import(const QString &name) override;

    Config &config;
    HearthAnalytics &tracker;
};
