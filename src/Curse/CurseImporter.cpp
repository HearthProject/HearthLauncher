#include "CurseImporter.h"
#include "CurseHandler.h"
#include "Minecraft/BasePack.h"
#include <OneFormat/PackImporter.h>
#include <QFileInfo>

CurseImporter::CurseImporter(QWidget *parent, Config &config, HearthAnalytics &tracker)
    : PackImporter(tr("Select a Curse Import file"), QStandardPaths::writableLocation(QStandardPaths::DownloadLocation),
                   tr("ZIP (*.zip)"), parent),
      config(config), tracker(tracker) {}

bool CurseImporter::import(const QString &name) {
    if (file.isEmpty() || file.isNull())
        return false;
    QFileInfo info(file);

    QString dir(config.cache_dir + "/packs/" + info.baseName());
    Utils::unzipFolder(file, dir);
    CurseHandler *handler = new CurseHandler(dir, 0, 0);
    OneFormat::PackPtr packptr = handler->readPack();
    if (!name.isEmpty())
        packptr->info->title = name;
    auto *pack = new BasePack(config, tracker, packptr, parent);
    if (pack) {
        QProgressDialog dia(parent);
        dia.setWindowTitle(packptr->info->title);
        connect(pack, SIGNAL(signalMessage(QString)), &dia, SLOT(setLabelText(QString)));
        connect(pack, SIGNAL(signalProgress(int)), &dia, SLOT(setValue(int)));
        connect(pack, SIGNAL(signalMax(int)), &dia, SLOT(setMaximum(int)));
        connect(pack, SIGNAL(installed()), this, SIGNAL(installed()));
        pack->install();
        return true;
    }
    return false;
}
