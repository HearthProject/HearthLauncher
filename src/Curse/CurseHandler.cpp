#include "CurseHandler.h"
#include "Analytics/HearthAnalytics.h"
#include "OneMeta/OneMeta.h"

CurseHandler::CurseHandler(QString directory, int pid, int fid) : dir(std::move(directory)), pid(pid), fid(fid) {}

OneFormat::PackPtr CurseHandler::readPack() {
    auto pack = new OneFormat::Pack(Config::instance(), HearthAnalytics::instance());

    auto manifest = Utils::combinePath(dir, "manifest.json");
    if (Utils::fileExists(manifest)) {
        QFile file(manifest);
        file.open(QIODevice::ReadOnly);
        QJsonObject manifestObject = QJsonDocument::fromJson(file.readAll()).object();
        file.close();

        pack->minecraft = manifestObject["minecraft"].toObject()["version"].toString();
        pack->forge =
            manifestObject["minecraft"].toObject()["modLoaders"].toArray()[0].toObject()["id"].toString().replace(
                "forge-", "");

        OneFormat::PackInfo *info = pack->info;
        info->title = manifestObject["name"].toString();
        info->authors.append(manifestObject["author"].toString());
        info->id = QString("%1").arg(pid);
        info->source = OneMeta::CURSE;
        info->version = manifestObject["version"].toString();

        OneMeta::FilePtr f = OneMeta::getDatabase().findFile(OneMeta::CURSE, fid);
        if (f)
            info->release = f->date;
        // Mods
        QJsonArray manifestMods = manifestObject["files"].toArray();
        QJsonObject c_mod;
        for (int i = 0; i < manifestMods.size(); ++i) {
            c_mod = manifestMods[i].toObject();
            if (c_mod.contains("required") && !c_mod["required"].toBool()) {
                auto t = OneMeta::getDatabase().findProject(OneMeta::CURSE, c_mod["projectID"].toInt())->title;
                qDebug() << t << " is not required";
                continue;
            }
            auto *mod = new OneFormat::PackMod;
            mod->project = c_mod["projectID"].toInt();
            mod->file = c_mod["fileID"].toInt();
            mod->hash = "";
            mod->source = OneMeta::CURSE;
            pack->mods.insert(mod->project, mod);
        }
    }

    QString overrides_dir = Utils::combinePath(dir, "overrides");
    QStringList overrides;
    Utils::findFiles(overrides_dir, &overrides, QStringList());
    for (QString override : overrides) {
        OneFormat::PackFile file;
        file.source = override;
        file.dest = override.replace(overrides_dir, "");
        file.hash = Utils::getChecksum(Utils::combinePath(overrides_dir, override));
        pack->files.insert(file.hash, file);
    }

    return pack;
}
