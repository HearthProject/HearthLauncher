
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include "Analytics/HearthAnalytics.h"
#include "GUI/Main/About.h"
#include "GUI/Main/AuthWidget.h"
#include "GUI/Main/Browse.h"
#include "GUI/Main/Home.h"
#include "GUI/Dialogs/SetupDialog.h"
#include "GUI/Main/MainWindow.h"
#include "GUI/Main/Settings.h"
#include "Hearth/HearthAPI.h"
#include "Hearth/HearthAuthSession.h"
#include "Minecraft/ForgeVersions.h"
#include "Minecraft/MinecraftVersions.h"
#include "Util/AsyncPoster.h"

#include <QNetworkDiskCache>

#include <iostream>
#include <future>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <Util/Themer.h>

static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    static QMutex mutex;
    QMutexLocker lock(&mutex);
    static std::ofstream logFile(Config::instance().log_file.toStdString());

    QString message = qFormatLogMessage(type,context,msg);
#ifdef OS_WIN32
    message.replace(qgetenv("USERNAME"),"%USER%");
#else
    message.replace(qgetenv("USER"), "%USER%");
#endif
    if(logFile) {
        logFile << qPrintable(message) << std::endl;
    }
    std::cout << qPrintable(message) << std::endl;
}

static void setupApplication() {
    QCoreApplication::setOrganizationDomain("hearthproject.uk");
    QCoreApplication::setApplicationName("hearthlauncher");

    QCoreApplication::setApplicationVersion(VERSION_STRING);

}

static void createDirectories(Config &config) {
    auto mk = [](QString p1, QString p2){
        QDir().mkpath(Utils::combinePath(p1,p2));
    };
    QDir().mkpath(config.data_dir);
    mk(config.data_dir, "logs");
    mk(config.data_dir, "natives");
    mk(config.data_dir, "libraries");
    mk(config.data_dir, "versions");

    QDir().mkpath(config.config_dir);
    mk(config.config_dir, "themes");

    QDir().mkpath(config.cache_dir);
    mk(config.cache_dir, "onemeta");
    mk(config.cache_dir, "packs");
    mk(config.cache_dir, "icons");
    mk(config.cache_dir, "icons");
    mk(config.cache_dir, "assets");
    mk(config.cache_dir + QDir::separator() + "assets", "indexes");
    mk(config.cache_dir + QDir::separator() + "assets", "objects");
}

Q_DECLARE_METATYPE(std::shared_ptr<OneMeta::Provider>)

void configureAsync(QApplication &a, AsyncLoad &loader) {
    qRegisterMetaType<std::shared_ptr<OneMeta::Provider>>();
    QObject::connect(
            &loader, &AsyncLoad::provider_loaded, &a, [](std::shared_ptr<OneMeta::Provider> provider, int type) {
                OneMeta::database.providers.insert(static_cast<OneMeta::SourceType>(type), provider);
            });
    QObject::connect(&loader, &AsyncLoad::minecraft_version_manifest_loaded, &a,
                     [](QString path) { MinecraftVersions::instance()->loadFromFile(path); });
    QObject::connect(&loader, &AsyncLoad::forge_version_manifest_loaded, &a,
                     [](QString path) { ForgeVersions::instance()->loadFromFile(path); });
}

int main(int argc, char *argv[]) {
    ScopedTask seSetup("Initial Setup");
    qSetMessagePattern("[%{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}] %{file}:%{line} - %{message}");
    qInstallMessageHandler(messageHandler);
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);

    QTranslator translator;
    translator.load(QString("hearthlauncher_") + QLocale::system().name());
    a.installTranslator(&translator);

    setupApplication();
    Config &config = Config::instance();

    config.parseCommandline(a);
    createDirectories(config);

    QSettings settings(config.config_file, QSettings::IniFormat);
    qInfo() << "Starting Hearth Launcher" << VERSION_STRING;
    qInfo() << "Built by" << BUILDER_NAME;
    qInfo() << "Running on" << OS_NAME;
    qInfo() << "Data Dir" << config.data_dir;
    qInfo() << "Settings Dir" << config.config_dir;
    qInfo() << "Cache Dir" << config.cache_dir;
    qInfo() << "Settings File" << settings.fileName();
    qDebug() << "PID is" << QCoreApplication::applicationPid();
    qDebug() << "Installed memory" << Utils::getInstalledMemory() << "MB";

    if (!settings.value("analytics/uuid", false).toBool()) settings.setValue("analytics/uuid", Utils::generateUUID());
    if (!settings.value("paste/host", false).toBool()) settings.setValue("paste/host", "https://paste.gnome.org");
    config.analyticsId = settings.value("analytics/uuid").toString();

    seSetup.end();
    ScopedTask seNonGUI("non-GUI Setup");

    // Create non-gui objects.
    QNetworkAccessManager network, network_nocache;

    QNetworkDiskCache cache;
    QThread loadThread;
    AsyncLoad loader(&loadThread, config, network_nocache);
    QThread postThread;
    AsyncPoster poster(&postThread);
    HearthAnalytics tracker(config);
    AuthSession authSession(config);
    MinecraftVersions mv(config);
    ForgeVersions fv;
    HearthAPI ha;
    HearthAuthSession has;
    Themer themer(config);

    minecraftVersions = &mv;
    forgeVersions = &fv;
    hearthAPI = &ha;
    hearthAuthSession = &has;

    cache.setCacheDirectory(config.cache_dir);
    network.setCache(&cache);

    QObject::connect(&tracker, &HearthAnalytics::post, &poster, &AsyncPoster::post);

    QObject::connect(&a, &QApplication::aboutToQuit, &a, [&] {
        loadThread.quit();
        postThread.quit();
        loadThread.wait();
        postThread.wait();
    });

    configureAsync(a, loader);

    loadThread.start();
    postThread.start();

    std::future<void> HearthAPIInited = std::async(std::launch::async, [&ha, &loader] {
        ha.init();
        invoke_slot(&loader, &AsyncLoad::reloadDatabase);
    });

    seNonGUI.end();
    ScopedTask seGUISetup("GUI Setup");

    auto splash = new QSplashScreen(QPixmap(":/icons/splash.png"));
    splash->show();
    ScopedTask seEvents("Process Events");
    Utils::splashMessage(splash, "Loading...");
    seEvents.end();

    // Create gui objects.
    auto about = new About;
    auto authWidget = new AuthWidget(authSession, config);
    auto browse = new Browse(OneMeta::MODPACK, loader, new PackWidgetHandler(config, tracker), 0, "Modpack Browsing");
    auto home = new Home(authSession, loader, config, tracker);
    auto settingsWindow = new Settings(loader,config, themer);
    QObject::connect(browse, SIGNAL(installed()), home, SLOT(scanInstances()));

    MainWindow mw(*about, *authWidget, *browse, *home, *settingsWindow, config, themer);
    QObject::connect(&loader, &AsyncLoad::loaded, home, &Home::scanInstances);
    QObject::connect(&loader, &AsyncLoad::loaded, browse, [browse] {
        browse->updateSearch();
        browse->_ui.search_refresh->setDisabled(false);
    });

    home->scanInstances();

    if (!OneMeta::database.providers.empty()) {
        // AsyncLoad::loaded got emitted during `Utils::splashMessage(...)` above before the above signal handler was
        // connected, so handle it here.
        browse->updateSearch();
        browse->_ui.search_refresh->setDisabled(false);
    }

    QObject::connect(authWidget->_ui.offline, &QCheckBox::stateChanged, &a,
                     [&config](int state) { config.offline = static_cast<bool>(state); });

    if (!settings.value("setup_done", false).toBool()) {
        seEvents.start();
        Utils::splashMessage(splash, "Starting Initial Setup");
        seEvents.end();
        splash->close();
        SetupDialog w(config, tracker, &mw);
        if (!w.exec())
            return EXIT_SUCCESS;
    }

    tracker.sendEvent(HearthEventType::LAUNCHED);

    seEvents.start();
    Utils::splashMessage(splash, "Loading Hearth");
    seEvents.end();
    {
        ScopedTask st_hwait("Wait for HearthAPI");
        HearthAPIInited.wait();
    }
    seEvents.start();
    Utils::splashMessage(splash, "Finished");
    seEvents.end();

    mw.show();
    splash->finish(&mw);

    seGUISetup.end();
    ScopedTask run("Running");

    return a.exec();
}
