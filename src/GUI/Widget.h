#pragma once

#include <QPainter>
#include <QStyleOption>
#include <QWidget>

class Widget : public QWidget {
public:
    Widget(QWidget *parent) : QWidget(parent) {}

public slots:
    void paintEvent(QPaintEvent *event) {
        QStyleOption opt;
        opt.init(this);
        QPainter p(this);
        style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    }
};
