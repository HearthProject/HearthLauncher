#include "CreateWidget.h"
#include <Analytics/HearthAnalytics.h>
#include <Minecraft/ForgeVersions.h>
#include <Minecraft/MinecraftVersions.h>

CreateWidget::CreateWidget(Config &conf, HearthAnalytics &ha, MinecraftInstance *instance, QWidget *parent)
    : Widget(parent), config(conf), m_instance(instance) {
    _ui.setupUi(this);
    _ui.icon->setIcon(QIcon(":/icons/hearth.png"));
    _ui.minecraft->addItems(MinecraftVersions::instance()->getVersions());
    setForgeVersions(_ui.minecraft->currentText());
    if (m_instance) {
        _ui.minecraft->setCurrentText(m_instance->minecraft());
        setForgeVersions(m_instance->minecraft());
        _ui.forge->setCurrentText(m_instance->forge());
        _ui.name->setText(m_instance->name());
        _ui.icon->setIcon(QIcon(m_instance->icon()));
        connect(_ui.icon, &QToolButton::clicked, this, [&] {
            file = selectIcon();
            setIcon(file);
        });
        connect(_ui.update, &QPushButton::clicked, this, &CreateWidget::updateData);
    } else {
        _ui.update->setVisible(false);
        connect(_ui.icon, &QToolButton::clicked, this, [&] { file = selectIcon(); });
    }
    connect(_ui.minecraft, SIGNAL(currentTextChanged(QString)), this, SLOT(setForgeVersions(QString)));
}

void CreateWidget::updateData() {
    QString name(_ui.name->text());
    if (name != m_instance->name())
        m_instance->setName(name);
    QString minecraft(_ui.minecraft->currentText());
    if (minecraft != m_instance->minecraft()) {
        m_instance->setMinecraft(minecraft);
    }
    QString forge(_ui.forge->currentText());
    if (forge != m_instance->forge()) {
        m_instance->setForge(forge);
    }
    QString serverDir = Utils::combinePath(m_instance->directory(), "server");
    if (!Utils::fileExists(serverDir)) {
        if (_ui.server->isChecked())
            m_instance->installServer(serverDir);
    }
}

bool CreateWidget::create(Config &config, HearthAnalytics &ha) {
    if (m_instance)
        return false;

    QString name(_ui.name->text()), version(_ui.minecraft->currentText());
    if (!name.isEmpty() && !version.isEmpty()) {
        ha.sendEvent(HearthEventType::INSTANCE_CREATED, version);
        m_instance = new MinecraftInstance(_ui.name->text(), config, ha, this);
        m_instance->setupInstance(_ui.minecraft->currentText(), _ui.forge->currentText());
        setIcon(file);
        if (_ui.server->isChecked())
            m_instance->installServer(Utils::combinePath(m_instance->directory(), "server"));
        return true;
    }
    return false;
}

void CreateWidget::setForgeVersions(const QString &minecraft) {
    _ui.forge->clear();
    _ui.forge->addItem("None");
    _ui.forge->addItems(ForgeVersions::instance()->getAvailableVersions(minecraft));
}

void CreateWidget::setIcon(QString file) {
    QFileInfo info(file);
    if (!Utils::fileExists(info.absoluteFilePath()))
        return;
    QString icon(info.absoluteFilePath());
    m_instance->setIcon(info.fileName());
}

QString CreateWidget::selectIcon() {
    QString image_file(QFileDialog::getOpenFileName(nullptr, tr("Select Icon Image"), config.icon_dir,
                                                    tr("Images (*.png *.xpm *.jpg *.gif)")));
    if (image_file.isNull())
        return QString();
    QFileInfo icon(image_file);
    QString icon_filename = Utils::combinePath(config.icon_dir, icon.fileName());
    QFile().copy(image_file, icon_filename);
    Utils::scaleImage(icon.absoluteFilePath());
    _ui.icon->setIcon(QIcon(icon_filename));
    return icon_filename;
}
