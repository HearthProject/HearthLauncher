#include "ModListWidget.h"
#include "Minecraft/FileMod.h"

ModListWidget::ModListWidget(MinecraftInstance *instance, QWidget *parent)
    : Widget(parent), instance(instance), modWatcher(new QFileSystemWatcher(this)) {
    _ui.setupUi(this);

    connect(this, SIGNAL(modsChanged()), this, SLOT(populate()));

    modWatcher->addPath(this->instance->getSubDir("mods"));
    connect(modWatcher, SIGNAL(directoryChanged(QString)), this, SIGNAL(modsChanged()));
    mapper = new QDataWidgetMapper(this);
    mods = new ModTableModel(this);

    model = new QSortFilterProxyModel(this);
    model->setSourceModel(mods);

    mapper->setModel(model);
    mapper->addMapping(_ui.mod_name, 0);

    _ui.list->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    _ui.list->setSelectionMode(QTableView::SingleSelection);
    _ui.list->horizontalHeader()->setStretchLastSection(true);
    _ui.list->setModel(model);
    connect(_ui.list->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)), mapper,
            SLOT(setCurrentModelIndex(QModelIndex)));
    connect(mapper, &QDataWidgetMapper::currentIndexChanged, this, &ModListWidget::changeSelection);
    connect(_ui.mods_folder, &QPushButton::clicked, this,
            [&] { QDesktopServices::openUrl(QUrl::fromLocalFile(this->instance->getSubDir("mods"))); });

    _ui.mod_remove->setEnabled(false);
    _ui.mod_update->setEnabled(false);

    connect(_ui.mods_add, SIGNAL(clicked(bool)), this, SLOT(addMod()));

    populate();
}

void ModListWidget::addMod() {
    auto dir = instance->getSubDir("mods");
    QString file = QFileDialog::getOpenFileName(this, tr("Select a mod file"), dir, "Mods (*.jar *.zip)");
    mods->addMod(new FileMod(instance, file));
    emit modsChanged();
}

void ModListWidget::changeSelection() {
    for (auto connection : connections) {
        QObject::disconnect(connection);
    }
    if (_ui.list->currentIndex().isValid()) {

        auto mod = mods->at(_ui.list->currentIndex().row());

        _ui.mod_remove->setEnabled(true);
        connections << connect(_ui.mod_remove, &QPushButton::clicked, this,
                               [&] { mods->removeMod(_ui.list->currentIndex().row()); });

        if (mod->versionsBehind()) {
            _ui.mod_update->setEnabled(true);
            connections << connect(_ui.mod_update, &QPushButton::clicked, this,
                                   [&] { mods->at(_ui.list->currentIndex().row())->update(this); });
        } else {
            _ui.mod_update->setEnabled(false);
        }
    } else {
        _ui.mod_remove->setEnabled(false);
        _ui.mod_update->setEnabled(false);
    }
}

void ModListWidget::populate() {
    mods->setMods(instance->mods());
    model->setSourceModel(mods);
}
