#include "ImportWidget.h"

#include "Curse/CurseImporter.h"
#include "OneFormat/OFImporter.h"
#include <QLineEdit>

ImportWidget::ImportWidget(Config &config, HearthAnalytics &tracker, QWidget *parent)
    : config(config), tracker(tracker), Widget(parent) {
    _ui.setupUi(this);
    _ui.comboBox->addItems(QStringList({tr("OneFormat Importer"), tr("Curse Import")}));

    connect(_ui.comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeImporter(int)));

    changeImporter(_ui.comboBox->currentIndex());
}

bool ImportWidget::import() {
    if (importer) {
        return importer->import(_ui.instance_name->text());
    }
    return false;
}

void ImportWidget::changeImporter(int i) {
    if (i == 0)
        importer = new OFImporter(this, config, tracker);
    else if (i == 1)
        importer = new CurseImporter(this, config, tracker);
    for (auto connect : connections)
        QObject::disconnect(connect);
    if (importer) {
        connections << connect(_ui.selectFile, SIGNAL(clicked(bool)), importer, SLOT(selectFile()));
        connections << connect(importer, SIGNAL(signalFile(QString)), _ui.file, SLOT(setText(QString)));
        connections << connect(importer, SIGNAL(installed()), this, SIGNAL(installed()));
        connections << connect(_ui.file, &QLineEdit::editingFinished, this,
                               [this] { importer->setFile(_ui.file->text()); });
        connections << connect(_ui.file, &QLineEdit::textChanged, this, [&](QString text) {
            QFileInfo info(text);
            _ui.instance_name->setText(info.baseName());
        });
    }
}
