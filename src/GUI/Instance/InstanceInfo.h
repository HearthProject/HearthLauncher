#pragma once
#include "GUI/Instance/CreateWidget.h"
#include "GUI/Widget.h"
#include "OneFormat/OneFormat.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

class Config;
class MinecraftInstance;
class HearthAnalytics;

class InstanceInfo : public Widget {
    Q_OBJECT
public:
    explicit InstanceInfo(MinecraftInstance *instance, Config &conf, HearthAnalytics &ha, QWidget *parent = nullptr);
    QVBoxLayout *layout;
    CreateWidget *create;
    QPushButton *exporter;
    OneFormat::PackPtr m_pack;
};
