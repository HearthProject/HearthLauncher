/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "InstanceWindow.h"

#include "Minecraft/MinecraftVersions.h"

InstanceWindow::InstanceWindow(MinecraftInstance *instance, AsyncLoad &loader, Config &conf, HearthAnalytics &ha,
                               QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags), m_instance(instance), config(conf), tracker(ha), loader(loader) {
    _ui.setupUi(this);

    browse = new Browse(OneMeta::MOD, loader, new ModWidgetHandler(config, tracker, m_instance), this, tr("Mod Browsing"), m_instance);
    info = new InstanceInfo(m_instance, config, tracker, this);
    mods = new ModListWidget(m_instance, this);

    _ui.browse_tab->layout()->addWidget(browse);
    _ui.instance_tab->layout()->addWidget(info);
    _ui.mods_tab->layout()->addWidget(mods);
    _ui.tabs->buttonLayout->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));

    QObject::connect(browse, SIGNAL(installed()), mods, SLOT(populate()));
}

void InstanceWindow::showEvent(QShowEvent *event) {
    QObject::connect(&loader, &AsyncLoad::loaded, this, [this] {
        browse->updateSearch();
        browse->_ui.search_refresh->setDisabled(false);
    });
    browse->updateSearch();
}

void InstanceWindow::paintEvent(QPaintEvent *e) {
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
