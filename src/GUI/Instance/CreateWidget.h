#pragma once

#include "ui_CreateWidget.h"
#include "Analytics/HearthAnalytics.h"
#include "GUI/Widget.h"
#include "Minecraft/AuthSession.h"
#include "Minecraft/MinecraftInstance.h"

class CreateWidget : public Widget {
    Q_OBJECT
public:
    explicit CreateWidget(Config &conf, HearthAnalytics &ha, MinecraftInstance *instance = 0, QWidget *parent = 0);
    Ui_CreateWidget _ui;
    Config &config;

    bool create(Config &config, HearthAnalytics &ha);

    QString selectIcon();

    void updateData();
public slots:
    void setForgeVersions(const QString &minecraft);
    void setIcon(QString file);

private:
    MinecraftInstance *m_instance;
    QString file;
};
