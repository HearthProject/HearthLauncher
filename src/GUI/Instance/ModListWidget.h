
#pragma once

#include "ui_ModList.h"
#include "GUI/Model/ModTableModel.h"
#include "GUI/Widget.h"

#include <QDataWidgetMapper>
#include <QVector>

class MinecraftInstance;

class ModListWidget : public Widget {
    Q_OBJECT
public:
    explicit ModListWidget(MinecraftInstance *instance, QWidget *parent = nullptr);
    Ui_ModList _ui;

    MinecraftInstance *instance;

public slots:
    void populate();
    void changeSelection();
    void addMod();
signals:
    void modsChanged();

private:
    QList<QMetaObject::Connection> connections;
    ModTableModel *mods;
    QSortFilterProxyModel *model;
    QDataWidgetMapper *mapper;
    QFileSystemWatcher *modWatcher;
};
