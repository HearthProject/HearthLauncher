/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "CreateWidget.h"
#include "InstanceInfo.h"
#include "ui_InstanceWindow.h"
#include "GUI/Instance/ModListWidget.h"
#include "GUI/Main/Browse.h"
#include "GUI/ProjectWidget.h"
#include "Minecraft/MinecraftInstance.h"
#include "OneMeta/OneMeta.h"
#include <QMainWindow>
#include <QSettings>
#include <QtWidgets>

class Config;
class HearthAnalytics;
class AuthSession;

class InstanceWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit InstanceWindow(MinecraftInstance *instance, AsyncLoad &loader, Config &conf, HearthAnalytics &ha,
                            QWidget *parent = nullptr, Qt::WindowFlags flags = 0);
    Browse *browse;
    InstanceInfo *info;
    ModListWidget *mods;

public slots:
    void showEvent(QShowEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    MinecraftInstance *m_instance;
    Config &config;
    HearthAnalytics &tracker;
    AsyncLoad &loader;
    Ui_InstanceWindow _ui;
};
