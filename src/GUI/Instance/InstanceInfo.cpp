#include "InstanceInfo.h"
#include "Minecraft/MinecraftInstance.h"
#include <QFileDialog>

InstanceInfo::InstanceInfo(MinecraftInstance *instance, Config &conf, HearthAnalytics &ha, QWidget *parent)
    : Widget(parent), m_pack(instance->pack()) {
    create = new CreateWidget(conf, ha, instance, this);
    layout = new QVBoxLayout(this);
    exporter = new QPushButton(tr("Export"), this);
    layout->addWidget(create);
    layout->addWidget(new QLabel(QString("%1").arg(instance->pack()->info->version)));
    layout->addWidget(exporter);
    setLayout(layout);
    connect(exporter, &QPushButton::clicked, this, [&] {
        auto path = Utils::combinePath(QStandardPaths::writableLocation(QStandardPaths::HomeLocation),
                                       m_pack->info->title + ".1f");
        m_pack->exportPack(
            QFileDialog::getSaveFileName(this, tr("Select an Export location"), path, tr("OneFormat (*.1f)")));
    });
}
