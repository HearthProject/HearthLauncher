/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "InstanceWidget.h"

#include "GUI/Main/AuthWidget.h"
#include "Minecraft/BasePack.h"

InstanceWidget::InstanceWidget(MinecraftInstance *instance, AuthSession &authSession, AsyncLoad &loader, Config &conf,
                               HearthAnalytics &tracker, QWidget *parent)
    : Widget(parent), m_instance(instance), authSession(authSession), loader(loader), config(conf), tracker(tracker),
      log_win(new LogWindow(parent)) {
    _ui.setupUi(this);
    _ui.instance_name->setText(instance->name());
    icon = m_instance->icon();

    connect(_ui.play_button, &QToolButton::clicked, this, &InstanceWidget::launchClicked);
    connect(_ui.instance_delete, &QToolButton::clicked, this, &InstanceWidget::remove);
    connect(_ui.instance_edit, &QToolButton::clicked, this, &InstanceWidget::showWindow);
    connect(_ui.share_button, &QToolButton::clicked, this,
            [&] { QDesktopServices::openUrl(QUrl::fromLocalFile(m_instance->getSubDir())); });

    connect(_ui.instance_update, &QToolButton::clicked, m_instance, [&] { m_instance->update(false, this); });

    setIcon(icon);
    connect(m_instance, SIGNAL(signalIcon(QIcon)), this, SLOT(setIcon(QIcon)));
    connect(m_instance, SIGNAL(signalName(QString)), this, SLOT(setName(QString)));

    connect(log_win, &LogWindow::dump, this, [&] { m_instance->debugDump(log_win->text()); });

    connect(log_win, SIGNAL(kill()), m_instance, SLOT(kill()));

    connect(m_instance, SIGNAL(log(QString)), log_win, SLOT(log(QString)));
    connect(m_instance, SIGNAL(preLaunch()), log_win, SLOT(show()));
}

void InstanceWidget::showWindow() {
    if (!window) {
        window = new InstanceWindow(m_instance, loader, config, tracker, this);
    }
    if (window) {
        window->show();
    }
}

void InstanceWidget::launchClicked() {
    if (!config.offline && !authSession.valid())
        if (!authSession.auth())
            return;
    if(!authSession.validateToken())
        authSession.refresh();
    m_instance->launch(authSession);
    emit(instanceLaunching());
}

void InstanceWidget::remove() {
    if (QMessageBox::question(this, tr("Delete Confirmation"),
                              QString(tr("Are you sure you want to delete %1?")).arg(m_instance->name()), QMessageBox::Yes,
                              QMessageBox::No) == QMessageBox::Yes) {
        m_instance->remove();
    }
}

void InstanceWidget::setIcon(QIcon icon) {
    auto size = _ui.icon->size();
    _ui.icon->setPixmap(icon.pixmap(size).scaled(size));
}

void InstanceWidget::setName(QString name) { _ui.instance_name->setText(name); }

void InstanceWidget::mouseDoubleClickEvent(QMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        launchClicked();
    }
}
