/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "ui_InstanceWidget.h"

#include "GUI/Instance/InstanceWindow.h"
#include "Minecraft/MinecraftInstance.h"
#include "Util/BackgroundDownloader.h"

#include "GUI/LogWindow.h"
#include "GUI/Widget.h"
#include <QWidget>
#include <QtWidgets>

class AuthSession;
class AuthWidget;
class Config;
class HearthAnalytics;

class InstanceWidget : public Widget {
    Q_OBJECT
public:
    explicit InstanceWidget(MinecraftInstance *instance, AuthSession &authSession, AsyncLoad &loader, Config &conf,
                            HearthAnalytics &ha, QWidget *parent = nullptr);
    Ui_InstanceWidget _ui;

    void showWindow();

public slots:
    void launchClicked();
    void remove();
    void setIcon(QIcon icon);
    void setName(QString name);
    void mouseDoubleClickEvent(QMouseEvent *e);

signals:
    void instanceLaunching();

private:
    MinecraftInstance *m_instance;
    AuthSession &authSession;
    AsyncLoad &loader;
    Config &config;
    HearthAnalytics &tracker;
    InstanceWindow *window = 0;
    QIcon icon;
    LogWindow *log_win;
};
