#pragma once
#include "GUI/Widget.h"
#include <QWidget>

#include "ui_ImportWidget.h"
#include "OneFormat/PackImporter.h"

class Config;
class HearthAnalytics;

class ImportWidget : public Widget {
    Q_OBJECT
public:
    explicit ImportWidget(Config &config, HearthAnalytics &tracker, QWidget *parent = nullptr);
    Ui_ImportWidget _ui;

    PackImporter *importer = nullptr;

public slots:
    void changeImporter(int i);
    bool import();
signals:
    void installed();

private:
    QList<QMetaObject::Connection> connections;
    Config &config;
    HearthAnalytics &tracker;
};
