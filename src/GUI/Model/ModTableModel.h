#pragma once

#include "Minecraft/MetaMod.h"
#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include <QVector>

class ModTableModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum Columns { ActiveColumn = 0, NameColumn, VersionColumn, NUM_COLUMNS };
    ModTableModel(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    void removeMod(int i);

    QVector<BaseModPtr> mods() const;

    BaseModPtr at(int row) const;

    void setMods(QVector<BaseModPtr> mods);
    void addMod(BaseModPtr mod);

private:
    QVector<BaseModPtr> m_mods;
};
