#pragma once

#include "OneMeta/OneMeta.h"
#include <QAbstractListModel>
#include <QList>

class QProjectListModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum AdditionalRoles { ProjectPointerRole = 0x34B1CB48 };

    QProjectListModel(QObject *parent = nullptr) : QAbstractListModel(parent) {}
    QProjectListModel(QList<OneMeta::ProjectPtr> list, QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;

    int rowCount(const QModelIndex &parent) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;

    OneMeta::ProjectPtr at(int i) { return list.at(i); }

protected:
    QList<OneMeta::ProjectPtr> list;
};
