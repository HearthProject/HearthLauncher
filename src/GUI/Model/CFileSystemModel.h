#pragma once

#include <QFileIconProvider>
#include <QFileSystemModel>
#include <QPersistentModelIndex>
#include <QSet>

class IconProvider : public QFileIconProvider {
public:
    IconProvider() : QFileIconProvider() {}

    QIcon icon(const QFileInfo &info) const { return QIcon(); }

    QIcon icon(IconType type) const { return QIcon(); }
};

class CFileSystemModel : public QFileSystemModel {
public:
    CFileSystemModel();
    mutable QSet<QPersistentModelIndex> checkedIndexes;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant data(const QModelIndex &index, int role) const;
signals:
    void changeStatus(const QModelIndex &index, bool checked);

private:
    void recursiveCheck(const QModelIndex &index, const QVariant &value);
};
