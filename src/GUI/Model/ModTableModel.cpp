#include "ModTableModel.h"

ModTableModel::ModTableModel(QObject *parent) : QAbstractListModel(parent) {}

int ModTableModel::rowCount(const QModelIndex & /*parent*/) const { return m_mods.size(); }

int ModTableModel::columnCount(const QModelIndex & /*parent*/) const { return NUM_COLUMNS; }

Qt::ItemFlags ModTableModel::flags(const QModelIndex &index) const {
    return QAbstractListModel::flags(index) | Qt::ItemIsUserCheckable;
}

QVariant ModTableModel::data(const QModelIndex &index, int role) const {
    int column = index.column();
    auto mod = at(index.row());
    switch (role) {
    case Qt::CheckStateRole:
        switch (column) {
        case ActiveColumn:
            return mod->enabled() ? Qt::Checked : Qt::Unchecked;
        default:
            return QVariant();
        }
    case Qt::DisplayRole:
        switch (column) {
        case ActiveColumn:
            return QString();
        case NameColumn:
            return mod->name();
        case VersionColumn: {
            int v = mod->versionsBehind();
            return (v == 0) ? tr("Latest") : QString(tr("Behind %1 version(s)")).arg(v);
        }
        default:
            return QVariant();
        }
    }

    return QVariant();
}

bool ModTableModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (role == Qt::CheckStateRole) {
        BaseModPtr mod = at(index.row());
        bool e = mod->enabled();
        if (mod->setEnabled(!e)) {
            emit dataChanged(index, index);
            return true;
        }
    }
    return QAbstractListModel::setData(index, value, role);
}

BaseModPtr ModTableModel::at(int row) const { return m_mods.at(row); }

void ModTableModel::setMods(QVector<BaseModPtr> mods) {
    beginResetModel();
    m_mods = mods;
    endResetModel();
}

void ModTableModel::addMod(BaseModPtr mod) {
    beginInsertRows(QModelIndex(), m_mods.size(), m_mods.size());
    m_mods.append(mod);
    endInsertRows();
}

QVector<BaseModPtr> ModTableModel::mods() const { return m_mods; }

void ModTableModel::removeMod(int i) {
    beginRemoveRows(QModelIndex(), i, i);
    auto m = m_mods.takeAt(i);
    m->remove();
    endRemoveRows();
}

QVariant ModTableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation != Qt::Horizontal)
        return QVariant();
    switch (role) {
    case Qt::DisplayRole:
        switch (section) {
        case ActiveColumn:
            return QString();
        case NameColumn:
            return tr("Name");
        case VersionColumn:
            return tr("Version");
            //        case DateColumn:
            //            return tr("Last changed");
        default:
            return QVariant();
        }

    case Qt::ToolTipRole:
        switch (section) {
        case ActiveColumn:
            return tr("Is the mod enabled?");
        case NameColumn:
            return tr("The name of the mod.");
        case VersionColumn:
            return tr("Mod Version");
            //        case DateColumn:
            //            return tr("The date and time this mod was last changed (or added).");
        default:
            return QVariant();
        }
    default:
        return QVariant();
    }
    return QVariant();
}
