#include "InstanceModel.h"

InstanceModel::InstanceModel(QObject *parent) : QAbstractItemModel(parent) {}

QVariant InstanceModel::headerData(int section, Qt::Orientation orientation, int role) const {
    switch (role) {
    case Qt::DisplayRole:
        return "Hello";
    }
    return QVariant();
}

QModelIndex InstanceModel::index(int row, int column, const QModelIndex &parent) const {
    return createIndex(row, column, new QModelIndex());
}

QModelIndex InstanceModel::parent(const QModelIndex &index) const { return index; }

int InstanceModel::rowCount(const QModelIndex &parent) const {
    if (!parent.isValid())
        return 0;
    return 1;
}

int InstanceModel::columnCount(const QModelIndex &parent) const {
    if (!parent.isValid())
        return 0;
    return 1;
}

QVariant InstanceModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    switch (role) {
    case Qt::DisplayRole:
        return "Hello";
    }

    return QVariant();
}
