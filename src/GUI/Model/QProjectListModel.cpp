#include "QProjectListModel.h"

QProjectListModel::QProjectListModel(QList<OneMeta::ProjectPtr> list, QObject *parent)
    : QAbstractListModel(parent), list(std::move(list)) {}

QVariant QProjectListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() >= list.size())
        return QVariant();

    auto *project = static_cast<OneMeta::Project *>(index.internalPointer());
    QVariant v = qVariantFromValue((void *)project);

    switch (role) {
    case ProjectPointerRole: {
        return v;
    }
    case Qt::DisplayRole: {
        return project->title;
    }
    }
    return QVariant();
}

int QProjectListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return list.size();
}

QModelIndex QProjectListModel::index(int row, int column, const QModelIndex &parent) const {
    Q_UNUSED(parent);
    if (row < 0 || row >= list.size())
        return QModelIndex();
    return createIndex(row, column, (void *)list.at(row).get());
}
