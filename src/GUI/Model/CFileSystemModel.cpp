#include "CFileSystemModel.h"
#include <QDebug>

CFileSystemModel::CFileSystemModel() { setIconProvider(new IconProvider); }

Qt::ItemFlags CFileSystemModel::flags(const QModelIndex &index) const {
    return QFileSystemModel::flags(index) | Qt::ItemIsUserCheckable;
}

QVariant CFileSystemModel::data(const QModelIndex &index, int role) const {
    if (role == Qt::CheckStateRole) {
        if (checkedIndexes.contains(index)) {
            return checkedIndexes.contains(index) ? Qt::Checked : Qt::Unchecked;
        } else {
            int checked = Qt::Unchecked;

            QModelIndex parent = index.parent();
            while (parent.isValid()) {
                if (checkedIndexes.contains(parent)) {
                    checked = Qt::Checked;
                    break;
                }

                parent = parent.parent();
            }

            if (checked == Qt::Checked) {
                checkedIndexes.insert(parent);
            }

            return checked;
        }
    } else {
        return QFileSystemModel::data(index, role);
    }
}

bool CFileSystemModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (role == Qt::CheckStateRole) {
        if (value == Qt::Checked) {
            checkedIndexes.insert(index);

        } else {
            checkedIndexes.remove(index);
            if (hasChildren(index) == true) {
                recursiveCheck(index, value);
            }
        }
        emit dataChanged(index, index);
        return true;
    }
    return QFileSystemModel::setData(index, value, role);
}

void CFileSystemModel::recursiveCheck(const QModelIndex &index, const QVariant &value) {
    if (hasChildren(index)) {
        int i;
        int childrenCount = rowCount(index);
        QModelIndex child;
        for (i = 0; i < childrenCount; i++) {
            child = QFileSystemModel::index(i, 0, index);
            setData(child, value, Qt::CheckStateRole);
        }
    }
}
