#pragma once
#include <QStylePainter>
#include <QStyledItemDelegate>
class PushButtonDelegate : public QStyledItemDelegate {
public:
    QAbstractItemView *view;

    PushButtonDelegate(QAbstractItemView *view, QObject *parent) : QStyledItemDelegate(parent), view(view) {}

    void PushButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                                   const QModelIndex &index) const {
        QStylePainter stylePainter(view);
        stylePainter->drawControl(QStyle::CE_PushButton, option);
    }
};
