#include "InstanceModsModel.h"

InstanceModsModel::InstanceModsModel(QObject *parent) : QAbstractItemModel(parent) {}

QVariant InstanceModsModel::headerData(int section, Qt::Orientation orientation, int role) const {
    // FIXME: Implement me!
}

QModelIndex InstanceModsModel::index(int row, int column, const QModelIndex &parent) const {
    // FIXME: Implement me!
}

QModelIndex InstanceModsModel::parent(const QModelIndex &index) const {
    // FIXME: Implement me!
}

int InstanceModsModel::rowCount(const QModelIndex &parent) const {
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

int InstanceModsModel::columnCount(const QModelIndex &parent) const {
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

bool InstanceModsModel::hasChildren(const QModelIndex &parent) const {
    // FIXME: Implement me!
}

bool InstanceModsModel::canFetchMore(const QModelIndex &parent) const {
    // FIXME: Implement me!
    return false;
}

void InstanceModsModel::fetchMore(const QModelIndex &parent) {
    // FIXME: Implement me!
}

QVariant InstanceModsModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}
