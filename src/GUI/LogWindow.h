/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "ui_LogWindow.h"
#include <QVector>
#include <QtWidgets>

class LogWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit LogWindow(QWidget *parent = nullptr, Qt::WindowFlags flags = 0);
    Ui_LogWindow _ui;
    QString text() const;

    static QVector<LogWindow *> windows;
public slots:
    void log(QString message);

signals:
    void kill();
    void uploadLog();
    void dump();
};
