/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LogWindow.h"
#include "Util/NetworkUtil.h"

QVector<LogWindow *> LogWindow::windows = QVector<LogWindow *>();

LogWindow::LogWindow(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags) {
    _ui.setupUi(this);

    connect(_ui.clear_log, &QPushButton::clicked, this, [this] { _ui.log->clear(); });
    connect(_ui.upload_log, &QPushButton::clicked, this, [this] {
        QString logUri = NetworkUtil::paste(_ui.log->toPlainText());
        QMessageBox msg(this);
        if (logUri.isNull()) {
            msg.setText(tr("Log upload failed!"));
            msg.exec();
            return;
        }
        QApplication::clipboard()->setText(logUri);
        msg.setText(tr("Link copied to clipboard!"));
        msg.exec();
    });

    connect(_ui.upload_log, SIGNAL(clicked(bool)), this, SIGNAL(uploadLog()));
    connect(_ui.instance_kill, SIGNAL(clicked(bool)), this, SIGNAL(kill()));
    windows << this;
}

void LogWindow::log(QString message) {
    _ui.log->append(message); }

QString LogWindow::text() const { return _ui.log->toPlainText(); }
