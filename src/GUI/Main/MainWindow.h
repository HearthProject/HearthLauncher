/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "ui_MainWindow.h"

#include <QFileSystemWatcher>
#include <QMainWindow>
#include <QMenu>
#include <QSystemTrayIcon>

class About;
class AuthWidget;
class Browse;
class Home;
class InstancesWidget;
class Settings;
class Config;
class Themer;

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(About &a, AuthWidget &aw, Browse &b, Home &h, Settings &s, Config &config, Themer &themer, QWidget *parent = nullptr,
               Qt::WindowFlags flags = nullptr);

    void setVisible(bool visible) override;

private:
    Ui_MainWindow _ui;
    Config &m_config;
    Themer &m_themer;

    QAction *minimizeAction = nullptr;
    QAction *maximizeAction = nullptr;
    QAction *restoreAction = nullptr;
    QAction *quitAction = nullptr;

    QSystemTrayIcon *trayIcon = nullptr;
    QMenu *trayIconMenu = nullptr;

    void createTrayIcon();
};
