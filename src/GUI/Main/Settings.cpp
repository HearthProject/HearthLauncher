#include "Settings.h"
#include "GUI/Dialogs/JavaDialog.h"

#include "Minecraft/JavaUtils.h"
#include "OneMeta/OneMeta.h"
#include "Util/HearthUpdater.h"
#include "Util/Utils.h"

#include <QtWidgets>

Settings::Settings(AsyncLoad &loader, Config &conf, Themer &themer, QWidget *parent) : Widget(parent), loader(loader), config(conf), theme_model(new ThemeModel(themer)) {
    _ui.setupUi(this);

    QSettings settings(config.config_file, QSettings::IniFormat);

    config.analyticsId = settings.value("analytics/uuid").toString();
    config.sendAnalytics = settings.value("analytics/enabled").toBool();

    _ui.java_path->setText(settings.value("java/path", "").toString());
    _ui.java_wrapper->setText(settings.value("java/wrapper", "").toString());
    _ui.java_arguments->setText(settings.value("java/args", "").toString());

    _ui.snapshots->setChecked(settings.value("snapshots", false).toBool());
    _ui.prerelease->setChecked(settings.value("prerelease", false).toBool());

    _ui.analytics_check->setChecked(config.sendAnalytics);
    //    _ui.auto_check_updates->setDisabled(true);

    if (!settings.value("analytics/uuid", false).toBool())
        settings.setValue("analytics/uuid", Utils::generateUUID());
    if (!settings.value("paste/host", false).toBool())
        settings.setValue("paste/host", "https://paste.gnome.org");
    // RAM

    auto max = Utils::getInstalledMemory();
    _ui.max_ram->setMaximum(max);
    _ui.min_ram->setMaximum(max);

    _ui.min_ram->setValue(settings.value("java/min_ram", 512).toInt());
    _ui.max_ram->setValue(settings.value("java/max_ram", 4096).toInt());
    _ui.min_ram->setMaximum(_ui.max_ram->value());

    _ui.checkUpdates->setChecked(settings.value("checkupdates", false).toBool());
    connect(_ui.checkUpdates, &QCheckBox::toggled, this,
            [&](bool toggle) { Utils::updateSettings(config, "checkupdates", toggle); });

    auto ram_update = [&] {
        _ui.min_ram->setMaximum(_ui.max_ram->value());
        Utils::updateSettings(config, "java/min_ram", _ui.min_ram->value());
        Utils::updateSettings(config, "java/max_ram", _ui.max_ram->value());
    };
    connect(_ui.min_ram, &QSpinBox::editingFinished, this, ram_update);
    connect(_ui.max_ram, &QSpinBox::editingFinished, this, ram_update);

    // Java Verify
    connect(_ui.java_verify, &QPushButton::clicked, this, [this] {
        QSettings settings(config.config_file, QSettings::IniFormat);
        JavaUtils::verifyJava(settings.value("java/path", "").toString(), true, this);
    });
    // Java Detection
    connect(_ui.java_detect, &QPushButton::clicked, this, [this] {
        QString path = JavaUtils::selectJava(config, this);
        _ui.java_path->setText(path);
    });
    connect(_ui.java_select, &QToolButton::clicked, this, [&]{
        QString path = QFileDialog::getOpenFileName(this, tr("Find Java Executable"));
        _ui.java_path->setText(path);
        JavaUtils::setJavaExecutable(config, path);
    });

    connect(_ui.java_path, &QLineEdit::editingFinished, this,
            [&] { Utils::updateSettings(config, "java/path", _ui.java_path->text()); });
    connect(_ui.java_wrapper, &QLineEdit::editingFinished, this,
            [&] { Utils::updateSettings(config, "java/wrapper", _ui.java_wrapper->text()); });
    connect(_ui.java_arguments, &QLineEdit::editingFinished, this,
            [&] { Utils::updateSettings(config, "java/args", _ui.java_arguments->text()); });
    connect(_ui.snapshots, &QCheckBox::stateChanged, this,
            [&] { Utils::updateSettings(config, "snapshots", _ui.snapshots->isChecked()); });
    connect(_ui.prerelease, &QCheckBox::stateChanged, this,
            [&] { Utils::updateSettings(config, "prerelease", _ui.prerelease->isChecked()); });

    _ui.themeComboBox->setModel(theme_model);
    _ui.themeComboBox->setCurrentIndex(theme_model->selected());
    connect(_ui.themeComboBox, SIGNAL(currentIndexChanged(int)), theme_model, SLOT(setTheme(int)));

    _ui.tray_icon->setChecked(settings.value("tray_icon", true).toBool());
    connect(_ui.tray_icon, &QCheckBox::stateChanged, this,
            [&] { Utils::updateSettings(config, "tray_icon", _ui.tray_icon->isChecked()); });

}

