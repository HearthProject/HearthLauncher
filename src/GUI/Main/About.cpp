#include "About.h"

#include "Hearth/HearthAuthSession.h"

About::About(QWidget *parent) : Widget(parent) {
    _ui.setupUi(this);

    if (JOB_ID >= 0)
        _ui.build_info->setText("Hearth Launcher " + VERSION_STRING + " built by " + BUILDER_NAME + " on " +
                                BUILDER_OS_NAME + " (Job ID " + QString::number(JOB_ID) + ")");
    else
        _ui.build_info->setText("Hearth Launcher " + VERSION_STRING + " built by " + BUILDER_NAME + " on " +
                                BUILDER_OS_NAME);
}
