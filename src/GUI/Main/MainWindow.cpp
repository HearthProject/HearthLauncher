/* * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MainWindow.h"

#include "About.h"
#include "AuthWidget.h"
#include "Browse.h"
#include "Settings.h"
#include "GUI/Main/Home.h"
#include "Util/Themer.h"

MainWindow::MainWindow(About &about, AuthWidget &auth, Browse &browse, Home &home, Settings &settings, Config &conf,
                       Themer &themer, QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags), m_config(conf), m_themer(themer) {
    ScopedTask _("Setup Main Window");

    ScopedTask st_ui("Setup UI");
    _ui.setupUi(this);
    st_ui.end();

    setWindowTitle("Hearth Launcher " + VERSION_STRING);
    setStyleSheet(m_themer.selected().load());
    connect(&m_themer, &Themer::changeTheme, this, [&]{
        setStyleSheet(m_themer.selected().load());
    });

    if (m_config.settings()->value("tray_icon", true).toBool()) {
        createTrayIcon();
    }

    _ui.tab_home->layout()->addWidget(&home);
    _ui.tab_settings->layout()->addWidget(&settings);
    _ui.tab_about->layout()->addWidget(&about);
    _ui.tab_browse->layout()->addWidget(&browse);
    _ui.tabs->buttonLayout->addWidget(&auth);
}

void MainWindow::setVisible(bool visible) {

    QMainWindow::setVisible(visible);
    if (trayIcon) {
        minimizeAction->setEnabled(visible);
        maximizeAction->setEnabled(!isMaximized());
        restoreAction->setEnabled(isMaximized() || !visible);
    }
}

void MainWindow::createTrayIcon() {

    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon(":/icons/hearth.png"));

    trayIcon->show();
}
