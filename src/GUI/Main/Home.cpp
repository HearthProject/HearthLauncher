#include "Home.h"
#include "GUI/Dialogs/InstanceDialog.h"
#include "GUI/Instance/InstanceWidget.h"
#include "GUI/ProjectWidget.h"
#include "Hearth/HearthAPI.h"
#include "Util/NetworkUtil.h"
#include "Minecraft/Yggdrasil.h"

QVector<MinecraftInstance *> Home::instances = QVector<MinecraftInstance *>();

Home::Home(AuthSession &auth, AsyncLoad &load, Config &conf, HearthAnalytics &ha, QWidget *parent)
    : Widget(parent), auth(auth), config(conf), loader(load), tracker(ha) {
    _ui.setupUi(this);
    QObject::connect(this, SIGNAL(installed()), this, SLOT(scanInstances()));
    connect(_ui.featured_next, &QToolButton::clicked, _ui.featured_widget, [&] { changePage(true); });
    connect(_ui.featured_previous, &QToolButton::clicked, _ui.featured_widget, [&] { changePage(false); });

    QJsonObject reply = NetworkUtil::get(QUrl("https://fdn.redstone.tech/theoneclient/hl3/lb.json")).json.object();
    news.link = reply["link"].toString();
    news.title = reply["title"].toString();
    _ui.news_title->setText(news.title);

    connect(_ui.news_browser, &QToolButton::clicked, this, [&] { QDesktopServices::openUrl(news.link); });

    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QLabel *label = new QLabel(tr("Loading Featured Packs"));
    label->setSizePolicy(sizePolicy);
    _ui.featured_widget->addWidget(label);

    connect(&loader, &AsyncLoad::loaded, this, [&, label] {
        _ui.featured_widget->removeWidget(label);
        for (auto featured : loadFeatured()) {
            QWidget *widget = new QWidget;
            QVBoxLayout *layout = new QVBoxLayout;
            widget->setSizePolicy(sizePolicy);
            widget->setLayout(layout);
            PackWidget *pack = new PackWidget(featured.project, config, tracker);
            pack->_ui.label_desc->setText("Featured:" + featured.note + "\n" + pack->_ui.label_desc->text());
            layout->addWidget(pack);
            connect(pack, SIGNAL(installed()), this, SIGNAL(installed()));
            _ui.featured_widget->addWidget(widget);
        }
        if (_ui.featured_widget->count() == 0) {
            label->setText(tr("No Featured Packs Found"));
            _ui.featured_widget->addWidget(label);
        }
    });

    connect(_ui.instance_add, &QToolButton::clicked, this, [&] {
        InstanceDialog dia(config, tracker, auth, this);
        connect(&dia, SIGNAL(installed()), this, SIGNAL(signalChange()));
        if (dia.exec()) {
            scanInstances();
        }
    });
    connect(_ui.instance_refresh, SIGNAL(clicked(bool)), this, SLOT(scanInstances()));
    connect(this, SIGNAL(signalChange()), this, SLOT(updateInstances()));

    checkStatus();
}

void Home::checkStatus() {
    Utils::clearLayout(_ui.status);
    _ui.status->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    auto status = Yggdrasil::checkStatus();

    auto makeIcon = [](QColor color){
        QPixmap pixmap(10,10);
        pixmap.fill(color);
        return QIcon(pixmap);
    };

    for(Yggdrasil::Service s: status) {
        QToolButton *service = new QToolButton(this);
        connect(service, &QToolButton::clicked, this, [&]{ QDesktopServices::openUrl(QUrl("https://help.mojang.com")); });
        service->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        service->setIcon(makeIcon(s.m_color));
        service->setText(s.m_name);
        service->setToolTip(s.m_url);
        _ui.status->addWidget(service);
    }
}

QList<Home::Featured> Home::loadFeatured() {
    QList<Featured> featured;
    auto reply = NetworkUtil::get(QUrl(HearthAPI::BASE_URL + "/featured"));
    QJsonArray a = reply.json.array();
    for (QJsonValue v : a) {
        QJsonObject o = v.toObject();
        OneMeta::SourceType type = static_cast<OneMeta::SourceType>(o["sourceType"].toInt());
        int id = o["id"].toInt();
        int timestamp = o["timestamp"].toInt();
        QString note = o["notes"].toString();
        OneMeta::ProjectPtr p = OneMeta::database.findProject(type, id);
        if (p) {
            Featured f;
            f.note = note;
            f.project = p;
            f.time = timestamp;
            featured << f;
        }
    }

    std::sort(featured.begin(), featured.end());

    if (featured.isEmpty()) {
        for (OneMeta::ProjectPtr p :
            OneMeta::search("", OneMeta::POPULARITY, {OneMeta::CURSE}, 5, {make_filter(&OneMeta::Project::type, OneMeta::MODPACK)})) {
            Featured f;
            f.project = p;
            featured.append(f);
        }
    }

    return featured;
}

void Home::changePage(bool next) {
    int current = _ui.featured_widget->currentIndex();
    int i = (current + (next ? 1 : -1));
    i = i >= _ui.featured_widget->count() ? 0 : i < 0 ? (_ui.featured_widget->count() - 1) : i;
    _ui.featured_widget->setCurrentIndex(i);
}

void Home::updateInstances() {
    _ui.instances->clear();

    for (MinecraftInstance *instance : Home::instances) {
        InstanceWidget *widget = new InstanceWidget(instance, auth, loader, config, tracker, this);
        connect(widget, SIGNAL(instanceLaunching()), this, SLOT(scanInstances()));
        connect(instance, SIGNAL(instanceRemoved()), this, SLOT(scanInstances()));
        QListWidgetItem *item = new QListWidgetItem;
        item->setSizeHint(widget->size());
        _ui.instances->addItem(item);
        _ui.instances->setItemWidget(item, widget);
    }
}

void Home::scanInstances() {
    ScopedTask _("Scan Instances");
    QStringList instanceFolders = QDir(config.instance_dir).entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    Home::instances.clear();
    for (QString i : instanceFolders) {
        auto *instance = new MinecraftInstance(i, config, tracker);
        connect(instance, &MinecraftInstance::instanceRemoved, this, &Home::scanInstances);
        instances.append(instance);
    }

    std::sort(instances.begin(), instances.end(),
              [](MinecraftInstance *a, MinecraftInstance *b) { return a->lastLauched() > b->lastLauched(); });
    emit signalChange();
}
