#pragma once

#include "ui_About.h"
#include "GUI/Widget.h"
#include <QWidget>

class About : public Widget {
    Q_OBJECT
public:
    explicit About(QWidget *parent = nullptr);
    Ui_About _ui;
};
