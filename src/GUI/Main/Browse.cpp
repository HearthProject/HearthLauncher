#include "Browse.h"
#include <Minecraft/MinecraftVersions.h>

#include "Util/Instrumentation.h"


Browse::Browse(OneMeta::ProjectType type, AsyncLoad &loader, WidgetHandler *handler, QWidget *parent, QString title, MinecraftInstance *instance)
    : Widget(parent), type(type), loader(loader), m_instance(instance) {
    _ui.setupUi(this);
    _ui.list->setAlternatingRowColors(true);
    _ui.title->setText(title);
    this->widgetHandler = handler;

    connect(_ui.search_refresh, SIGNAL(clicked(bool)), this, SLOT(reloadDatabase()));
    connect(_ui.search_submit, &QPushButton::clicked, this, [&] { updateSearch(); });
    connect(_ui.search_text, &QLineEdit::returnPressed, this, [&] { updateSearch(); });
    connect(_ui.list->verticalScrollBar(), &QScrollBar::valueChanged, this, [&](int position) {
        auto bar = _ui.list->verticalScrollBar();
        if (position >= bar->maximum()) {
            loadPage(20);
        }
    });
}

void Browse::reloadDatabase() {
    _ui.search_refresh->setDisabled(true);
    invoke_slot(&loader, &AsyncLoad::reloadDatabase);
}

void Browse::updateSearch() {
    ScopedTask _("Update Search");
    QString search = _ui.search_text->text();
    QRegularExpression regexVersion("(version:(?<version>[\\d\\.]+))");
    QRegularExpression regexSorting("(sort:(?<sort>[^\\s]+))");
    QRegularExpression regexAuthor("(author:(?<author>[^\\s]+))");
    QRegularExpressionMatch matchVersion = regexVersion.match(search);
    QRegularExpressionMatch matchSorting = regexSorting.match(search);
    QRegularExpressionMatch matchAuthor = regexAuthor.match(search);

    int sorting = OneMeta::POPULARITY;
    QList<std::function<bool(OneMeta::ProjectPtr)>> filters;

    if(matchAuthor.hasMatch()) {
        search.replace(matchAuthor.captured(0),"");
        QString author = matchAuthor.captured("author");
        filters << [author](OneMeta::ProjectPtr p) { return p->isAuthor(author); };
    }

    if(matchVersion.hasMatch()) {
        search.replace(matchVersion.captured(0),"");
        QString version = matchVersion.captured("version");
        filters << [version](OneMeta::ProjectPtr p) { return p->minecraft.contains(version); };
    } else if(m_instance) {
        QString version = m_instance->minecraft();
        filters << [version](OneMeta::ProjectPtr p) { return p->minecraft.contains(version); };
    }

    if(matchSorting.hasMatch()) {
        search.replace(matchSorting.captured(0),"");

        QString sort = matchSorting.captured("sort");
        bool reversed = false;
        if(sort.startsWith("!")) {
            sort.replace("!","");
            reversed = true;
        }

        for(int i = 0; i < OneMeta::NUM_SORTING;i++) {
            if(OneMeta::sorts[i].toLower() == sort.toLower())
                sorting = (reversed ? -1 : 1) * i;
        }
    }

    filters << make_filter(&OneMeta::Project::type, type);

    for (auto c : connections)
        QObject::disconnect(c);
    connections.clear();
    _ui.list->clear();

    projects = OneMeta::search(search, sorting, {OneMeta::CURSE}, -1, filters);
    currentPage = 0;
    loadPage(20);
}

void Browse::loadPage(int size) {
    auto page = projects.mid(currentPage, size);
    currentPage += size;
    populateBrowse(page);
}

void Browse::populateBrowse(QVector<OneMeta::ProjectPtr> projects) {
    for (auto &project : projects) {
        ProjectWidget *widget = widgetHandler->createWidget(project);
        connections << connect(widget, &ProjectWidget::installed, this, &Browse::installed);
        QListWidgetItem *item = new QListWidgetItem;
        item->setSizeHint(widget->sizeHint());
        _ui.list->addItem(item);
        _ui.list->setItemWidget(item, widget);
    }
}
