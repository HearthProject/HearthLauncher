#pragma once

#include "ui_Browse.h"
#include "GUI/ProjectWidget.h"
#include "GUI/Widget.h"
#include "Util/AsyncLoader.h"
#include "Minecraft/MinecraftInstance.h"
#include <QWidget>

class WidgetHandler {
public:
    explicit WidgetHandler(Config &conf, HearthAnalytics &ha) : config(conf), tracker(ha) {}

    virtual ProjectWidget *createWidget(OneMeta::ProjectPtr project) = 0;

    Config &config;
    HearthAnalytics &tracker;
};

class PackWidgetHandler : public WidgetHandler {
public:
    explicit PackWidgetHandler(Config &conf, HearthAnalytics &ha) : WidgetHandler(conf, ha) {}

    ProjectWidget *createWidget(OneMeta::ProjectPtr project) { return new PackWidget(project, config, tracker); }
};

class ModWidgetHandler : public WidgetHandler {
public:
    explicit ModWidgetHandler(Config &conf, HearthAnalytics &ha, MinecraftInstance *instance)
        : WidgetHandler(conf, ha), m_instance(instance) {}

    ProjectWidget *createWidget(OneMeta::ProjectPtr project) {
        return new ModInstallWidget(project, m_instance, config, tracker);
    }

private:
    MinecraftInstance *m_instance;
};

class Browse : public Widget {
    Q_OBJECT
public:
    explicit Browse(OneMeta::ProjectType type, AsyncLoad &loader, WidgetHandler *handler, QWidget *parent = nullptr,
                    QString title = QString(),MinecraftInstance *instance = nullptr);

    Ui_Browse _ui;

    void populateBrowse(QVector<OneMeta::ProjectPtr> projects);
    void loadPage(int size);

public slots:

    void reloadDatabase();

    void updateSearch();

signals:

    void currentSearch(QString search);

    void installed();

private:
    MinecraftInstance *m_instance;
    OneMeta::ProjectType type;
    WidgetHandler *widgetHandler;
    AsyncLoad &loader;
    QList<QMetaObject::Connection> connections;

    QVector<OneMeta::ProjectPtr> projects;
    int currentPage = 0;
};
