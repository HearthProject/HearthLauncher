#pragma once

#include "ui_Home.h"
#include "Analytics/HearthAnalytics.h"
#include "GUI/Widget.h"
#include "Minecraft/AuthSession.h"
#include "Minecraft/MinecraftInstance.h"
#include "Util/AsyncLoader.h"
#include "Util/Config.h"
#include "Util/Instrumentation.h"
#include <QWidget>

class Home : public Widget {
    Q_OBJECT
public:
    explicit Home(AuthSession &auth, AsyncLoad &loader, Config &config, HearthAnalytics &ha, QWidget *parent = nullptr);
    Ui_Home _ui;

    AuthSession &auth;
    Config &config;
    AsyncLoad &loader;
    HearthAnalytics &tracker;

    struct News {
        QString link;
        QString title;
    };

    News news;

    struct Featured {
        int time;
        QString note;
        OneMeta::ProjectPtr project;

        bool operator<(const Featured &f) const { return time < f.time; }
    };

    void changePage(bool next);
    QList<Featured> loadFeatured();
    static QVector<MinecraftInstance *> instances;
public slots:
    void scanInstances();
    void updateInstances();
    void checkStatus();

signals:
    void signalChange();
    void installed();
};
