#include "AuthWidget.h"

#include "GUI/Dialogs/AuthDialog.h"
#include "Hearth/HearthAuthSession.h"
#include "Minecraft/AuthSession.h"
#include "Util/DownloadUtil.h"
#include <QBitmap>

AuthWidget::AuthWidget(AuthSession &auth, Config &conf, QWidget *parent)
    : Widget(parent), authSession(auth), config(conf) {
    _ui.setupUi(this);

    connect(&authSession, SIGNAL(signalAuthenticate(QString, QString, QString)), this,
            SLOT(changeAuth(QString, QString, QString)));
    authSession.load(Utils::combinePath(config.data_dir, "auth.dat"));

    //    if (Utils::fileExists(config.data_dir + "/hauth.dat")) {
    //        HearthAuthSession::instance()->loadFromFile(config.data_dir + "/hauth.dat");
    //        _ui.role->setText(HearthAuthSession::instance()->roleName);
    //    }
    connect(_ui.authentication, &QPushButton::clicked, this, &AuthWidget::dialog);
}

void AuthWidget::changeAuth(QString state, QString skin, QString role) {
    _ui.username->setText(authSession.user().name);
    _ui.authentication->setText(state);
    _ui.skin->setPixmap(QPixmap(skin));
}

void AuthWidget::dialog() {
    if (authSession.valid()) {
        authSession.logout();
    } else {
        AuthDialog d(authSession, config, this);
        d.exec();
    }
}
