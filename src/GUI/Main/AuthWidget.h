#pragma once

#include "ui_AuthWidget.h"
#include "GUI/Widget.h"
#include <QWidget>

class AuthSession;
class Config;

class AuthWidget : public Widget {
    Q_OBJECT
public:
    explicit AuthWidget(AuthSession &authSession, Config &config, QWidget *parent = nullptr);
    Ui_AuthWidget _ui;

public slots:
    void changeAuth(QString state, QString skin, QString role);
    void dialog();

private:
    AuthSession &authSession;
    Config &config;
};
