#pragma once

#include "ui_Settings.h"
#include "GUI/Widget.h"
#include "Util/AsyncLoader.h"
#include "Util/Config.h"
#include <QWidget>

#include <QFileSystemWatcher>
#include <QProcess>
#include <QStringListModel>
#include "Util/Themer.h"
#include "Util/ThemeModel.h"

class Settings : public Widget {
    Q_OBJECT
public:
    explicit Settings(AsyncLoad &loader, Config &config, Themer &themer, QWidget *parent = nullptr);
    Ui_Settings _ui;
    Config &config;
    AsyncLoad &loader;
    ThemeModel *theme_model;

};
