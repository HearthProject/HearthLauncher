/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "ProjectWidget.h"
#include <Minecraft/BasePack.h>
#include <Minecraft/MetaMod.h>
#include <QWidget>

#include "GUI/Dialogs/FileDialog.h"
#include "Util/DownloadUtil.h"

ProjectWidget::~ProjectWidget() {
    dl.quit();
    if (!dl.wait(3000)) {
        dl.terminate();
        dl.wait();
    }
}

ProjectWidget::ProjectWidget(OneMeta::ProjectPtr project, Config &config, HearthAnalytics &ha, QWidget *parent)
    : Widget(parent), m_project(project), config(config), tracker(ha) {

    if (!m_project)
        return;
    _ui.setupUi(this);
    _ui.name->setText(m_project->title.trimmed());

    getIcon();

    _ui.label_desc->setText(m_project->description);
    QString dltext = "Downloads: " + Utils::formatNumber(m_project->stats.value("downloads", 0).toDouble(), 3);
#ifdef HL_DEBUG
    dltext += " Popularity: " + QString::number(m_project->stats.value("popularity", 0).toDouble()) +
              " Project ID: " + QString::number(m_project->id);
#endif

    _ui.label_downloads->setText(dltext);
    _ui.label_authors->setText("By " + m_project->getAuthors().join(","));
    if (!project->minecraft.isEmpty())
        _ui.label_versions->setText("Minecraft " + m_project->minecraft.join(","));

    QDateTime created = QDateTime::fromMSecsSinceEpoch(m_project->getCreationDate()), updated = QDateTime::fromMSecsSinceEpoch(m_project->getLatestDate());
    _ui.label_created->setText(QString("Created: %1").arg(Utils::timeDifference(created)));
    _ui.label_updated->setText(QString("Updated: %1").arg(Utils::timeDifference(updated)));
    connect(_ui.button_browse, &QPushButton::clicked, this, &ProjectWidget::moreClicked);
    connect(_ui.button_install, &QToolButton::clicked, this, &ProjectWidget::installClicked);
    connect(_ui.button_more, &QToolButton::clicked, this, [&] {
        int i = _ui.pages->currentIndex();
        _ui.pages->setCurrentIndex(i >= 1 ? 0 : i + 1);
    });

    connect(this, SIGNAL(signalFile(QString)), _ui.selected_file, SLOT(setText(QString)));
}

void ProjectWidget::installClicked() { install(); }

void ProjectWidget::setIcon(QString icon) {
    Utils::scaleImage(icon);
    _ui.icon->setPixmap(QPixmap(icon));
}

QString ProjectWidget::getIcon() {
    if (m_project) {
        QString icon = Utils::combinePath(config.icon_dir, QString::number(m_project->id) + "." +
                                                               QFileInfo(m_project->icon.url).suffix());
        if (!Utils::fileExists(icon)) {
            setIcon(":/icons/hearth.png");
            if (!m_project->icon.url.isEmpty()) {
                dl.setTarget(m_project->icon.url, icon);
                connect(&dl, SIGNAL(downloadDone(QString)), this, SLOT(setIcon(QString)));
                dl.start();
            }
        } else {
            setIcon(icon);
        }
    }
    return QString();
}

void ProjectWidget::moreClicked() { QDesktopServices::openUrl(OneMeta::getUrl(m_project)); }

void PackWidget::selectFile(bool latest, QString version) {
    pack = FileDialog::getFilePack(config, tracker, m_project, latest, 0, version, this);
    signalFile(pack ? pack->m_file->displayname : "");
}

PackWidget::PackWidget(OneMeta::ProjectPtr project, Config &conf, HearthAnalytics &ha, QWidget *parent)
    : ProjectWidget(project, conf, ha, parent) {
    connect(_ui.button_file, &QToolButton::clicked, this, [&] { selectFile(false, ""); });
    selectFile();
}

void PackWidget::install() {
    if (pack) {
        QProgressDialog dia(this);
        dia.setWindowTitle(tr("Installing ") + m_project->title);
        connect(pack, SIGNAL(signalMessage(QString)), &dia, SLOT(setLabelText(QString)));
        connect(pack, SIGNAL(signalProgress(int)), &dia, SLOT(setValue(int)));
        connect(pack, SIGNAL(signalMax(int)), &dia, SLOT(setMaximum(int)));
        connect(pack, &BasePack::installed, this, &ProjectWidget::installed);
        pack->download();
        pack->install();
    }
}

ModInstallWidget::ModInstallWidget(OneMeta::ProjectPtr project, MinecraftInstance *instance, Config &config,
                                   HearthAnalytics &ha, QWidget *parent)
    : ProjectWidget(project, config, ha, parent) {
    m_instance = instance;
    _ui.label_versions->setText("Minecraft " + m_instance->minecraft());

    connect(_ui.button_file, &QToolButton::clicked, this, [&] { selectFile(false, m_instance->minecraft()); });

    selectFile(true, m_instance->minecraft());
}

void ModInstallWidget::install() {
    if (mod) {
        connect(mod, &MetaMod::installed, this, &ProjectWidget::installed);
        mod->setResolveDependencies(true);
        mod->install();
    }
}

void ModInstallWidget::selectFile(bool latest, QString version) {
    mod = FileDialog::getFileMod(m_instance, tracker, m_project, latest, 0, version, this);
    signalFile(mod ? mod->m_file->displayname : "");
}

void ProjectWidget::paintEvent(QPaintEvent *e) {
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
