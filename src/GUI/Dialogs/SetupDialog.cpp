/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SetupDialog.h"

#include "Analytics/HearthAnalytics.h"
#include "Minecraft/AuthSession.h"
#include "Util/DownloadUtil.h"
#include "Util/Utils.h"

#include "GUI/Dialogs/JavaDialog.h"
#include "Minecraft/JavaUtils.h"

SetupDialog::SetupDialog(Config &conf, HearthAnalytics &ha, QWidget *parent, Qt::WindowFlags flags)
    : Dialog(parent, flags), config(conf), tracker(ha) {
    _ui.setupUi(this);

    QString doc = _ui.textBrowser->document()->toHtml();
    doc.replace("{VER}", VERSION_STRING);
    doc.replace("{BUILDER}", BUILDER_NAME);
    _ui.textBrowser->document()->setHtml(doc);

    _ui.tabWidget->tabBar()->setVisible(false);

    QObject::connect(_ui.pushButton, &QPushButton::clicked, this, &SetupDialog::nextTab);
    QObject::connect(_ui.pushButton_2, &QPushButton::clicked, this, &SetupDialog::nextTab);

    QSettings settings(config.config_file, QSettings::IniFormat);

    auto java = JavaUtils::getDefaultInstall().path;

    if (JavaUtils::verifyJava(java, false))
        settings.setValue("java/path", java);

    _ui.java_path->setText(settings.value("java/path", "").toString());
    connect(_ui.java_path, &QLineEdit::editingFinished, this, [&] {
        QSettings s(config.config_file, QSettings::IniFormat);
        s.setValue("java/path", _ui.java_path->text());
    });

    connect(_ui.java_detect, &QPushButton::clicked, this, [&] {
        QString dir = JavaUtils::selectJava(config, this);
        this->_ui.java_path->setText(dir);
    });

    connect(_ui.java_verify, &QPushButton::clicked, this, [this] {
        QSettings s(config.config_file, QSettings::IniFormat);
        JavaUtils::verifyJava(s.value("java/path", "").toString(), true, this);
    });
}

void SetupDialog::nextTab() {
    int i = _ui.tabWidget->currentIndex();
    if (i == (_ui.tabWidget->count()) - 1) {
        _ui.tabWidget->setTabEnabled(0, false);
        _ui.tabWidget->setTabEnabled(1, false);
        QSettings settings(config.config_file, QSettings::IniFormat);
        settings.setValue("setup_done", true);
        settings.setValue("analytics/enabled", _ui.analytics->isChecked());
        config.sendAnalytics = _ui.analytics->isChecked();
        tracker.sendEvent(HearthEventType::INSTALLED);
        settings.setValue("autoupdate", _ui.autoupdate->isChecked());
        settings.setValue("java/min_ram", 512);
        settings.setValue("java/max_ram", 4096);
        settings.setValue("tray_icon", true);
        done(1);

        return;
    }
    _ui.tabWidget->setTabEnabled(i + 1, true);
    _ui.tabWidget->setCurrentIndex(i + 1);
}
