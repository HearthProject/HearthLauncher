/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ui_FileDialog.h"
#include "Analytics/HearthAnalytics.h"
#include "OneFormat/OneFormat.h"
#include "OneMeta/OneMeta.h"
#include "Util/Config.h"
#include <QAbstractTableModel>
#include <QDialog>
#include <QMap>
#include <QPair>
#include <QVector>
#include "Dialog.h"

class MetaMod;

class BasePack;

class MetaFileModel : public QAbstractTableModel {
    Q_OBJECT
public:
    MetaFileModel(QObject *parent) : QAbstractTableModel(parent) {}

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    QVector<OneMeta::FilePtr> files() const;

    void setFiles(QVector<OneMeta::FilePtr> files);

    OneMeta::FilePtr at(int row) const;

    bool isEmpty() const;

private:
    QVector<OneMeta::FilePtr> m_files;

    enum Columns { NameColumn, StageColumn, NUM_COLUMNS };
};

enum Result { Success, Pass, Fail };

class FileDialog : public Dialog {
    Q_OBJECT
public:
    explicit FileDialog(HearthAnalytics &ha, OneMeta::ProjectPtr p, int currentFileDate = 0, QString version = "",
                        QWidget *parent = nullptr, Qt::WindowFlags flags = 0)
        : Dialog(parent, flags), p(p), currentFileDate(currentFileDate), tracker(ha) {
        _ui.setupUi(this);
        _ui.minecraft_version->addItems(Utils::sortVersions(p->minecraft));
        if (!version.isEmpty())
            _ui.minecraft_version->setCurrentText(version);
        model = new MetaFileModel(this);
        _ui.files->setModel(model);

        connect(_ui.minecraft_version, &QComboBox::currentTextChanged, this, &FileDialog::searchChanged);
        connect(_ui.cancel, &QPushButton::clicked, this, [&] { reject(); });
        connect(_ui.select, &QPushButton::clicked, this, [&] {
            if (_ui.files->currentIndex().isValid()) {
                OneMeta::FilePtr file = model->at(_ui.files->currentIndex().row());
                setFile(file);
            }
        });

        _ui.files->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

        searchChanged();
    }

    Ui_FileDialog _ui;

    void populateFiles(QVector<OneMeta::FilePtr> files);

    void searchChanged();

    OneMeta::FilePtr selectedFile() { return this->file; }

    bool isEmpty() const { return model->isEmpty(); }

    static OneMeta::FilePtr getSelectedFile(HearthAnalytics &tracker, OneMeta::ProjectPtr project, bool latest = false,
                                            int currentFileDate = 0, QString version = "", QWidget *parent = 0);

    static BasePack *getFilePack(Config &config, HearthAnalytics &tracker, OneMeta::ProjectPtr project,
                                 bool latest = false, int currentFileDate = 0, QString version = "",
                                 QWidget *parent = 0);

    static MetaMod *getFileMod(MinecraftInstance *instance, HearthAnalytics &tracker, OneMeta::ProjectPtr project,
                               bool latest = false, int currentFileDate = 0, QString version = "", QWidget *parent = 0);

public slots:

    void setFile(OneMeta::FilePtr file);

private:
    HearthAnalytics &tracker;
    OneMeta::ProjectPtr p;
    OneMeta::FilePtr file;
    int currentFileDate;
    MetaFileModel *model;
};
