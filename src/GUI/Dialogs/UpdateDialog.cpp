/*
 * Copyright (C) 2017-2018 joonatoona
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "UpdateDialog.h"

UpdateDialog::UpdateDialog(const Updater::HearthVersion &ver, QWidget *parent, Qt::WindowFlags flags)
    : Dialog(parent, flags) {
    _ui.setupUi(this);
    this->_ui.changelog->setHtml(ver.changelog);
    connect(_ui.yes_button, &QPushButton::clicked, this, [this] { this->done(1); });
    connect(_ui.no_button, &QPushButton::clicked, this, [this] { this->done(0); });
}
