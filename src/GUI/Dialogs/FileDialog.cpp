/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "FileDialog.h"
#include "Minecraft/BasePack.h"
#include "Minecraft/MetaMod.h"
#include "Minecraft/MinecraftVersions.h"
#include "Util/DownloadUtil.h"
#include <QtWidgets>

void FileDialog::searchChanged() {
    populateFiles(std::move(p->filelist(_ui.minecraft_version->currentText(), currentFileDate)));
}

void FileDialog::populateFiles(QVector<OneMeta::FilePtr> files) { model->setFiles(files); }

void FileDialog::setFile(OneMeta::FilePtr file) {
    this->file = file;
    done(file ? 1 : 0);
}

OneMeta::FilePtr FileDialog::getSelectedFile(HearthAnalytics &tracker, OneMeta::ProjectPtr project, bool latest,
                                             int currentFileDate, QString version, QWidget *parent) {
    if (latest)
        return project->latest(version);
    FileDialog *fileDialog = new FileDialog(tracker, project, currentFileDate, version, parent);
    if (fileDialog->isEmpty()) {
        fileDialog->reject();
    } else if (fileDialog->exec()) {
        return fileDialog->selectedFile();
    }
    return 0;
}

MetaMod *FileDialog::getFileMod(MinecraftInstance *instance, HearthAnalytics &tracker, OneMeta::ProjectPtr project,
                                bool latest, int currentFileDate, QString version, QWidget *parent) {
    auto selectedFile = FileDialog::getSelectedFile(tracker, project, latest, currentFileDate, version, parent);
    if (selectedFile) {
        MetaMod *mod = new MetaMod(instance, selectedFile);
        return mod;
    }
    return 0;
}

BasePack *FileDialog::getFilePack(Config &config, HearthAnalytics &tracker, OneMeta::ProjectPtr project, bool latest,
                                  int current, QString version, QWidget *parent) {
    auto selectedFile = FileDialog::getSelectedFile(tracker, project, latest, current, version, parent);
    if (selectedFile) {
        BasePack *pack = new BasePack(config, tracker, selectedFile, parent);
        return pack;
    }
    return 0;
}

int MetaFileModel::rowCount(const QModelIndex &parent) const { return m_files.size(); }

int MetaFileModel::columnCount(const QModelIndex &parent) const { return NUM_COLUMNS; }

QVariant MetaFileModel::data(const QModelIndex &index, int role) const {
    auto file = at(index.row());
    switch (role) {
    case Qt::DisplayRole:
        switch (index.column()) {
        case NameColumn:
            return file->displayname;
        case StageColumn:
            return file->stage();
        }
    }
    return QVariant();
}

QVector<OneMeta::FilePtr> MetaFileModel::files() const { return m_files; }

void MetaFileModel::setFiles(QVector<OneMeta::FilePtr> files) {
    beginResetModel();
    this->m_files = files;
    endResetModel();
}

OneMeta::FilePtr MetaFileModel::at(int row) const { return m_files.at(row); }

bool MetaFileModel::isEmpty() const { return files().isEmpty(); }

QVariant MetaFileModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation != Qt::Horizontal)
        return QVariant();
    switch (role) {
    case Qt::DisplayRole:
        switch (section) {
        case NameColumn:
            return tr("File");
        case StageColumn:
            return tr("Stage");
        default:
            return QVariant();
        }

    case Qt::ToolTipRole:
        switch (section) {
        case NameColumn:
            return tr("The name of the file.");
        case StageColumn:
            return tr("The release stage of the file.");
        default:
            return QVariant();
        }
    default:
        return QVariant();
    }
    return QVariant();
}
