/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "CheckTreeDialog.h"
#include "GUI/Model/CFileSystemModel.h"
#include "Util/Utils.h"

CheckTreeDialog::CheckTreeDialog(QString dir, QWidget *parent, Qt::WindowFlags flags) : QDialog(parent, flags) {
    _ui.setupUi(this);

    CFileSystemModel *model = new CFileSystemModel();
    model->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs);
    model->setRootPath(dir);
    _ui.tree->setModel(model);
    _ui.tree->setRootIndex(model->index(dir));
    _ui.tree->hideColumn(1);
    _ui.tree->hideColumn(2);
    _ui.tree->hideColumn(3);
}
