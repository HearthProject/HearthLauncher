/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "JavaDialog.h"
#include "Minecraft/JavaUtils.h"
#include "Util/Utils.h"

QString JavaDialog::getSelectPathed(QWidget *parent) {
    JavaDialog dialog(parent);
    if(dialog.exec())
       return dialog.path;
    return QString();
}

JavaDialog::JavaDialog(QWidget *parent) : Dialog(parent) {
    _ui.setupUi(this);
    QVector<JavaInstall> list;
    for (const JavaInstall &java : JavaUtils::getInstallLocations()) {
        if (Utils::fileExists(java.path) || java.path == JavaUtils::getDefaultInstall().path)
            list.append(java);
    }
    _ui.table->setColumnCount(1);
    _ui.table->setRowCount(list.size());
    _ui.table->setSelectionMode(QAbstractItemView::SingleSelection);
    _ui.table->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Path")));

    for (int i = 0; i < list.size(); i++)
        _ui.table->setItem(i, 0, new QTableWidgetItem(list[i].path));
    _ui.table->selectRow(0);
    _ui.table->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    connect(_ui.select, &QPushButton::clicked, this, &JavaDialog::selectJavaVersion);
    connect(_ui.cancel, &QPushButton::clicked, this, [this] { this->done(1); });
}

void JavaDialog::selectJavaVersion() {
    QList<QTableWidgetItem *> selected(_ui.table->selectedItems());
    if (!selected.isEmpty()) {
        path = selected.first()->text();
        done(1);
        return;
    }
    done(1);
}
