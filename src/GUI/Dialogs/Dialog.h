#pragma once

#include <QPainter>
#include <QStyleOption>
#include <QDialog>

class Dialog : public QDialog {
public:
    Dialog(QWidget *parent = nullptr, Qt::WindowFlags flags = 0) : QDialog(parent, flags) {}

public slots:
    void paintEvent(QPaintEvent *event) {
        QStyleOption opt;
        opt.init(this);
        QPainter p(this);
        style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    }
};
