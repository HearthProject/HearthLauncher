/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Hearth/HearthAPI.h"
#include "Hearth/HearthAuthSession.h"

#include "ui_AuthDialog.h"
#include "Minecraft/AuthSession.h"
#include "Minecraft/Yggdrasil.h"
#include "Util/Config.h"
#include <QDialog>
#include <QPair>
#include <QString>
#include "Dialog.h"

class AuthDialog : public Dialog {
    Q_OBJECT
public:
    explicit AuthDialog(AuthSession &authSession, Config &conf, QWidget *parent = nullptr);
    Ui_AuthDialog _ui;

public slots:
    void login();

private:
    QString authenticate(QString username, QString password);
    AuthSession &authSession;
    Config &config;
};
