/* 
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "InstanceDialog.h"
#include "GUI/Instance/CreateWidget.h"
#include "GUI/Instance/ImportWidget.h"
#include "Minecraft/MinecraftInstance.h"
#include "Minecraft/MinecraftVersions.h"
#include "Util/Utils.h"

InstanceDialog::InstanceDialog(Config &conf, HearthAnalytics &ha, AuthSession &auth, QWidget *parent)
    : Dialog(parent), createWidget(new CreateWidget(conf, ha, 0, this)),
      importWidget(new ImportWidget(conf, ha, this)), submit(new QPushButton(tr("Submit"))),
      cancel(new QPushButton(tr("Cancel"))), config(conf), tracker(ha) {
    _ui.setupUi(this);

    connect(importWidget, SIGNAL(installed()), this, SIGNAL(installed()));
    connect(_ui.tabWidget, SIGNAL(currentChanged(int)), this, SLOT(changeTab(int)));
    connect(_ui.cancel, &QPushButton::clicked, this, [&] { done(0); });

    changeTab(_ui.tabWidget->currentIndex());

    _ui.tab_create->layout()->addWidget(createWidget);
    _ui.tab_import->layout()->addWidget(importWidget);
}

void InstanceDialog::changeTab(int i) {

    for (auto connect : connections)
        QObject::disconnect(connect);

    if (i == 0) {
        connect(_ui.submit, &QPushButton::clicked, this, [&] {
            if (createWidget->create(config, tracker)) {
                done(1);
            }
        });

    } else if (i == 1) {
        connect(_ui.submit, &QPushButton::clicked, this, [&] {
            if (importWidget->import()) {
                done(1);
            }
        });
    }
}
