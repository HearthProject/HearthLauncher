/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AuthDialog.h"


AuthDialog::AuthDialog(AuthSession &authSession, Config &conf, QWidget *parent)
    : Dialog(parent), authSession(authSession), config(conf) {
    _ui.setupUi(this);
    connect(_ui.login, &QPushButton::clicked, this, &AuthDialog::login);
    connect(_ui.cancel, &QPushButton::clicked, this, [this] { this->done(0); });
}

void AuthDialog::login() {
    QString user = _ui.username->text();
    QString pass = _ui.password->text();
    if (!user.isEmpty() && !pass.isEmpty()) {
        QString res = authenticate(user, pass);
        if (!res.isEmpty()) {
            authSession.login();
            done(1);
        }
    }
}

QString AuthDialog::authenticate(QString username, QString password) {
    qDebug() << "Auth:" << "Attempting to authenticate";
    authSession.read(Yggdrasil::login(username, password).object());

    if (!authSession.valid()) {
        qInfo() << "Auth: Invalid login!";
        return "";
    } else {
        qInfo() << "Auth: Valid login!";
    }
    authSession.write(Utils::combinePath(config.data_dir, "auth.dat"));
    return authSession.user().name;
}
