/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "ui_ProjectWidget.h"
#include "Minecraft/MinecraftInstance.h"
#include "OneFormat/OneFormat.h"
#include "OneMeta/OneMeta.h"
#include "Util/BackgroundDownloader.h"
#include <QWidget>
#include <QtWidgets>
#include <functional>

#include "Widget.h"
#include <QIcon>
#include <QToolButton>

class BasePack;
class MetaMod;
class Config;
class HearthAnalytics;

class ProjectWidget : public Widget {
    Q_OBJECT
public:
    ProjectWidget(OneMeta::ProjectPtr project, Config &conf, HearthAnalytics &ha, QWidget *parent = nullptr);
    ProjectWidget(Config &conf, HearthAnalytics &ha, QWidget *parent = nullptr)
        : Widget(parent), config(conf), tracker(ha) {}
    ~ProjectWidget();
    Ui_ProjectWidget _ui;

    virtual void install() = 0;
    virtual void selectFile(bool latest, QString version) = 0;

    OneMeta::ProjectPtr m_project;
    Config &config;
    HearthAnalytics &tracker;

    QString getIcon();

signals:
    void signalFile(QString file = "");
    void installed();

public slots:
    void setIcon(QString icon);
    void moreClicked();
    void installClicked();

    void paintEvent(QPaintEvent *event);

protected:
    BackgroundDownloader dl;
};

class PackWidget : public ProjectWidget {
    Q_OBJECT
public:
    explicit PackWidget(OneMeta::ProjectPtr project, Config &conf, HearthAnalytics &ha, QWidget *parent = nullptr);
    void install() override;
    void selectFile(bool latest = true, QString version = "") override;

private:
    BasePack *pack = nullptr;
};

class ModInstallWidget : public ProjectWidget {
    Q_OBJECT
public:
    explicit ModInstallWidget(OneMeta::ProjectPtr project, MinecraftInstance *instance, Config &conf,
                              HearthAnalytics &ha, QWidget *parent = nullptr);
    void install() override;
    void selectFile(bool latest = true, QString version = "") override;

private:
    MinecraftInstance *m_instance = nullptr;
    MetaMod *mod = nullptr;
};
