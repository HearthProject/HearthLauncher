/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OneMeta.h"

#include "Fuzz/fuzzywuzzy.hpp"
#include "Util/Instrumentation.h"

#include <QDebug>
#include <QJsonArray>

#include <math.h>

OneMeta::OneMetaDatabase OneMeta::database = OneMeta::OneMetaDatabase();

const QString OneMeta::stages[OneMeta::Stage::NUM_STAGES] = {"Release", "Beta", "Alpha"};

const QString OneMeta::sourceNames[OneMeta::SourceType::NUM_TYPES] = {"Hearth", "Curse"};

const QString OneMeta::sorts[OneMeta::Sorting::NUM_SORTING] = {"none", "update", "created", "popularity",
                                                               "name"};

void OneMeta::Source::load(QJsonObject json) {
    name = json["name"].toString().toLower();
    url = json["url"].toString();
    hash = json["hashUrl"].toString();
    type = static_cast<SourceType>(json["source"].toInt());
    compression = static_cast<Compression>(json["compression"].toInt());
}

QString OneMeta::Source::getCompressionExt() const {
    switch (compression) {
        case NONE:
            return QString();
        case ZIP:
            return ".zip";
        case XZ:
            return ".xz";
    }
}

void OneMeta::Index::load(QJsonArray json) {
    for (int i = 0; i < json.size(); i++) {
        Source p;
        p.load(json[i].toObject());
        sources.insert(p.type, p);
    }
}

void OneMeta::File::load(QJsonObject json) {
    id = json["id"].toInt();
    date = json["date"].toInt();
    url = json["url"].toString();
    hash = json["hash"].toString();
    filename = json["filename"].toString();
    displayname = json["displayname"].toString();
    m_stage = static_cast<Stage>(json["stage"].toInt());
    project = json["project"].toInt();
    for (QJsonValue v : json["minecraft"].toArray()) {
        minecraft << v.toString();
    }
    for (QJsonValue v : json["dependencies"].toArray()) {
        QJsonObject o = v.toObject();
        Dependency d;
        //TODO make it called project in CMDB
        d.project = o["file"].toInt();
        d.optional = o["optional"].toBool(false);
        dependencies << d;
    }
}

void OneMeta::Project::load(QJsonObject json) {
    id = json["id"].toInt();
    title = json["title"].toString();
    description = json["description"].toString();
    type = static_cast<ProjectType>(json["type"].toInt());

    // Icon
    icon.url = json["icon"].toObject()["url"].toString();
    icon.hash = json["icon"].toObject()["hash"].toString();
    // Minecraft versions
    for (QJsonValue v : json["minecraft"].toArray()) {
        minecraft << v.toString();
    }
    // Stats
    for (QString key : json["stats"].toObject().keys()) {
        stats.insert(key, json["stats"].toObject()[key].toVariant());
    }

    // Authors
    for (QJsonValue v : json["authors"].toArray()) {
        QJsonObject o = v.toObject();
        Author a;
        a.role = static_cast<Role>(o["role"].toInt());
        a.username = o["username"].toString();
        a.uuid = o["uuid"].toString();
        authors.append(a);
    }

    // Files
    for (QJsonValue v : json["files"].toArray()) {
        this->files << v.toInt();
    }
}

QStringList OneMeta::Project::getAuthors() const {
    QStringList authors;
    for (Author a : this->authors) {
        authors << a.username;
    }
    return authors;
}

double OneMeta::Project::getPopularity() const { return stats.value("popularity", 0).toDouble(); }

qint64 OneMeta::Project::getCreationDate() const {
    return static_cast<qint64>(stats.value("created", 0).toInt()) * 1000;
}

qint64 OneMeta::Project::getLatestDate() const {
    return static_cast<qint64>(stats.value("latest", 0).toInt()) * 1000;
}

QVector<OneMeta::FilePtr> OneMeta::Project::filelist(QString version, int currentFileDate) const {
    QVector<OneMeta::FilePtr> files;
    for (int id : this->files) {
        OneMeta::FilePtr file = OneMeta::database.findFile(this->sourceType, id);
        if ((version.isEmpty() || file->minecraft.contains(version)) && file->date > currentFileDate) {
            files.append(std::move(file));
        }
    }
    std::sort(files.begin(), files.end(), [&](auto a, auto b) { return a->date > b->date; });
    return files;
}

OneMeta::FilePtr OneMeta::Project::latest(const QString &version) const {
    QVector<OneMeta::FilePtr> files(filelist(version));
    if (files.isEmpty())
        return nullptr;
    return filelist(version).first();
}

OneMeta::FilePtr OneMeta::Project::oldest(const QString &version) const {
    QVector<OneMeta::FilePtr> files(filelist(version));
    if (files.isEmpty())
        return nullptr;
    return filelist(version).last();
}

bool OneMeta::Project::isAuthor(const QString &user) const {

    for(Author a: authors) {
        if(a.username.toLower() == user.toLower()) {
            return true;
        }
    }
    return false;
}


OneMeta::Provider::Provider(QJsonObject json, OneMeta::SourceType source) {
    ScopedTask _("OneMeta::Provider::Provider");
    QJsonObject projectsObject = json["projects"].toObject();
    QJsonObject filesObject = json["files"].toObject();

    for (QString key : filesObject.keys()) {
        auto f = std::make_shared<File>(filesObject[key].toObject(), source);
        files.insert(f->id, f);
    }

    for (QString key : projectsObject.keys()) {
        auto p = std::make_shared<Project>(projectsObject[key].toObject(), source);
        projects.insert(p->id, p);
    }
}

std::function<bool(OneMeta::ProjectPtr,OneMeta::ProjectPtr)> OneMeta::sortingFunction(Sorting sorting, bool direction ) {
    switch (sorting) {
        case LATEST_UPDATE:
            return [direction](ProjectPtr a, ProjectPtr b) {
                if(direction)
                    return a->getLatestDate() > b->getLatestDate();
                else
                    return a->getLatestDate() < b->getLatestDate();
            };
        case LATEST_CREATED:
            return [direction](ProjectPtr a, ProjectPtr b) {
                if(direction)
                    return a->getCreationDate() > b->getCreationDate();
                else
                    return a->getCreationDate() < b->getCreationDate();
                };
        case NAME:
            return [direction](ProjectPtr a, ProjectPtr b) {
                if(direction)
                    return a->title.toLower() < b->title.toLower();
                else
                    return a->title.toLower() > b->title.toLower();
            };
        case POPULARITY:
        default:
            return [direction](ProjectPtr a, ProjectPtr b) {
                if(direction)
                    return a->getPopularity() > b->getPopularity();
                else
                    return a->getPopularity() < b->getPopularity();
            };
    }
}

QVector<OneMeta::ProjectPtr>
OneMeta::search(const QString &query, int sorting, QList<OneMeta::SourceType> types, int limit,
                QList<std::function<bool(ProjectPtr)>> filters) {
    clock_t start = clock();

    QVector<ProjectPtr> collect;
    for (SourceType type : types) {
        if (OneMeta::database.providers.contains(type)) {
            collect.append(OneMeta::database.providers.value(type)->projects.values().toVector());
        }
    }
    QVectorIterator<ProjectPtr> iter(collect);

    QVector<ProjectPtr> results;
    QVector<QPair<ProjectPtr, int>> candidates;

    std::string q = query.toLower().toStdString();
    std::string title;
    int title_partial;
    int title_full;
    bool isNumber = false;
    int pid = query.toInt(&isNumber);
    auto sortFunc = sortingFunction(static_cast<OneMeta::Sorting>(std::abs(sorting)), sorting > 0);
    while (iter.hasNext()) {
        ProjectPtr project = iter.next();
        for (std::function<bool(ProjectPtr)> filter : filters)
            if (!filter(project))
                goto end;

        if (query.isEmpty()) {
            results.append(std::move(project));
        } else {
            if (isNumber) {
                if (project->id == pid)
                    results.append(std::move(project));
            } else {
                title = project->title.toLower().toStdString();
                title_partial = fuzz::partial_ratio(q, title);
                if (title == q)
                    title_partial = 10000000;
                if (title_partial < 75)
                    continue;
                title_full = fuzz::ratio(q, title);
                candidates.append(QPair<ProjectPtr, int>(
                        std::move(project),
                        title_partial + (title_full * 2) * (static_cast<int>(project->getPopularity()))));
            }
        }
        end:
        continue;
    }

    if (query.isEmpty()) {
        if (results.isEmpty())
            return results;
        std::sort(results.begin(), results.end(), sortFunc);
        if (limit > 0)
            results.erase(results.begin() + limit, results.end());
    } else {
        if (candidates.isEmpty())
            return results;
        auto compare = [](QPair<ProjectPtr, int> p1, QPair<ProjectPtr, int> p2) { return p1.second > p2.second; };
        std::sort(candidates.begin(), candidates.end(), compare);
        auto length = limit > 0 ? std::min(candidates.size(), limit) : candidates.size();
        for (int i = 0; i < length; i++) {
            results.append(candidates[i].first);
        }
    }

    qDebug() << "Search finished in" << (clock() - start) / (double) (CLOCKS_PER_SEC / 1000) << "ms";
    return results;
}

QUrl OneMeta::getUrl(OneMeta::ProjectPtr project) {
    switch (project->sourceType) {
        case CURSE:
            return QUrl(QString("https://minecraft.curseforge.com/projects/%1").arg(project->id));
        default:
            return QUrl("");
    }
}

OneMeta::ProjectPtr OneMeta::OneMetaDatabase::findProject(OneMeta::SourceType type, int id) {
    for (auto &provider : providers.values(type)) {
        if (provider->projects.contains(id))
            return provider->projects[id];
    }
    return 0;
}

OneMeta::FilePtr OneMeta::OneMetaDatabase::findFile(OneMeta::SourceType type, int id) {
    for (auto &provider : providers.values(type)) {
        if (provider->files.contains(id))
            return provider->files[id];
    }
    return 0;
}
