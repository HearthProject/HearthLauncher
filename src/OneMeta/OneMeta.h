/*
 * Copyright (C) 2017-2018 joonatoona
 * Copyright (C) 2017-2018 primetoxinz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "Util/Utils.h"
#include <QJsonObject>
#include <QMap>
#include <QMultiMap>
#include <QUrl>
#include <QVariant>
#include <QVector>
#include <functional>
#include <memory>
#include <utility>
#include <functional>

class OneMeta {

public:
    enum Role { GUEST, USER, TRUSTED, MODERATOR, ADMIN };

    enum SourceType { HEARTH = 0, CURSE, NUM_TYPES };

    static const QString sourceNames[SourceType::NUM_TYPES];

    enum Compression { NONE, ZIP, XZ };

    enum Stage { RELEASE, BETA, ALPHA, NUM_STAGES };

    static const QString stages[NUM_STAGES];

    enum Sorting { UNKNOWN, LATEST_UPDATE, LATEST_CREATED, POPULARITY, NAME, NUM_SORTING };

    static const QString sorts[NUM_SORTING];

    enum ProjectType { MODPACK, MOD, RESOURCEPACK };

    class Source {
    public:
        void load(QJsonObject json);

        QString name, url, hash;
        SourceType type;
        Compression compression;

        QString getCompressionExt() const;
    };

    class Index {
    public:
        void load(QJsonArray json);

        QMultiMap<SourceType, Source> sources;
    };

    struct Author {
        QString username = QString(), uuid  = QString();
        Role role = Role::GUEST;
    };

    struct BaseFile {
        QString url = QString(), hash = QString();
    };

    struct Dependency {
        int project = 0;
        bool optional = false;
    };

    class File : public BaseFile {
    public:
        File(QJsonObject json, OneMeta::SourceType type) : sourceType(type) { load(std::move(json)); }
        int id, project, date;
        OneMeta::SourceType sourceType;
        Stage m_stage;
        QString version, filename, displayname, file;
        QStringList minecraft;
        QVector<Dependency> dependencies;

        void load(QJsonObject json);

        bool operator<(const File &f) { return date < f.date; }

        bool operator>(const File &f) { return date > f.date; }

        QString stage() const { return stages[m_stage]; }
    };

    typedef std::shared_ptr<const OneMeta::File> FilePtr;

    class Project {
    public:
        Project(QJsonObject json, OneMeta::SourceType type) : sourceType(type) { load(std::move(json)); }
        void load(QJsonObject json);

        int id;
        ProjectType type;
        QString title, description;
        QVector<Author> authors;
        BaseFile icon;
        QMap<QString, QVariant> stats;
        QVector<int> files;
        QStringList minecraft;

        QStringList getAuthors() const;
        double getPopularity() const;
        qint64 getCreationDate() const;
        qint64 getLatestDate() const;

        bool operator<(const Project &p) { return p.getPopularity() < getPopularity(); }

        bool isAuthor(const QString &user) const;

        OneMeta::SourceType sourceType;

        OneMeta::FilePtr latest(const QString &version = "") const;
        OneMeta::FilePtr oldest(const QString &version = "") const;

        QVector<OneMeta::FilePtr> filelist(QString version = "", int current = 0) const;
    };

    typedef std::shared_ptr<const OneMeta::Project> ProjectPtr;

    class Provider {
    public:
        Provider() {}
        Provider(QJsonObject data, OneMeta::SourceType source);
        QMap<int, ProjectPtr> projects;
        QMap<int, FilePtr> files;
    };

    class OneMetaDatabase {
    public:
        QMap<SourceType, std::shared_ptr<OneMeta::Provider>> providers;
        ProjectPtr findProject(OneMeta::SourceType type, int id);
        FilePtr findFile(OneMeta::SourceType type, int id);
    };
    static OneMetaDatabase database;

    static QVector<ProjectPtr> search(const QString &query, int sorting, QList<SourceType> types, int limit,
                                      QList<std::function<bool(ProjectPtr)>> filters);

    static std::function<bool(OneMeta::ProjectPtr,OneMeta::ProjectPtr)> sortingFunction(Sorting sorting, bool direction = true);

    static OneMetaDatabase &getDatabase() { return database; }

    static QUrl getUrl(OneMeta::ProjectPtr project);
};
