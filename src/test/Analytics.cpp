#include "catch.hpp"

#include "Analytics/HearthAnalytics.h"
#include "Util/Config.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QSignalSpy>

#include "Hearth/HearthAPI.h"

TEST_CASE("Analytics", "[Analytics]") {
    Config c;
    c.config_dir = "Inputs";
    c.data_dir = "Inputs";
    c.cache_dir = "Inputs";
    c.config_file = c.config_dir + QDir::separator() + "hearthlauncher.ini";
    c.sendAnalytics = true;
    c.analyticsId = "ponies";

    HearthAnalytics ha(c);
    QSignalSpy spy(&ha, &HearthAnalytics::post);
    ha.sendEvent(HearthEventType::INSTALLED, "glowing orbs");
    REQUIRE(spy.size() == 1);
    QList<QVariant> args = spy.takeFirst();
    REQUIRE(args.size() == 3);
    CHECK(args[0].toString() == QString(HearthAPI::BASE_URL + "/analytics/event"));
    CHECK(args[1].toString() == "application/json");
    QJsonDocument json = QJsonDocument::fromJson(args[2].toByteArray());
    REQUIRE(json.isObject());
    QJsonObject obj = json.object();
    CHECK(obj["uuid"].toString() == "ponies");
    CHECK(!obj["version"].isNull());
    CHECK(!obj["builder"].isNull());
    CHECK(!obj["os_name"].isNull());
    CHECK(!obj["arch"].isNull());
    CHECK(!obj["mem"].isNull());
    CHECK(!obj["locale"].isNull());
    CHECK(obj["event"].toInt() == HearthEventType::INSTALLED);
    CHECK(obj["extra"].toString() == "glowing orbs");
}
