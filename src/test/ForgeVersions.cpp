#include "catch.hpp"

#include <QDebug>
#include "Minecraft/ForgeVersions.h"

TEST_CASE("Forge version parsing", "[Minecraft]") {
    ForgeVersions fv;
    fv.loadFromFile("Inputs/forge.json");

    REQUIRE(fv.versions.size() == 1);

    BasicForgeVersion bf = fv.versions.values("1.12.2")[0];

    REQUIRE(bf.version == "14.23.1.2556");
    REQUIRE(bf.releaseTime == "2017-11-27T03:20:30+00:00");
    REQUIRE(bf.sha256 == "b34bcd893e13e6eb87e09f902ac70a7e8674f16f407b5042867b9f3a2cdd724e");
}
