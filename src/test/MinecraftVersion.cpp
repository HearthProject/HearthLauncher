#include "catch.hpp"

#include "Minecraft/MinecraftVersions.h"
#include "Util/Config.h"

TEST_CASE("Minecraft version parsing", "[Minecraft]") {
    Config c;
    c.config_dir = "Inputs";
    c.data_dir = "Inputs";
    c.cache_dir = "Inputs";
    c.config_file = c.config_dir + QDir::separator() + "hearthlauncher.ini";

    MinecraftVersions mv(c);
    mv.loadFromFile(c.cache_dir + "/minecraft.json");
    {
        // Check latest.
        CHECK(mv.latest_release == "1.12.2");
        CHECK(mv.latest_snapshot == "17w48a");
    }

    {
        // Check that we see the 2 non-prerelease versions in the correct order.
        QStringList vers = mv.getVersions();
        REQUIRE(vers.size() == 2);
        CHECK(vers[0] == "17w48a");
        CHECK(vers[1] == "1.11.2");
    }

    {
        // Check the values.
        BasicMinecraftVersion &bmv = mv.versions["1.11.2"];
        CHECK(bmv.id == "1.11.2");
        CHECK(bmv.type == ReleaseType::RELEASE);
        CHECK(bmv.release_time == "2016-12-21T09:29:12+00:00");
        CHECK(bmv.url ==
              "https://launchermeta.mojang.com/mc/game/12f260fc1976f6dd688a211f1a906f956344abdd/1.11.2.json");
    }
}
