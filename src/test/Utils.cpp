#include "catch.hpp"

#include "Util/Utils.h"
#include <QCryptographicHash>

TEST_CASE("getInstalledMemory works", "[Utils]") {
    REQUIRE(Utils::getInstalledMemory() > 0);
}

TEST_CASE("compareChecksum works", "[Utils]") {
    REQUIRE(Utils::compareChecksum("Inputs/test_hash", "324af02abe340d078c7f9c1c0ffe8f28"));
    REQUIRE(Utils::compareChecksum("Inputs/test_hash", "abcd") == false);

    REQUIRE(Utils::compareChecksum("Inputs/test_hash", "6c4e7a556d8515be1e672d8141fd1d8f8b0f9792", QCryptographicHash::Sha1));
    REQUIRE(Utils::compareChecksum("Inputs/test_hash", "abcd", QCryptographicHash::Sha1) == false);
}

TEST_CASE("formatNumber works", "[Utils]") {
    REQUIRE(Utils::formatNumber(10500) == "10.5K");
    REQUIRE(Utils::formatNumber(1500000) == "1.5M");
}
